/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import React, { Component } from 'react';
import Routes from './src/Routes/Routes';

export default class App extends Component {

  render() {
    return (
      <>
        {/* <SafeAreaView style={styles.topSafeArea} /> */}
        <SafeAreaView style={styles.bottomSafeArea}>
          <StatusBar backgroundColor={'white'} barStyle='dark-content' />
          
            <Routes />
         
        </SafeAreaView>
      </>
    );
  }

}

const styles = StyleSheet.create({
  topSafeArea: {
    flex: 0,
    backgroundColor: 'white'
  },
  bottomSafeArea: {
    flex: 1,
    backgroundColor: 'white'
  },
});

