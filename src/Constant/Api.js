const apis = {

    // stage api
    baseUrlTest: "https://goodluckstationers.com/testing/GoodluckApi/public/index.php/api/",
    baseUrl: "https://goodluckwholesale.com/GoodluckApi/public/index.php/api/",
   
    Login:"Login",
    SignInOtp:"OtpSend",
    OtpVarification:"OtpVarification",
    LocationApi:"GetDataAll",
    verifyPin:"GetDataAllTest",
    RegisterApi:"Register",
    Category:"GetDataAll",
    SubCategoryApi:"GetDataAll",
    UpdateApi:"InsertAll",
    ForgetPassword:"ForgetPassword",
    ChangePassword:"ChangePassword",
    
    imageUrl:"https://goodluckwholesale.com/" 
  

 
  };
  
  export default apis
  