import React, { Component } from 'react';
import { createStackNavigator, createDrawerNavigator, createAppContainer } from "react-navigation";
//import { createStackNavigator } from 'react-navigation-stack';
import SplashScreen from '../pages/SplashScreen';
import LoginScreen from '../pages/Login';
import CustomerListScreen from '../pages/CustomerList';
import ProductListScreen from '../pages/ProductList';
import OTPScreen from '../pages/OTP';
import CheckoutScreen from '../pages/Checkout';
import MyCartScreen from '../pages/MyCart';

const AppNavigation = createStackNavigator({
    Login: { screen: LoginScreen, navigationOptions: { header: null, gesturesEnabled: false, } },
    //OTP: { screen: OTPScreen, navigationOptions: { header: null } },
  });
  
  const MyApp = createDrawerNavigator({
    App: {
      screen: AppNavigation,
    }
  });
  
  const LandingNavigation = createStackNavigator({
  

    Splash: {
      screen: SplashScreen,
      navigationOptions: { header: null, gesturesEnabled: false, }
    },

    Login: {
      screen: LoginScreen,
      navigationOptions: { header: null, gesturesEnabled: false, }
    },

    OTP: {
      screen: OTPScreen,
      navigationOptions: { header: null, gesturesEnabled: false, }
    },

    CustomerList: {
      screen: CustomerListScreen,
      navigationOptions: { header: null, gesturesEnabled: false, }
    },

    ProductList: {
      screen: ProductListScreen,
      navigationOptions: { header: null, gesturesEnabled: false, }
    },

    MyCart: {
      screen: MyCartScreen,
      navigationOptions: { header: null, gesturesEnabled: false, }
    },

    Checkout: {
      screen: CheckoutScreen,
      navigationOptions: { header: null, gesturesEnabled: false, }
    },


  
    // Root:
    // {
    //   screen: MyApp,
    //   navigationOptions: ({ navigation }) => ({
    //     header: null,
    //     gesturesEnabled: false,
    //   })
    // },
  });

const AppContainer = createAppContainer(LandingNavigation);

export default AppContainer;
