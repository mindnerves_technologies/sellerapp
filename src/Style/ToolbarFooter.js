import React, { Component } from 'react'
import {
  AppRegistry, StyleSheet,
  View, Toolbar, Text, Image, TouchableOpacity, TouchableHighlight,Dimensions
} from "react-native";
import global_style from '../Style/GlobalStyle';
import Images from "../Constant/Images";
import Theme from '../Style/Theme'
import {verticalScale,moderateScale,scale} from 'react-native-size-matters'

const SCREEN_WIDTH = Dimensions.get('window').width;
export default class ToolbarFooter extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={{ backgroundColor:"red", position: 'absolute', bottom: 0, width: SCREEN_WIDTH, }}>

        <View style={{ flexDirection: 'row', alignSelf:'center' }}>

          <View
            style={[global_style.connectionStatus]}>

            <Text style={{
              color: Theme.color_white, fontFamily: "Roboto-Bold",
              fontSize: moderateScale(14), textAlign: 'center', width: '100%'
            }}>
              {this.props.param_title}
            </Text>

          </View>

        </View>

      </View>
    )
  }
}
