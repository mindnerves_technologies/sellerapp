import React, { Component } from 'react'

import { StyleSheet,
  View, Toolbar, Text, TouchableOpacity, Dimensions, Platform, ImageBackground
} from "react-native";

const SCREEN_WIDTH = Dimensions.get('window').width;
import Theme from '../Style/Theme';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import AntDesign from 'react-native-vector-icons/dist/AntDesign';
import Entypo from 'react-native-vector-icons/dist/Entypo';
import FontAwesome5 from 'react-native-vector-icons/dist/FontAwesome5';
import Feather from 'react-native-vector-icons/dist/Feather';
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from '@react-native-community/async-storage';
import {verticalScale,moderateScale,scale} from 'react-native-size-matters'

export default class TabFooter extends Component {

  constructor(props) {
    super(props);
    this.state = {

      connection_status: false,
      userLID:'',
      userUID:'',
      userGetData:''
    }
  }

  componentDidMount() {
    //To get the network state once
    NetInfo.addEventListener(state => {
      if (state.isConnected) {
        this.setState({ connection_status: true });
      } else {
        this.setState({ connection_status: false });
      }
    });
      this.getUserData();
  }

  //Get user Info
getUserData = async () => {

  try {
    // for getting user data
    let user_data = await AsyncStorage.getItem('userData');

    //Parse user data
    let get_data = JSON.parse(user_data);

    this.setState({ userGetData: get_data })

    this.setState({ userLID: get_data.userLID })

    this.setState({ userUID: get_data.userId })

  } catch (e) {
    // Saving error
    // this.props.navigation.navigate('Login')
    
    let user_loc = await AsyncStorage.getItem('user_location');
    let get_token = JSON.parse(user_loc);
    //this.state.userLID = get_token.LocationId;
  }

}


gotoProfile = () => {

  // if(this.state.userGetData == "" || this.state.userGetData == null){
  //   this.props.global_navigation.navigate('Login')
  
  // }else{
    this.props.global_navigation.navigate('CustomerList',{
      param_fromPage:'tab'
    })

  // }

}

  render() {
    return (

      <View style={
        this.props.connection_status ?
          {
            flexDirection: "row", height: verticalScale(50), backgroundColor: Theme.color_white,flex:1,width: "100%",
             position: 'absolute', bottom: 0, borderTopRightRadius:7,borderTopLeftRadius:7,
          }
          :
          {
            flexDirection: "row", height: verticalScale(50), backgroundColor: Theme.color_white,width: "100%",
            position: 'absolute', bottom: 25,borderRadius:10, borderTopRightRadius:7,borderTopLeftRadius:7,flex:1
          }
      }>
        {/* <TouchableOpacity
          onPress={() => {
            this.props.global_navigation.replace("dashboard");
          }}
          style={{ width: SCREEN_WIDTH / 4, justifyContent: "center",flex:1 ,width: "100%",}}>
          <View style={{ alignItems: "center" }}>

            {this.props.activePage == "dashboard" ?
              <View style={{ alignItems: "center", height: '100%' }}>
                <View style={{
                  height: verticalScale(3), backgroundColor: Theme.secondary,
                  position: 'absolute', top: 0, width: '70%',borderRadius:10
                }}></View>
                <View style={{ width: '100%', alignSelf: 'center', height: '100%',
                 justifyContent: 'center' , }}>
                  <Entypo name="home" size={verticalScale(20)} color={Theme.color_blue} style={[styles.Menu_icons]} />
                  <Text style={[styles.MenuText_Active, { color: Theme.color_blue }]}>{'Home'}</Text>
                </View>
              </View>
              :
              <View style={{ alignItems: "center" }}>
               <Entypo name="home" size={verticalScale(20)} color={Theme.color_grayL} style={[styles.Menu_icons]} />
              <Text style={[styles.MenuText_Inactive]}>{'Home'}</Text>
              </View>
            }

          </View>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            this.props.global_navigation.replace('SubCategory', {
              param_subCategory: "",
              selectedCellIndex: 0,
              param_FromTab:true,
              param_CategoryName: "General and Cosmetics",

            })
          }}
          style={{ width: SCREEN_WIDTH / 4, justifyContent: "center" ,flex:1}}>
          <View style={{ alignItems: "center" }}>
            {this.props.activePage == "SubCategory" ?
              <View style={{ alignItems: "center" }}>
               <View style={{
                  height: verticalScale(3), backgroundColor: Theme.secondary,
                  position: 'absolute', top: 0, width: '70%',borderRadius:10
                }}></View>
                <View style={{ width: '100%', alignSelf: 'center', height: '100%', justifyContent: 'center' }}>
                  <AntDesign name="appstore1" size={verticalScale(20)} color={Theme.color_blue} style={[styles.Menu_icons]} />
                  <Text style={[styles.MenuText_Active, { color: Theme.color_blue }]}>{'Category'}</Text>
                </View>
              </View>
              :
              <View style={{ alignItems: "center" }}>
                <AntDesign name="appstore1" size={verticalScale(20)} color={Theme.color_grayL} style={[styles.Menu_icons]} />
              <Text style={[styles.MenuText_Inactive]}>{'Category'}</Text>
              </View>
            }
          </View>
        </TouchableOpacity> */}

        

        <TouchableOpacity
          activeOpacity={global.activeOpacity}
          hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
          onPress={() => this.gotoProfile()}
            style={{ width: SCREEN_WIDTH / 4, justifyContent: "center" ,flex:1}}>
          
          <View style={{ alignItems: "center" }}>
            {this.props.activePage == "CustomerList" ?
              <View style={{ alignItems: "center" }}>
                <View style={{
                  height: verticalScale(3), backgroundColor: Theme.secondary,
                  position: 'absolute', top: 0, width: '70%',borderRadius:10
                }}></View>
                <View style={{ width: '100%', alignSelf: 'center', height: '100%', justifyContent: 'center' }}>
                <AntDesign name="user" size={verticalScale(23)} color={Theme.color_blue} style={[styles.Menu_icons]} />
                  <Text style={[styles.MenuText_Active, { color: Theme.color_blue }]}>{'Customer List'}</Text>
                </View>
              </View>
              :
              <View style={{ alignItems: "center" }}>
                <AntDesign name="user" size={verticalScale(23)} color={Theme.color_grayL} style={[styles.Menu_icons]} />
              <Text style={[styles.MenuText_Inactive]}>{'Customer List'}</Text>
              </View>
            }
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            this.props.global_navigation.replace("ProductList");
          }}

          style={{ width: SCREEN_WIDTH / 4, justifyContent: "center",flex:1 }}>
          <View style={{ alignItems: "center" }}>
            {this.props.activePage == "ProductList" ?
              <View style={{ alignItems: "center" }}>
               <View style={{
                  height: verticalScale(3), backgroundColor: Theme.secondary,
                  position: 'absolute', top: 0, width: '70%',borderRadius:10
                }}></View>
                <View style={{ width: '100%', alignSelf: 'center', height: '100%', justifyContent: 'center' }}>
                  <FontAwesome5 name="th-list" size={verticalScale(20)} color={Theme.color_blue} style={[styles.Menu_icons]} />
                  <Text style={[styles.MenuText_Active, { color: Theme.color_blue }]}>{'Product List'}</Text>
                </View>
              </View>
              :
              <View style={{ alignItems: "center" }}>
                <FontAwesome5 name="th-list" size={verticalScale(20)} color={Theme.color_grayL} style={[styles.Menu_icons]} />
              <Text style={[styles.MenuText_Inactive]}>{'Product List'}</Text>
              </View>
            }
          </View>
        </TouchableOpacity>

      </View>
    )
  }
}
const styles = StyleSheet.create(
  {
  MenuText_Active: {
    color: Theme.color_faint_pink,
    fontFamily: "TIMES",
    marginTop: 1,
    fontSize: moderateScale(12.5),
    fontWeight:'bold'
  },
  MenuText_Inactive: {
    color: Theme.color_grayL,
    fontFamily: "TIMES",
    marginTop: 1,
    fontSize: moderateScale(12.5),
    fontWeight:'bold'
  },
  Menu_icons: {
    width: verticalScale(20),
    height: verticalScale(20),
    marginBottom: 2,
    alignSelf: 'center'

  }
});