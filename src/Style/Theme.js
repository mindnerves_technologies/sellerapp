
const Theme = {

    color_orange: '#54179b',
    color_red: 'red',
    color_black: '#000',
    color_white: '#FFF',
    color_gray: '#DCE0E9',
    color_mrp_gray:"#606060",
    color_lightBlue:'#6495ed',
    color_blue: "#00285A",
    color_whatsapp: "#128c7e",
    primary_colors:"#00285A",
    secondary:'#CBA72D',
    // color_blue: "#0000FF",
    color_light_gray: "#f0f0f0",
    color_bgColor: "#fff",
    color_grayL: "#C8C8C8",

    // old
    color_caption: '#BCCCD4',
    color_active: '#007BFA',
    color_desc: '#f8f8f8',
    color_pure_gray: "#7E7E7E",
    color_address_border: "#c1c1c1",
    color_faint_pink: "#FF9D00",
    color_medium_purple: '#9370DB',
    color_green: '#008000',
};

export default Theme;





