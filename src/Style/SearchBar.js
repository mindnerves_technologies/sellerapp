import React, { Component } from 'react';
import {
	View, Text, StyleSheet, Image, TouchableOpacity, Dimensions, YellowBox, Alert,
	FlatList, TextInput, BackHandler, ActivityIndicator, ImageBackground, SafeAreaView,Pressable,
	PixelRatio, Share, KeyboardAvoidingView, Platform, StatusBar, ScrollView, Button
	, PermissionsAndroid
} from 'react-native'
import global from '../Style/GlobalValue';
import global_style from '../Style/GlobalStyle';
import Images from "../Constant/Images";
import Theme from '../Style/Theme'
import Api from "../Constant/Api";
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import AntDesign from 'react-native-vector-icons/dist/AntDesign';
import AsyncStorage from '@react-native-community/async-storage';
import DeviceInfo from 'react-native-device-info';
import Util from '../util/Util';
import { NavigationEvents } from 'react-navigation';
import {verticalScale,moderateScale,scale} from 'react-native-size-matters'

import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';
import SpeechAndroid from 'react-native-android-voice';
import CommonActivityIndicator from '../util/ActivityIndicator';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;


export default class SearchBar extends Component {

	constructor(props) {
		super(props);
		this.state = {
			deviceWidth:deviceWidth,
			deviceHeight:deviceHeight,
			connection_status: false,
			showDrawer: false,
			userUID: '',
			userLID: '',
			searchTerm: '',
			showArray: false,
			from:5,
			dataComesfrom:'',
			showProgressBar:true,

			showExitAppAlert: false,
			showPincodeModel: false,
			SubCategory: [],
			count: 0,
			AllProducts: [],
			refreshing: false,
			OnBottomReach: false,
			CartCount: '',
			CartItems: [],
			filterPID: '',
			WishlistArrayPush: [],
			stockArray: [],
			FinalTotal: 0.0,
			ProdPID: '',
			showArray1: false,
			totalValue2: '',
			AllProductsList: [],
			zeroQty: true,
			oneIncrement: 1,
			Product_PriceQty: 2,
			MultiQty: '',
			clickCount: 0,
			userGetData: null,
			VersionName: '',
			currentCartVisible: [],
			pageCount: 0,
			endResult: false,

			filterType:'',
			pagesCount: 0,
			priceFilterOn: '',
			sortingOrder:''
		};
	}

	async _buttonClick(){
		try{
			//More Locales will be available upon release.
			var spokenText = await SpeechAndroid.startSpeech("Speak to search product", SpeechAndroid.DEFAULT);
			// this.setState({searchTerm:spokenText},()=>{
			// 	this.searchUpdated(this.state.searchTerm)
			// })
			console.log('########',spokenText)
			
		}catch(error){
			switch(error){
				case SpeechAndroid.E_VOICE_CANCELLED:
					ToastAndroid.show("Voice Recognizer cancelled" , ToastAndroid.LONG);
					break;
				case SpeechAndroid.E_NO_MATCH:
					ToastAndroid.show("No match for what you said" , ToastAndroid.LONG);
					break;
				case SpeechAndroid.E_SERVER_ERROR:
					ToastAndroid.show("Google Server Error" , ToastAndroid.LONG);
					break;
				/*And more errors that will be documented on Docs upon release*/
			}
		}
	}

	componentWillUnmount() {
		//this.didFocus.remove();
		//this.willBlur.remove();
		BackHandler.removeEventListener("hardwareBackPress", this.onBack);
	}

	componentDidMount = () => {
		var temp = ''
		var rightSide = 5
		temp = this.props && this.props.global_navigation && this.props.global_navigation.state && this.props.global_navigation.state.routeName;

		if(temp===''){
			rightSide = 5
		}
		// if(temp==='ProductList' || temp==='SubCategoryList'){
		if(temp==='ProductList' || temp==='dashboard' ){
			rightSide = 55
		}
		//To get the network state once
		NetInfo.addEventListener(state => {
			if (state.isConnected) {
				this.setState({ connection_status: true });
				//call API

			} else {
				this.setState({ connection_status: false });
			}
		});
		var VersionCode = DeviceInfo.getVersion()
		setTimeout(() => {
			this.getUserData();
		}, 50);
		
		this.setState({from:rightSide})
		this.setState({dataComesfrom:temp})


		///////////////////////////////////////////////////

	// 	this.willBlur = this.props.navigation.addListener("willBlur", payload =>
	// 	BackHandler.removeEventListener("hardwareBackPress", this.onBack),
	// );

	//To get the network state once
	NetInfo.addEventListener(state => {
		if (state.isConnected) {
			this.setState({ connection_status: true });
		} else {
			this.setState({ connection_status: false });
		}
	});

	var VersionCode = DeviceInfo.getVersion()
	setTimeout(() => {

		//this.getUserData();
		this.setState({ VersionName: VersionCode })
		//this.LoadSubCategoryAPI();
		// this.LoadStockAPI();
	}, 100);

	Util.getAsyncStorage('ORDER_MIN_VALUE').then((data) => {
		if (data !== null) {
			this.setState({ limit: data })
		}
	})
	this.callLocalcart()
	this.props.searchData(this)
	}

	callLocalcart() {

		Util.getAsyncStorage('MY_LOCAL_CART').then((data) => {
			if (data !== null) {
				this.setState({ currentCartVisible: data })
				setTimeout(() => {
					this.caluculate()
				}, 100);
			}
		})
	}

	getUserData = async () => {
		try {
			let user_data = await AsyncStorage.getItem('userData');
			let get_data = JSON.parse(user_data);
			this.setState({ userLID: get_data.userLID })
			this.setState({ userUID: get_data.userId })
		} catch (e) {
			let user_loc = await AsyncStorage.getItem('user_location');
			let get_token = JSON.parse(user_loc);
			//this.state.userLID = get_token.LocationId;
			this.state.userLID = 11;
		}
	}

	searchForCategory = async (searchTerm) => {
		let url = Api.baseUrl + Api.verifyPin
		let mUserData = {
			"CatByFilter":"CatByFilter",
   			"CID": this.props.searchFor.CID,
   			"LID":this.state.userLID,
   			"search_term" :searchTerm,
   			"type" :"Category"
		}

		fetch(url, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(mUserData)
		})
			.then((response) => response.json())
			.then((response) => {
				let bigCities = [];
				for (let i = 0; i < response.length; i++) {
					if (response[i].pstatus === 'Y') {
						bigCities.push(response[i]);
					}
				}
				this.setState({ AllProducts: bigCities });
				this.setState({ showArray: true })
				this.setState({ showProgressBar: false })
			})
			.catch((error) => {
				this.setState({ showProgressBar: false })
			})
			.done();
	}

	searchProductsAPI = (priceType,priceFilterOn,sortingOrder) => {
		this.setState({ showProgressBar: true })
		priceType!=undefined && this.setState({filterType:priceType})
		priceFilterOn!=undefined && this.setState({priceFilterOn:priceFilterOn})
		sortingOrder!=undefined && this.setState({sortingOrder:sortingOrder})
		var temp = null
		var priceFilterOn = priceFilterOn
		var priceType = priceType
		var sortingOrder = sortingOrder
		//console.log(":::type:::",priceType,":::ONN:::",priceFilterOn,":::sorting:::",sortingOrder)
		if(this.state.searchTerm.length == 0 && (this.state.dataComesfrom === 'ProductList' || this.state.dataComesfrom === 'SubCategoryList')){
			this.setState({showArray:false})
		}
		if (this.state.searchTerm.length > 2) {
			if (this.state.connection_status) {
				var url = Api.baseUrl + Api.verifyPin
				let mUserData = {
					"ProductSearch_v1_test": "ProductSearch_v1_test",
					"term": this.state.searchTerm,
					"LID": this.state.userLID
				};

				if (sortingOrder!=undefined && priceFilterOn!=undefined && priceFilterOn != '') {
					mUserData={
						"AllProducts3":"AllProducts3",
						"pageno":`${parseInt(this.state.pagesCount/20)+1}`,
						"FilterData": priceFilterOn,
						"HTOL": sortingOrder,
						"LID":this.state.userLID,
						"term": this.state.searchTerm,
					}
					if (sortingOrder!=undefined && priceFilterOn!=undefined && priceType!=undefined && priceFilterOn == 'AlphabetFilter' && priceType == "Alphabetic") {
						mUserData = {
							"AllProducts3":"AllProducts3",
							"pageno":`${parseInt(this.state.pagesCount/20)+1}`,
							"FilterData": priceFilterOn,
							"AToZ": sortingOrder,
							"LID": this.state.userLID,
							"term": this.state.searchTerm,
						}	
					}
					// if (this.state.filterType=='Brands') {
						
					// }
					if (priceType!=undefined && priceType=='Discounts') {
						mUserData={
							"AllProducts3":"AllProducts3",
							"pageno":`${parseInt(this.state.pagesCount/20)+1}`,
							"FilterData": "DiscountFilter",
							"Discount":"Discount",
							"LID":this.state.userLID,
							"term": this.state.searchTerm,
						}
					}
					
				}

				if (this.props.searchFor) {
					if (this.props.searchFor.type == 'Category') {
						mUserData = {
							"CatByFilter":"CatByFilter",
							   "CID": this.props.searchFor.CID,
							   "LID":this.state.userLID,
							   "search_term" :this.state.searchTerm,
							   "type" :"Category"
						}
					}
					if (this.props.searchFor.type == 'SubCategory') {
						mUserData = {
							"CatByFilter": "CatByFilter",
							"SCID": this.props.searchFor.SCID,
							"LID": this.state.userLID,
							"search_term": this.state.searchTerm,
							"type" :"subCategory"
						 }
					}
					
					if (this.props.searchFor.type == 'BrandProducts') {
						mUserData = {
							"BrandProducts_v2":"BrandProducts_v2",
							"BID": this.props.searchFor.BID,
							"LID": this.state.userLID,
							"term": this.state.searchTerm
						}
					}
				}
				fetch(url, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
					},
					body: JSON.stringify(mUserData),
				})
					.then((response) => response.json())
					.then((response) => {
						let bigCities = [];
						for (let i = 0; i < response.length; i++) {
							if (response[i].pstatus === 'Y') {
								bigCities.push(response[i]);
							}
						}

						// bigCities.length!=0 && bigCities.map(()=>{
						// 	bigCities.length!=0 && bigCities.map((ele,index)=>{
						// 		if(ele.Qty_avail==0){
						// 			temp = bigCities.splice(index,1)
						// 			bigCities.push(temp[0])
						// 		}
						// 	})
						// })
						this.setState({ showProgressBar: false })
						this.setState({ AllProducts: bigCities });
						// if(this.state.dataComesfrom === 'ProductList'){
						// 	this.props.AllProductsData.setState({AllProducts:bigCities})
						// }
						// if(this.state.dataComesfrom === 'SubCategoryList'){
						// 	this.props.AllProductsData.setState({SubCategoryProduct:bigCities})
						// }
						if (response != 'blank') {
							this.setState({ showArray: true })	
						} else {
							this.setState({ showArray: false })	
						}
						
						this.setState({ showProgressBar: false })
					})
					.catch((error) => {
						this.setState({ showProgressBar: false })
					})
					.done();
			} else {
				Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
				this.setState({ showProgressBar: false })
			}
		} else {
			this.setState({ showArray: false })
		}
	}

	searchUpdated = (term) => {
		this.callLocalcart()
		this.getUserData();
		//this.props.allData.callLocalcart()
		this.setState({ searchTerm: term },()=>{
			if (this.state.searchTerm.length < 1) {
				this.setState({ showArray: false })
				if(this.state.dataComesfrom == 'dashboard'){
					this.props.allData.setState({showFilter:false})
				}
			} else {
				this.searchProductsAPI()
				if(this.state.dataComesfrom == 'dashboard'){
					this.props.allData.setState({showFilter:true})
				}
			}
		})
		
	}

	clearSearchItemField = () => {
		//this.props.allData.callLocalcart()
		this.setState({ searchTerm: '' })
		this.forceUpdate()
		this.setState({ showArray: false })
		this.props.allData.setState({showFilter:false}) 
		// this.state.subcategoryArray = this.state.tempSubcategory;
	}
	JumpToProductDetais = (item, index) => {
		this.props.global_navigation.navigate('ProductDetais', {
			param_productDetails: item
		})
	}

	isAlreadyopen(itemId) {
		var found = false;
		this.state.currentCartVisible.map((item) => {
			if (item !== null && item.PID === itemId) {
				found = true;
			}
		});
		return found;
	}

	isAlreadyInLiveCartCount(itemId) {
		var found = false;
		this.state.currentCartVisible.map((item) => {
			if (item !== null && item.PID === itemId) {
				found = item;
			}
		});
		return found;
	}

	onSearchErase(){
		//this.props.allData.callLocalcart()
		this.props.allData.setState({showFilter:false})
	}


	render() {
		return (
			<View>
				<NavigationEvents onDidFocus={() => this._backRefresh()} />
				<View
					style={{ width: '100%', backgroundColor: Theme.color_blue }} onLayout={(event) =>{ this.setState({deviceWidth:event.nativeEvent.layout.width, deviceHeight:event.nativeEvent.layout.height}) }} >
					<View style={{
						flexDirection: 'row', borderRadius: 5, backgroundColor: Theme.color_white,
						justifyContent: 'space-between', marginLeft: 0, marginRight: 0, width: '95%', alignSelf: 'center',
						marginVertical: 10
					}}>
						<View style={{
							width: '90%', flexDirection: 'row', justifyContent: 'space-between',
							alignItems: 'center', alignSelf: 'center'
						}}>
							<Image
								source={Images.Search_icon}
								style={{ alignSelf: 'center', marginLeft: 15, width: verticalScale(20), height: verticalScale(20), }} />
							<TextInput
								style={{
									fontSize: moderateScale(14), color: Theme.color_black, alignSelf: 'center',
									height: verticalScale(40), marginLeft: 10, width: '85%',
								}}
								placeholder={'Search For customer'} 
								placeholderTextColor={Theme.color_blue}
								placeholderStyle={{ alignSelf: 'center', alignItems: 'center' }}
								returnKeyType={"go"}
								ref="search_item"
								value={this.state.searchTerm}
								onChangeText={term => { term.length > 2 ? this.searchUpdated(term) : this.setState({showArray:false, searchTerm:term}),term.length < 1 ? this.onSearchErase() : this.props.allData.setState({showFilter:true}) } }
								onSubmitEditing={() => this.searchProductsAPI()}
								allowFontScaling={false} />

							{this.state.searchTerm == '' ?
							// <MaterialIcons
							// 	name='mic'
							// 	color={Theme.color_blue}
							// 	hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
							// 	size={verticalScale(28)}
							// 	onPress={() => this._buttonClick() } 
							// 	style={{ justifyContent: 'center', right: this.state.dataComesfrom == "ProductList" ? verticalScale(this.state.from) : 0, padding: 2 }} />
							null
							:
							 <AntDesign
								name={'close'}
								color={Theme.color_black}
								hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
								size={verticalScale(28)}
								onPress={() => this.clearSearchItemField()}
								style={{ justifyContent: 'center', right: verticalScale(this.state.from), padding: 2 }} />
							}
						</View>
					</View>
				</View>
				
				{this.state.showArray && this.state.searchTerm.length > 2 ?
					<ScrollView style={{ width: '100%', }}>
						{this.state.showProgressBar ?
							<View style={{width:deviceWidth,height:deviceHeight}}>
								<CommonActivityIndicator showCenter={true}></CommonActivityIndicator>
							</View>
							:
							<View>
							<View style={{borderBottomWidth:2,borderBottomColor:Theme.color_blue, width:'100%'}}>
								<Text style={[global_style.TIMES_Bold14, {color:Theme.color_blue, textAlign:'center',marginVertical:10}]}>{'Showing '}{this.state.AllProducts.length}{' Search Results for '}{this.state.searchTerm ==='' ? "Last Search" : this.state.searchTerm}</Text>
							</View>
							{ <FlatList
								data={this.state.AllProducts}
								marginBottom={500}
								keyExtractor={(item, index) => index.toString()}
								showsHorizontalScrollIndicator={false}
								showsVerticalScrollIndicator={false}
								onEndReachedThreshold={0.5}
								//onEndReached={this.doInfinte}
								refreshing={this.state.refreshing}
								renderItem={this.renderItem.bind(this)}
								// renderItem={({ item, index }) =>
								// 	<View>
								// 		<View style={{ flexDirection: 'row' }}>
								// 			<View style={{ width: "30%", alignItems: 'center', alignSelf: 'center' }}>
								// 				<TouchableOpacity
								// 					activeOpacity={global.activeOpacity}
								// 					hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
								// 					onPress={() => this.JumpToProductDetais(item, index)}
								// 					style={{ height: 100, width: 100, padding: 5 }}
								// 				>
								// 					{item.image_small == null ?
								// 						<Image
								// 							source={Images.goodLuckimg}
								// 							style={[global_style.thmbnail_box, { width: 80, height: 90, }]} />
								// 						:
								// 						<Image
								// 							source={{ uri: Api.imageUrl + item.image_small }}
								// 							style={[{ flex: 1, width: null, height: null, resizeMode: 'cover' }]} />
								// 					}
								// 					{item.Qty_avail == 0 ?
								// 						<Image
								// 							source={Images.stockimg}
								// 							style={[global_style.thmbnail_box, { width: 100, height: 100, marginTop: - 100 }]} />
								// 						: null}
								// 				</TouchableOpacity>
								// 			</View>
								// 			<View style={{ width: "70%", padding: 10 }}>
								// 				<TouchableOpacity
								// 					activeOpacity={global.activeOpacity}
								// 					hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
								// 					onPress={() => this.JumpToProductDetais(item, index)}
								// 				>
								// 					<Text style={[global_style.TIMES_Regular14,]}>
								// 						{item.ProductName}
								// 					</Text>
								// 					<View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', marginTop: 10, marginRight: 10 }}>
														
								// 						<Text style={[global_style.TIMES_Bold12, { color: Theme.color_mrp_gray, textDecorationLine: 'line-through' }]}>
								// 							{"MRP ₹"} {item.MrpPrice}
								// 						</Text>
														
								// 						 <Text style={[global_style.TIMES_Bold14, { color: Theme.color_blue, }]}>
								// 							{"Price ₹"} {item.Selling_Price}
								// 						</Text>
								// 						<Text style={[global_style.TIMES_Bold12, { color: Theme.secondary, }]}>
								// 							{"You Save ₹"}{Number.parseFloat(item.MrpPrice - item.Selling_Price).toFixed(2)}
								// 							{item.discount != "" ?
								// 								<Text style={[global_style.TIMES_Bold12, { color: Theme.color_red }]}>
								// 									&nbsp; ({Number.parseFloat(item.discount).toFixed(2) + "% OFF"})
								// 								</Text>
								// 								: null}
								// 						</Text>
								// 					</View>
								// 				</TouchableOpacity>
								// 			</View>
								// 		</View>
								// 		<View
								// 			style={{
								// 				borderBottomColor: Theme.color_gray, alignSelf: 'center',
								// 				borderBottomWidth: 0.6, width: '100%', marginVertical: 5
								// 			}} />
								// 	</View>
								// }
							/> }
						</View>}
					</ScrollView> :
					null}
			</View>

		);
	}

	handleNotifyClick = (PID) => {
		var url = Api.baseUrl + "InsertAll"
		let mUserData = {
		      "InsertNotifyPro":"InsertNotifyPro",
		      "PID":PID,
		      "UID":this.state.userUID,
		      "LID":this.state.userLID
		  }      
	
		fetch(url, {
		  method: 'POST',
		  headers: {
			'Content-Type': 'application/json',
		  },
		  body: JSON.stringify(mUserData),
		})
		  .then((response) => response.json())
		  .then((response) => {
			Toast.showWithGravity(response.msg === "Added Successfully" ? "Will Notify When In Stock" : null, Toast.SHORT, Toast.CENTER);
			this.searchProductsAPI(this.state.filterType,this.state.priceFilterOn,this.state.sortingOrder)
		})
		  .catch((error) => {
		  })
		  
	  }

	renderItem(item_) {
		var item = item_.item;
		const zeroQty = this.isAlreadyopen(item.PID)
		const liveItem = this.isAlreadyInLiveCartCount(item.PID)
		const incartQunat = liveItem.qty
		const liverowid = liveItem.rowid
		if (zeroQty == true) {
			item.qty = liveItem.qty
		}
		let AviliableQuantity = item.Qty_avail
		if (zeroQty) {
			AviliableQuantity = item.Qty_avail - item.qty
		}

		if (item != null || item != undefined) {
			return (
				<View>
					<View style={{ flexDirection: 'row', margin: 5, }}>
						<View style={{ flex: 1, alignItems: 'center', alignSelf: 'center' }}>
							<TouchableOpacity
								activeOpacity={global.activeOpacity}
								hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
								onPress={() => this.JumpToProductDetais(item)}
								style={{ height: verticalScale(100), width: "95%", padding: 5 }}
							>
								{item.image_mid == null ?
									<Image
										source={Images.goodLuckimg}
										style={[global_style.thmbnail_box, { width: verticalScale(80), height: verticalScale(90), }]} />
									:
									<Image
										source={{ uri: Api.imageUrl + item.image_mid }}
										resizeMode={'contain'} resizeMethod="resize"
										style={[{ flex: 1, width: null, height: null }]} />}
								{item.Qty_avail == 0 ?
									<Image
										source={Images.stockimg}
										style={[global_style.thmbnail_box, { width: "95%", height: verticalScale(100), marginTop: -100 }]} />
									: null}
							</TouchableOpacity>
						</View>
						<View style={{ flex: 2, }}>
							<TouchableOpacity
								activeOpacity={global.activeOpacity}
								hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
								onPress={() => this.JumpToProductDetais(item)}
							>
								<Text style={[global_style.TIMES_Regular14,]}>
									{item.ProductName}
								</Text>
								<View style={{ flex: 1, flexDirection: 'row' }}>
									<View style={{ marginTop: 10, marginRight: 10 }}>
										<Text style={[global_style.TIMES_Bold12, { color: Theme.color_mrp_gray, textDecorationLine: 'line-through' }]}>
											{'MRP'} {item.MrpPrice}
										</Text>
										<Text style={[global_style.TIMES_Bold14, { color: Theme.color_blue, }]}>
											{'Price'} {item.Selling_Price}
										</Text>
										<Text style={[global_style.TIMES_Bold12, { color: Theme.secondary, }]}>
											{'YouSave'}{Number.parseFloat(item.MrpPrice - item.Selling_Price).toFixed(2)}
											{item.discount != "" &&
												<Text style={[global_style.TIMES_Bold12, { color: Theme.color_red }]}>
													{"\n"}&nbsp; ({Number.parseFloat(item.discount).toFixed(2) + 'PercentOff'})
										 </Text>}
										</Text>
									</View>
									<View style={{ flex: 1, alignItems: 'flex-end', marginTop: 10, marginRight: 10 }}>
										<Text style={[global_style.TIMES_Bold14, { color: Theme.color_blue, }]}>
											{'AvailQty'} {AviliableQuantity}
										</Text>
										{item.priceData != null && item.priceData != undefined && <Text style={[global_style.TIMES_Bold14, { color: Theme.color_blue, }]}>
											{'MinimumQty'} {item.priceData.productprice_qty}
										</Text>}

										 {item.Qty_avail === "0" 
										?
											zeroQty == false &&
											<TouchableOpacity
												//	disabled={this.state.hideIncrement ? true : false}
												activeOpacity={global.activeOpacity}
												hitSlop={{ top: 10, bottom: 10, left: 10, right: 8 }}
												style={{ width: DeviceInfo.isTablet() ? "55%" : "85%", marginTop: 30 }}
												onPress={() => { item.Notify !== "Notified" ? this.handleNotifyClick(item.PID) : Toast.showWithGravity("Notification for this product already set", Toast.SHORT, Toast.CENTER)} }
											>
												<View style={{
													backgroundColor: item.Notify === "Notified" ? 'green' : Theme.color_red, borderRadius: 5,
													paddingHorizontal: 15, paddingVertical: 5
												}}>
													<Text style={{
														color: Theme.color_white, fontSize: moderateScale(15),
														alignSelf: "center"
													}}>
														{"Notify Me"}
													</Text>
												</View>
											</TouchableOpacity>
										: 
										 zeroQty == false && 
											<TouchableOpacity
												//	disabled={this.state.hideIncrement ? true : false}
												activeOpacity={global.activeOpacity}
												hitSlop={{ top: 10, bottom: 10, left: 10, right: 8 }}
												style={{ width:DeviceInfo.isTablet() ? "55%" : "70%" , marginTop: 30 }}
												onPress={() => [item.Qty_avail === "0" ? Toast.showWithGravity("Product is out of stock", Toast.SHORT, Toast.CENTER) : this.LoadProductPriceAPI(item)]}
											>
												<View style={{
													backgroundColor: Theme.color_blue, borderRadius: 5,
													paddingHorizontal: 15, paddingVertical: 5
												}}>
													<Text style={{
														color: Theme.color_white, fontSize: moderateScale(14),
														alignSelf: "center"
													}}>
														{'Add'}
													</Text>
												</View>
											</TouchableOpacity>
										}
										
									</View>
								</View>
							</TouchableOpacity>
						</View>
					</View>
					<View style={{ flex: 1 }}>
						{(zeroQty == true && item.priceData != null && item.priceData != undefined) &&
							<View style={{ alignSelf: 'flex-end', alignItems: 'flex-end', flexDirection: "row", marginTop: 0, marginEnd: 7 }}>
								<TouchableOpacity
									style={{marginRight:4}}
									activeOpacity={global.activeOpacity}
									hitSlop={{ top: 7, bottom: 10, left: 10, right: 8 }}
									//onPress={() => this.Decrement(item)}
									onPress={() => this.Decrement(item, liverowid)}
								>
									<View style={{
										backgroundColor: Theme.color_blue, borderRadius: 50,
										height: verticalScale(26), width: verticalScale(26), alignSelf: "center", flexDirection: 'column',
										justifyContent: 'center', alignItems: 'center'
									}}>
										<AntDesign name='minus' color={Theme.color_white} size={verticalScale(16)} />
									</View>
								</TouchableOpacity>
								<TextInput style={{
									color: Theme.color_black, fontSize: moderateScale(14),padding:0,
									alignSelf: 'center', width: 'auto',marginHorizontal:5, height: verticalScale(32),paddingHorizontal:5,
									textAlign: "center", alignSelf: 'center',top:2 , borderWidth:0.5,borderColor:'#c9c9c9',
								}}
								onEndEditing={(e)=>{ [ AviliableQuantity >= parseInt(e.nativeEvent.text) && parseInt(e.nativeEvent.text)%parseInt(item && item.pro_price_data[0] && item.pro_price_data[0].productprice_qty)==0 ? this.changeQuantityDirect(item, liverowid, parseInt(e.nativeEvent.text == "0" ? item.priceData.productprice_qty : e.nativeEvent.text)) : this.getValue(AviliableQuantity, item,liverowid, parseInt(e.nativeEvent.text == "0" ? item.priceData.productprice_qty : e.nativeEvent.text),parseInt(item && item.pro_price_data[0] && item.pro_price_data[0].productprice_qty)) ] }}
								>
									{item.qty}
								</TextInput>
								<TouchableOpacity style={global_style.plusStyle}
									activeOpacity={global.activeOpacity}
									hitSlop={{ top: 7, bottom: 10, left: 10, right: 8 }}
									onPress={() => [AviliableQuantity < parseInt(item.pro_price_data[0].productprice_qty) ?
										Toast.showWithGravity('ReachAvailQty', Toast.SHORT, Toast.CENTER) : this.Increment(item, liverowid)]}>
									<View style={{
										backgroundColor: Theme.color_blue, borderRadius: 50,
										height: verticalScale(26), width: verticalScale(26), alignSelf: "center", flexDirection: 'column',
										justifyContent: 'center', alignItems: 'center'
									}}>
										<AntDesign name='plus' color={Theme.color_white} size={verticalScale(16)} />
									</View>
								</TouchableOpacity>
								{liveItem &&
									<TouchableOpacity
										disabled={this.state.hideIncrement ? true : false}
										activeOpacity={global.activeOpacity}
										hitSlop={{ top: 10, bottom: 10, left: 10, right: 8 }}
										style={{ width: "10%", marginTop: 0 }}
										onPress={() => this.RemoveApi(item, liverowid)}>
										<View style={{
											// backgroundColor: Theme.color_blue, borderRadius: 5,
											paddingHorizontal: 15, paddingVertical: 3
										}}>
											<Image style={{
												tintColor: Theme.color_blue,
												alignSelf: "center", width: verticalScale(20), height: verticalScale(20)
											}} source={Images.trash}>
											</Image>
										</View>
									</TouchableOpacity>}
							</View>
						}
					</View>
					<View
						style={{
							borderBottomColor: Theme.color_gray, alignSelf: 'center',
							borderBottomWidth: 0.6, width: '100%', marginVertical: 5
						}} />
				</View>
			)
		}
	}

		// BackBtn
		onBack = () => {

			this.props.navigation.navigate('dashboard')
			return true
		};
	
		//Get user Info
		// getUserData = async () => {
	
		// 	try {
		// 		this.setState({ showProgressBar: true })
		// 		// for getting user data
		// 		let user_data = await AsyncStorage.getItem('userData');
	
		// 		//Parse user data
		// 		let get_data = JSON.parse(user_data);
		// 		this.setState({ userLID: get_data.userLID })
		// 		this.setState({ userUID: get_data.userId, userGetData: get_data })
		// 		this.LoadSubCategoryAPI(false, this.state.pagesCount);
		// 		//		this.LoadCartCountAPI();
		// 	} catch (e) {
		// 		let user_loc = await AsyncStorage.getItem('user_location');
		// 		let get_token = JSON.parse(user_loc);
		// 		this.state.userLID = get_token.LocationId;
		// 		this.LoadSubCategoryAPI(false, this.state.pagesCount);
		// 	}
		// }
	
		applyFilterAPI(show,count) {
	
			if (this.state.refreshing) {
				count = 0
				this.setState({ AllProducts: [], pagesCount: 0 })
			}
	
			if (!show)
				this.setState({ showProgressBar: true })
			
			
			if (this.state.connection_status) {
				let mUserData = {
					"AllProducts1":"AllProducts1",
					"count": this.state.pagesCount,
					"FilterData":this.state.priceFilterOn,
					"HTOL": this.state.sortingOrder,
					"LID": this.state.userLID
				}
		
				const url = Api.baseUrl + Api.verifyPin
				fetch(url, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
					},
					body: JSON.stringify(mUserData),
				})
					.then((response) => response.json())
					.then((response) => {
						this.setState({ refreshing: false, bottomLoader: false })
						if (response.length > 0) {
							let data = []
							
							data = this.state.AllProducts.length > 0 ? this.state.AllProducts : []
							let bigCities = response;
						
							data = data.length > 1 ? data.concat(bigCities) : bigCities
							this.setState({ showArray: true })
							if (response.length > 2) {
								this.setState({ AllProducts: bigCities })
								for (let i = 0; i <= 10; i++) {
									this.setState({ AllProducts: data })
								}
								this.setState({ showProgressBar: false })
							} else {
								this.setState({ AllProducts: data })
								this.setState({ showProgressBar: false })
							}
						} else {
							this.setState({ showArray: false })
						}
					})
					.catch((error) => {
						this.setState({ showProgressBar: false })
					})
					.done();
			} else {
				Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
				this.setState({ showProgressBar: false })
			}
	
			
		}
		// location code end
		LoadSubCategoryAPI = (show, count) => {
	
			if (this.state.refreshing) {
				count = 0
				this.setState({ AllProducts: [], pagesCount: 0 })
			}
	
			if (!show)
				this.setState({ showProgressBar: true })
			if (this.state.connection_status) {
				var url = Api.baseUrl + Api.verifyPin
				let mUserData = {
					"AllProducts1_V1": "AllProducts1_V1",
					"LID": this.state.userLID,
					"count": count
				};
	
				if (this.state.priceFilterOn != '') {
					mUserData={
						"AllProducts2":"AllProducts2",
						"pageno":`${parseInt(count/20)+1}`,
						"FilterData":this.state.priceFilterOn,
						"HTOL": this.state.sortingOrder,
						"LID":this.state.userLID
					}
					if (this.state.priceFilterOn == 'AlphabetFilter' && this.state.filterType == "Alphabetic") {
						mUserData = {
							"AllProducts2":"AllProducts2",
							"pageno":`${parseInt(count/20)+1}`,
							"FilterData": this.state.priceFilterOn,
							"AToZ": this.state.sortingOrder,
							"LID": this.state.userLID
						}	
					}
					// if (this.state.filterType=='Brands') {
						
					// }
					if (this.state.filterType=='Discounts') {
						mUserData={
							"AllProducts2":"AllProducts2",
							"pageno":`${parseInt(count/20)+1}`,
							"FilterData": "DiscountFilter",
							"Discount":"Discount",
							"LID":this.state.userLID
						}
					}	
				}
				fetch(url, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
					},
					body: JSON.stringify(mUserData),
				})
					.then((response) => response.json())
					.then((response) => {
						this.setState({ refreshing: false, bottomLoader: false })
						if (response.length > 0) {
							//this.setState({ AllProducts1: response })
							let data = []
							//let jsondata = []
							data = this.state.AllProducts.length > 0 ? this.state.AllProducts : []
							let bigCities = response;
							//	for (let i = 0; i < response.length; i++) {
							//		if (response[i].pstatus === 'Y') {
							// var obj = JSON.parse(response[i].pro_price_data);
	
							// if(i===4){
	
							// }
							//	response[i].priceData = obj[0];
	
							// jsondata.push(obj)
							//			bigCities.push(response[i]);
							//		}
							//	}
							data = data.length > 1 ? data.concat(bigCities) : bigCities
							//this.setState({ showArray: true })
							if (response.length > 2) {
								this.setState({ AllProducts: bigCities })
								for (let i = 0; i <= 10; i++) {
									this.setState({ AllProducts: data })
								}
								this.setState({ showProgressBar: false })
							} else {
								this.setState({ AllProducts: data })
								this.setState({ showProgressBar: false })
							}
						} else {
							this.setState({ showArray: false })
						}
					})
					.catch((error) => {
						this.setState({ showProgressBar: false })
					})
					.done();
			} else {
				Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
				this.setState({ showProgressBar: false })
			}
		}
	
		LoadStockAPI = (getPID, getUnit, getIndex) => {
	
			if (this.state.connection_status) {
				var url = Api.baseUrl + Api.verifyPin
				let mData = {
					"check_stock_qty": "check_stock_qty",
					"PID": getPID,
					"Unit": getUnit
				};
				fetch(url, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
					},
					body: JSON.stringify(mData),
				})
					.then((response) => response.json())
					.then((response) => {
						this.state.AllProducts[getIndex].available = response.Data;
					})
					.catch((error) => {
						this.setState({ showProgressBar: false })
					})
					.done();
			} else {
				Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
				this.setState({ showProgressBar: false })
			}
		}
	
		doInfinte = () => {
			let pageCount = this.state.pagesCount + 20
			this.setState({ pagesCount: pageCount, bottomLoader: true, isScroll: true })
			this.setState({ isScroll: true })
			this.searchProductsAPI(this.state.filterType,this.state.priceFilterOn,this.state.sortingOrder)
		}
		scrollProd = () => {
		}
		LoadCartCountAPI = () => {
			
			if (this.state.connection_status) {
				var url = Api.baseUrl + Api.verifyPin
				let mUserData = {
					"CartDataByUser": "CartDataByUser",
					"uid": this.state.userUID
				};
	
				fetch(url, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
					},
					body: JSON.stringify(mUserData),
				})
					.then((response) => response.json())
					.then((response) => {
						if (response == 'blank') {
	
							this.setState({ CartCount: 0 })
						}
	
						if (response.ProductsDetail != undefined) {
							this.state.CartItems = [];
							this.setState({ CartItems: response.ProductsDetail })
							//	this.setState({ currentCartVisible: response.ProductsDetail })
							//	this.setState({ CartCount: this.state.CartItems.length })
	
							this.setState({ showArray1: true })
							var totalValue = 0.0
	
	
							for (let i = 0; i < this.state.CartItems.length; i++) {
								//totalValue = totalValue + Number.parseFloat(this.state.CartItems[i].Total_val_enc)
	
								//   let totalValue1 = totalValue
	
								//   this.setState({ totalValue2: totalValue1 });
								// }
								// this.setState({ FinalTotal: this.state.totalValue2 });
								var stringToConvert = this.state.CartItems[i].Total_val_enc
								var numberValue = JSON.parse(stringToConvert);
								totalValue = totalValue + numberValue
	
							}
	
							//		this.setState({ FinalTotal: totalValue.toFixed(2) });
							this.setState({ AddTocartLoading: false })
	
							// this.setState({ showProgressBar: false })
							//this.setState({ showProgressBar: false })
	
						} else {
							this.setState({ AddTocartLoading: false })
							//this.setState({ showProgressBar: false })
						}
	
					})
					.catch((error) => {
						this.setState({ showProgressBar: false })
						this.setState({ AddTocartLoading: false })
						//Toast.showWithGravity("Something went wrong", Toast.SHORT, Toast.CENTER);;
					})
					.done();
			} else {
				Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
				this.setState({ showProgressBar: false })
			}
		}
	
		//jump to drawer
		clickHamburgerMenu = () => {
			this.setState({ showDrawer: !this.state.showDrawer })
		}
	
		// JumpToProductDetais = (item) => {
	
		// 	this.props.navigation.navigate('ProductDetais', {
	
		// 		param_productDetails: item
		// 	})
	
		// }
	
		JumpToSearch = () => {
			this.props.navigation.navigate('Search')
		}
	
		gotoCartPage = () => {
			this.props.navigation.navigate('MyCart')
		}
	
		onRefresh = () => {
			this.setState({ refreshing: true });
			this.getUserData();
		}
	
		_backRefresh = () => {
			//this.setState({ AllProducts: [] })
			if(this.state.filterType != '' || this.state.priceFilterOn != '' || this.state.sortingOrder != ''){
				this.searchProductsAPI(this.state.filterType,this.state.priceFilterOn,this.state.sortingOrder)
			}
			else{
				this.searchProductsAPI()
			}
			this.getUserData();
			this.callLocalcart()
	
			if (this.state.showArray1 == true) {
				this.componentDidMount()
				//.LoadCartCountAPI();
			}
		}
	
		removeFromCart(itemId) {
			var newData = this.state.currentCartVisible;
			var updatedData = [];
			newData.map((item) => {
				if (item.PID !== itemId) {
					updatedData.push(item);
				}
			});
			this.setState({ currentCartVisible: updatedData })
			return updatedData;
		}
	
		// RemoveApi
		RemoveApi = (item, id) => {
	
	
			Alert.alert(
				"Delete?",
				"Are you sure you want to Delete selected item?",
				[
					{
						text: "No",
						style: "cancel"
					},
					{
						text: "YES", onPress: () => {
	
	
							{
	
								let newLst = this.state.currentCartVisible.filter(arr => arr.PID !== item.PID)
								
								this.setState({ currentCartVisible: newLst })
								this.caluculate()
								this._backRefresh()
							}
	
						}
					}
				],
				{ cancelable: false }
			);
		}
	
	
	
	
		toAddcart = (items,) => {
			let item = items
			this.setState({ AddTocartLoading: true })
	
	
			var price = item.pro_price_data
			item.Product_Unit = price[0].productprice_qty
			item.qty = parseInt(price[0].productprice_qty)
			this.AddToCartApi(item)
		}
		updatmData = (id, items) => {
			var currentCartVisible = this.state.currentCartVisible
			currentCartVisible.map((item) => {
				if (item.PID === id) {
					item.Total_val_enc = items.Total_val_enc
					item.qty = items.qty
					item.qty = items.qty
					item.single_price = items.single_price
					item.unit = items.unit
					item.priceData = items.priceData
				}
			});
			this.forceUpdate();
			this.setState({ currentCartVisible: currentCartVisible })
			setTimeout(() => {
				this.caluculate()
			}, 100);
		}
	
	
		Increment = (items, id) => {
			let item = items
			let decidePriceArr = []
			var pricepro = item.priceData
			var upQty = 0
			let price = item.pro_price_data
	
			if (price.length > 1) {
				pricepro = ''
	
				decidePriceArr = price
				decidePriceArr = decidePriceArr.sort((firstProduct, secondProduct) => {
					return (parseInt(firstProduct.productprice_qty) - parseInt(secondProduct.productprice_qty))
				})
	
				let count = 0
				upQty = parseInt(item.qty) + parseInt(decidePriceArr[0].productprice_qty)
	
				for (let product of decidePriceArr) {
	
					if (parseInt(upQty) === parseInt(product.productprice_qty)) {
						pricepro = product
						// upQty = parseInt(item.qty) + parseInt(pricepro.productprice_qty)
					}
					else if (parseInt(upQty) < parseInt(product.productprice_qty) && count !== 0) {
						pricepro = decidePriceArr[count - 1]
						break;
					} else if (decidePriceArr.length === count + 1) {
						pricepro = product
					}
	
					count += 1
	
				}
	
			} else {
				upQty = parseInt(item.qty) + parseInt(pricepro.productprice_qty)
			}
	
	
			var Total_val_enc = upQty * pricepro.productprice_ppc
	
			if (parseInt(item.Qty_avail) >= upQty) {
				item.qty = upQty
				item.Total_val_enc = upQty * pricepro.productprice_ppc
				item.single_price = pricepro.productprice_ppc
				item.unit = pricepro.productprice_unit
				item.priceData = pricepro
				this.updatmData(item.PID, item);
			}
	
		}
		Decrement = (item, id) => {
			var pricepro = item.priceData
			var price = item.pro_price_data
			let decidePriceArr = []
			var upQty = 0
	
			if (price.length > 1) {
				pricepro = ''
	
				decidePriceArr = price
				decidePriceArr = decidePriceArr.sort((firstProduct, secondProduct) => {
					return (parseInt(firstProduct.productprice_qty) - parseInt(secondProduct.productprice_qty))
				})
	
				let count = 0
				upQty = parseInt(item.qty) - parseInt(decidePriceArr[0].productprice_qty)
	
				for (let product of decidePriceArr) {
	
					if (decidePriceArr.length === 1) {
						pricepro = product
					} else if (parseInt(upQty) === parseInt(product.productprice_qty)) {
						pricepro = product
					}
					else if (parseInt(upQty) < parseInt(product.productprice_qty) && count !== 0) {
						pricepro = decidePriceArr[count - 1]
						break;
					} else if (decidePriceArr.length === count + 1) {
						pricepro = product
					} else if (upQty === 0) {
						//upQty = parseInt(item.qty) - parseInt(decidePriceArr[count].productprice_qty)
						pricepro = decidePriceArr[count - 1]
					}
	
					count += 1
	
				}
	
			} else {
				upQty = parseInt(item.qty) - parseInt(pricepro.productprice_qty)
			}
	
			var Total_val_enc = upQty * pricepro.productprice_ppc
			if (upQty > 0) {
				item.qty = upQty
				item.Total_val_enc = upQty * pricepro.productprice_ppc
				item.single_price = pricepro.productprice_ppc
				item.unit = pricepro.productprice_unit
				item.priceData = pricepro
				this.updatmData(item.PID, item);
			} else {
				this.RemoveApi(item)
				// let newLst = this.state.currentCartVisible.filter(arr => arr.PID !== item.PID)
				// this.setState({ currentCartVisible: newLst })
				// this.caluculate()
			}
		}
	
		changeQuantityDirect = (item, id,value) => {
			var pricepro = item.priceData
			var price = item.pro_price_data
			let decidePriceArr = []
			var upQty = 0
			if (price.length > 1) {
				pricepro = ''
	
				decidePriceArr = price
				decidePriceArr = decidePriceArr.sort((firstProduct, secondProduct) => {
					return (parseInt(firstProduct.productprice_qty) - parseInt(secondProduct.productprice_qty))
				})
	
				let count = 0
			if(parseInt(value) <= parseInt(item && item.Qty_avail) && parseInt(value) % parseInt(item && item.pro_price_data[0] && item.pro_price_data[0].productprice_qty) == 0){
				upQty = parseInt(value) 
			}
			else{
				Toast.showWithGravity('EnterValidData'+parseInt(item && item.pro_price_data[0] && item.pro_price_data[0].productprice_qty)+'AndUnderValid', Toast.SHORT, Toast.CENTER)
				upQty = parseInt(item && item.pro_price_data[0] && item.pro_price_data[0].productprice_qty)
			}
	
				for (let product of decidePriceArr) {
	
					if (decidePriceArr.length === 1) {
						pricepro = product
					} else if (parseInt(upQty) === parseInt(product.productprice_qty)) {
						pricepro = product
					}
					else if (parseInt(upQty) < parseInt(product.productprice_qty) && count !== 0) {
						pricepro = decidePriceArr[count - 1]
						break;
					} else if (decidePriceArr.length === count + 1) {
						pricepro = product
					} else if (upQty === 0) {
						//upQty = parseInt(item.qty) - parseInt(decidePriceArr[count].productprice_qty)
						pricepro = decidePriceArr[count - 1]
					}
	
					count += 1
	
				}
	
			} else {
				if(parseInt(value) <= parseInt(item && item.Qty_avail) && parseInt(value) % parseInt(item && item.pro_price_data[0] && item.pro_price_data[0].productprice_qty) == 0){
					upQty = parseInt(value) 
				}
				else{
					Toast.showWithGravity('EnterValidData'+parseInt(item && item.pro_price_data[0] && item.pro_price_data[0].productprice_qty)+'AndUnderValid', Toast.SHORT, Toast.CENTER)
					upQty = parseInt(item && item.pro_price_data[0] && item.pro_price_data[0].productprice_qty)
				}
			}
	
		
			var Total_val_enc = upQty * pricepro.productprice_ppc
			if (upQty > 0) {
				item.qty = upQty
				item.Total_val_enc = upQty * pricepro.productprice_ppc
				item.single_price = pricepro.productprice_ppc
				item.unit = pricepro.productprice_unit
				item.priceData = pricepro
				this.updatmData(item.PID, item);
			} else {
				let newLst = this.state.currentCartVisible.filter(arr => arr.PID !== item.PID)
				this.setState({ currentCartVisible: newLst })
				this.caluculate()
			}
			this.onRefresh()
		}
	
		// isAlreadyopen(itemId) {
		// 	var found = false;
		// 	this.state.currentCartVisible.map((item) => {
		// 		if (item !== null && item.PID === itemId) {
		// 			found = true;
		// 		}
		// 	});
		// 	return found;
		// }
	
		isAlreadyInLiveCart(itemId) {
			var found = false;
			this.state.CartItems.map((item) => {
				if (item !== null && item.PID === itemId) {
					found = true;
				}
			});
			return found;
		}
		isAlreadyInLiveCartRowid(itemId) {
			var found = false;
			this.state.CartItems.map((item) => {
				if (item !== null && item.PID === itemId) {
					found = item;
				}
			});
			return found;
		}
		isAlreadyInLiveCartCount(itemId) {
			var found = false;
			this.state.currentCartVisible.map((item) => {
				if (item !== null && item.PID === itemId) {
					found = item;
				}
			});
			return found;
		}
	
	
		getItemLiveCart(itemId) {
			var found = false;
			this.state.CartItems.map((item) => {
				if (item.PID === itemId) {
					found = item;
				}
			});
			return found;
		}
	
		updatePriceData(itemId, data) {
			var found = false;
			var newData = this.state.currentCartVisible;
			var updatedItem = '';
			newData.map((item) => {
				if (item.PID === itemId) {
					item.priceData = data
					updatedItem = item;
				}
			});
			this.setState({ currentCartVisible: newData })
			return updatedItem;
		}
	
		updateItemData(itemId, item_) {
			var found = false;
			var newData = this.state.currentCartVisible;
			this.state.currentCartVisible = [];
			var updatedItem = '';
			newData.map((item) => {
				if (item.PID === itemId) {
					item = item_
					updatedItem = item;
				}
			});
			this.setState({ currentCartVisible: newData })
			setTimeout(() => {
				this.caluculate()
			}, 100);
			//	return updatedItem;
		}
	
		getItem(itemId) {
			var found = "";
			this.state.currentCartVisible.map((item) => {
				if (item.PID === itemId) {
					found = item;
				}
			});
			return found;
		}
		LoadProductPriceAPI = (item) => {
			if (parseFloat(item.priceData.productprice_qty) <= parseFloat(item.Qty_avail)) {
				var newItem = false;
				var currentCartVisible = this.state.currentCartVisible;
				var data = {};
				if (!this.isAlreadyopen(item.PID)) {
					var price = item.pro_price_data
					let pricepro = price[0]
	
					item.Total_val_enc = pricepro.productprice_ppc * pricepro.productprice_qty;
					item.qty = pricepro.productprice_qty;
					item.single_price = pricepro.productprice_ppc
					item.unit = item.priceData.productprice_unit
					item.priceData = pricepro
	
				}
				currentCartVisible.push(item)
	
				this.setState({ currentCartVisible: currentCartVisible })
				setTimeout(() => {
					this.caluculate(currentCartVisible)
	
				}, 100);
			} else {
				Toast.showWithGravity("Product is out of stock", Toast.SHORT, Toast.CENTER)
			}
	
		}
	
		gettotalvalinc = (item) => {
	
			var price = JSON.parse(item.pro_price_data)
			let proPriceData = price[0]
			var cal = proPriceData.productprice_ppc * proPriceData.productprice_qty;
	
			return proPriceData
	
		}
		caluculate = () => {
			//this.props.loadData()
			this.props.allData.setState({ CartCount: this.state.currentCartVisible.length })
			var totalValue = 0.0
			for (let i = 0; i < this.state.currentCartVisible.length; i++) {
				var stringToConvert = this.state.currentCartVisible[i].Total_val_enc
				var numberValue = JSON.parse(stringToConvert);
				totalValue = totalValue + numberValue
			}
			this.props.allData.setState({ FinalTotal: totalValue.toFixed(2) }); 
			Util.setAsyncStorage('MY_LOCAL_CART', this.state.currentCartVisible)
			this.forceUpdate();
		}
	
		onEndReached = ({ distanceFromEnd }) => {
			if (!this.state.bottomLoader) {
				this.doInfinte();
			}
		}
	
		gotoTop = () => {
			this.flatListRef.scrollToOffset({animated: true, offset:0})
		}
	
		getValue(maxQuanti, item , liverowid,enterData, minimumData){
			var reminder = enterData % minimumData
			var data = reminder!= 0 ? enterData - reminder : maxQuanti + minimumData
			Toast.showWithGravity('EnterValidData'+minimumData+'AndUnderValid', Toast.SHORT, Toast.CENTER)
			this.changeQuantityDirect(item, liverowid, data)
			this.forceUpdate()
		}

}
