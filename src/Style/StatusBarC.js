import React, { Component } from 'react';
import {
  View, Text, StyleSheet, Image, TouchableOpacity, Dimensions, YellowBox, Alert,
  FlatList, TextInput, BackHandler, ActivityIndicator, ImageBackground, SafeAreaView,
  PixelRatio, Share, KeyboardAvoidingView, Platform, StatusBar, ScrollView, Button
  , PermissionsAndroid
} from 'react-native'

export default class StatusBarC extends Component {
  constructor(props) {
    super(props);
    this.state = {
    
    };
  }

  render() {
    return (
      <View>
         {/* for status bar color */}
         <StatusBar
            barStyle="light-content"
            hidden={false}
            backgroundColor='#00285A'
            translucent={false} />

      </View>
    );
  }
}
