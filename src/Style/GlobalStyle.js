import React, { Component } from "react";
import { StyleSheet, Dimensions, } from "react-native";
import Theme from '../Style/Theme'
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
import {verticalScale,moderateScale,scale} from 'react-native-size-matters'
export default StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: Theme.color_bgColor,
  },
  imageBackgroundStyle: {
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    flex: 1,
  },
  Center: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  TxtCenter: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },

  dropdownStyle: {
    margin: 10,
    backgroundColor: '#FFF',
    width: '100%',
    flexDirection: "row",
    alignSelf: 'center',
    paddingLeft: 15,
    marginTop: 20,
    paddingRight: 15,
    justifyContent: 'space-between',
    borderColor: '#707070',
    borderRadius: 5,
    ...Platform.select({
      ios: {
        borderWidth: 0.25,
        padding: 15,
        shadowColor: '#0000000D',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 3,
        elevation: 3,
        flexDirection: "row"
      },
      android: {
        shadowColor: '#0000000D',
        shadowOffset: { width: 10, height: 5 },
        shadowOpacity: 10,
        shadowRadius: 4,
        elevation: 4,
        flexDirection: "row"
      }
    })
  },

  // inner Header style

  ImgBox: {
		width: verticalScale(25),
		height: verticalScale(25),
		resizeMode: 'contain',
		tintColor: Theme.color_blue,
		alignSelf: 'center',
		alignItems: 'center',
	},
	OuterImgBox: {
		alignSelf: 'center', alignItems: 'center', marginTop: 5
	}, outerHeader:
	{
		flexDirection: 'row',
		justifyContent: 'space-between',
		width: '100%',
		height: verticalScale(40),
		alignSelf: 'center'
  },

  // blank flalist

  thumbnailMain: {
    flex: 1,
    margin: 8,
    borderRadius: 4,
    borderWidth: 0.5,
  },

  thmbnail_box: {
    height: verticalScale(120),
    //  marginTop:10,
    flexDirection: 'column',
    resizeMode: 'contain',
    justifyContent: 'center',
    alignSelf: 'center',
  },

  inVisibleStyle: {
    flex: 1
  },

  thumbnail: {
    alignSelf: 'center',
    textAlign: 'center',
    color: '#000',
    marginTop:5,
    marginBottom:10

  },

  /////////////////// Clear ////////////////////

  clear5: {
    flex: 1,
    height: verticalScale(5)
  },
  clear10: {
    flex: 1,
    height: verticalScale(10)
  },
  clear15: {
    flex: 1,
    height: verticalScale(15)
  },
  clear20: {
    flex: 1,
    height: verticalScale(20)
  },


  /////////////////fonts //////////////

  TIMES_Bold12: {
    fontFamily: "TIMESBD0",
    fontSize: moderateScale(12),
    color: "#000",
    fontWeight: 'bold'
  },

  TIMES_Regular12: {
    fontFamily: "TIMES",
    fontSize: moderateScale(12),
    color: "#000"
  },
  TIMES_Bold14: {
    fontFamily: "TIMESBD0",
    fontSize: moderateScale(14),
    color: "#000",
    fontWeight: 'bold'
  },

  TIMES_Regular14: {
    fontFamily: "TIMES",
    fontSize: moderateScale(14),
    color: "#000"
  },
  TIMES_Bold16: {
    fontFamily: "TIMESBD0",
    fontSize: moderateScale(16),
    color: "#000",
    fontWeight: 'bold'
  },

  TIMES_Regular16: {
    fontFamily: "TIMES",
    fontSize: moderateScale(16),
    color: "#000"
  },
  TIMES_Bold18: {
    fontFamily: "TIMESBD0",
    fontSize: moderateScale(18),
    color: "#000",
    fontWeight: 'bold'
  },

  TIMES_Regular18: {
    fontFamily: "TIMES",
    fontSize: moderateScale(18),
    color: "#000"
  },

  TIMES_Bold20: {
    fontFamily: "TIMESBD0",
    fontSize: moderateScale(20),
    color: "#000",
    fontWeight: 'bold'
  },

  TIMES_Regular20: {
    fontFamily: "TIMES",
    fontSize: moderateScale(20),
    color: "#000"
  },

  underline: {
    textDecorationLine: 'underline'
  },

  textInputSignin: {
    // borderWidth:1,
    // borderColor:Theme.color_gray,
    height:verticalScale(45),
    fontFamily: "TIMES",
    fontSize: moderateScale(16),
    color: "#000",
    paddingLeft: 15,

  },
  dropdownStyle: {
    backgroundColor: '#fff',
    width: '100%',
    alignSelf: 'center',
    borderWidth: 1,
    borderColor: Theme.color_gray,
    height: verticalScale(50),
    borderRadius: 5,
    width: '100%',
    alignSelf: 'center',
    marginVertical: 7,
    //elevation: 4
  },
  cartCircle: {
    backgroundColor: Theme.secondary,
    width: verticalScale(22),
    height: verticalScale(22),
    borderRadius: 50,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop:10,
    // marginLeft: -14,
    position:'absolute'
  },
  imageBackgroundStyle: {
    width: '100%',
    height: verticalScale(200),
    resizeMode: "contain"
  },
  plusStyle:{
    marginRight: 15, 
    width:verticalScale(35) 
  },
  gotoTopBtn: {
		backgroundColor: 'white',
		width: verticalScale(50),
		height: verticalScale(50),
		borderRadius:verticalScale(50),
		position: "absolute",
		bottom: verticalScale(70),
		right: verticalScale(15),
		elevation:5
	},
	gotoTopImg: {
		width: verticalScale(30),
		height: verticalScale(30),
		left: verticalScale(10),
		top:verticalScale(10)
	}


});