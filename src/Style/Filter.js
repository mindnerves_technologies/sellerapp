import React, { Component } from 'react';
import {
	View, Text, StyleSheet, Image, TouchableOpacity, Dimensions, YellowBox, Alert,
	FlatList, TextInput, BackHandler, ActivityIndicator, ImageBackground, SafeAreaView,
	PixelRatio, Share, KeyboardAvoidingView, Platform, StatusBar, ScrollView, Button
	, PermissionsAndroid
} from 'react-native'
import global_style from './GlobalStyle';
import Images from "../Constant/Images";
import Theme from './Theme';
import Api from "../Constant/Api";
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';
import DeviceInfo from 'react-native-device-info';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import {verticalScale,moderateScale,scale} from 'react-native-size-matters'

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;


export default class Filter extends Component {

	constructor(props) {
		super(props);
		this.state = {
			connection_status: false,
			showDrawer: false,
			userUID: '',
			userLID: '',
			searchTerm: '',
			selectedFilter: '',
			showArray: false,
			filterList: ['High To Low', 'Low To High']
		};
	}
	componentDidMount = () => {
		//To get the network state once
		NetInfo.addEventListener(state => {
			if (state.isConnected) {
				this.setState({ connection_status: true });
				//call API

			} else {
				this.setState({ connection_status: false });
			}
		});
		var VersionCode = DeviceInfo.getVersion()
		setTimeout(() => {
			this.getUserData();
		}, 50);
	}
	getUserData = async () => {
		try {
			let user_data = await AsyncStorage.getItem('userData');
			let get_data = JSON.parse(user_data);
			this.setState({ userLID: get_data.userLID })
			this.setState({ userUID: get_data.userId })
		} catch (e) {
			let user_loc = await AsyncStorage.getItem('user_location');
			let get_token = JSON.parse(user_loc);
			this.state.userLID = get_token.LocationId;
		}
	}

	LoadSubCategoryAPI = (searchKey) => {
		if (searchKey.length > 2) {
			if (this.state.connection_status) {
				var url = Api.baseUrl + Api.SubCategoryApi
				let mUserData = {
					"ProductSearch": "ProductSearch",
					"term": searchKey,
					"LID": this.state.userLID
				};
				fetch(url, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
					},
					body: JSON.stringify(mUserData),
				})
					.then((response) => response.json())
					.then((response) => {
						let bigCities = [];
						for (let i = 0; i < response.length; i++) {
							if (response[i].pstatus === 'Y') {
								bigCities.push(response[i]);
							}
						}
						this.setState({ AllProducts: bigCities });
						this.setState({ showArray: true })
						this.setState({ showProgressBar: false })
					})
					.catch((error) => {
						this.setState({ showProgressBar: false })
					})
					.done();
			} else {
				Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
				this.setState({ showProgressBar: false })
			}
		} else {
			this.setState({ showArray: false })
		}
	}

	searchUpdated = (term) => {
		this.getUserData();
		this.setState({ searchTerm: term })
		if (this.state.searchTerm == "") {
			this.setState({ showArray: false })
		} else {
			this.LoadSubCategoryAPI(term)
		}
	}

	clearSearchItemField = () => {
		this.setState({ searchTerm: '' })
		this.forceUpdate()
		this.setState({ showArray: false })

		// this.state.subcategoryArray = this.state.tempSubcategory;
	}
	JumpToProductDetais = (item, index) => {
		this.props.global_navigation.navigate('ProductDetais', {
			param_productDetails: item
		})
	}
	doClearFilter = () => {
		this.setState({ selectedFilter: '' })
		this.props.filterClassobjt.setState({ priceFilterOn: '', pagesCount: 0, sortingOrder: '', AllProducts: [],filterType:'' })
		setTimeout(() => {
			this.props.from =='dashboard' && this.props.filterClassobjt.callSearchRef();
			this.props.from!='dashboard' && this.props.filterClassobjt.LoadSubCategoryAPI(false, 0);
			this.setState({ showArray: false })

		}, 200);
		this.hideMenu()
	}
	doApplyFilter = (item, index) => {
		this.setState({ showArray: false })

	}


	getFilterData = (item, index) => {
		this.setState({ selectedFilter: item })
		// if (item == 'Brands') {
		// 	this.props.filterClassobjt.setState({ priceFilterOn: 'AlphabetFilter', pagesCount: 0, sortingOrder: 'AToZ' , AllProducts: [], filterType: 'Brands' })
		// }
		// else
		if (item == 'Discounts') {
			this.props.filterClassobjt.setState({ priceFilterOn: 'PriceFilter', pagesCount: 0, sortingOrder: 'AToZ' , AllProducts: [], filterType: 'Discounts' })
		}
		else if (index > 1) {
			this.props.filterClassobjt.setState({ priceFilterOn: 'AlphabetFilter', pagesCount: 0, sortingOrder: index === 2 ? 'AToZ' : '', AllProducts: [], filterType:'Alphabetic' })
		} else {
			this.props.filterClassobjt.setState({ priceFilterOn: 'PriceFilter', pagesCount: 0, sortingOrder: index === 0 ? 'HTOL' : '', AllProducts: [], filterType:'Price' })
		}
		setTimeout(() => {
			this.props.from!='dashboard' && this.props.filterClassobjt.LoadSubCategoryAPI(false, 0);
			this.props.from =='dashboard' && this.props.filterClassobjt.callSearchRef();
			this.setState({ showArray: false })
		}, 200);
		//this.setState({ showArray: false })
		this.hideMenu()
	}

	_menu = null;

	setMenuRef = ref => {
	  this._menu = ref;
	};
  
	hideMenu = () => {
	  this._menu.hide();
	};
  
	showMenu = () => {
	  this._menu.show();
	};

	render() {
		const { selectedFilter } = this.state;
		return (
			<View style={{ minHeight: verticalScale(34),backgroundColor: Theme.color_blue }}>
				{/* <ScrollView style={{ width: '100%', backgroundColor: Theme.color_light_gray, }} horizontal={true} showsHorizontalScrollIndicator={false}>
					<View

						style={{ alignItems: 'center', paddingHorizontal: 10, flexDirection: 'row', justifyContent: 'space-evenly' }}>
						<TouchableOpacity onPress={() => this.doClearFilter()} style={{marginRight:5}}>
							<Text style={{ fontSize: 18, color: selectedFilter == '' ? Theme.color_white : Theme.color_blue, textAlign: 'center' }}>Clear</Text>
						</TouchableOpacity>
						<TouchableOpacity style={{ height: 24, marginHorizontal: 5, width: 100, borderWidth: 1, borderColor: selectedFilter == 'HTOL' ? 'red' : 'black', alignItems: 'center', borderRadius: 10 }} onPress={() => this.getFilterData('HTOL', 0)} >
							<Text style={{ color: 'black', padding: 0 }}>Hight To Low</Text>
						</TouchableOpacity>
						<TouchableOpacity style={{ height: 24, marginHorizontal: 5, width: 100, borderWidth: 1, borderColor: selectedFilter == 'LTOH' ? 'red' : 'black', alignItems: 'center', borderRadius: 10 }} onPress={() => this.getFilterData('LTOH', 1)} >
							<Text style={{ color: 'black', padding: 0 }}>Low To High</Text>
						</TouchableOpacity>
						<TouchableOpacity style={{ height: 24, marginHorizontal: 5, width: 100, borderWidth: 1, borderColor: selectedFilter == 'AToZ' ? 'red' : 'black', alignItems: 'center', borderRadius: 10 }} onPress={() => this.getFilterData('AToZ', 2)} >
							<Text style={{ color: 'black', padding: 0 }}>A to Z</Text>
						</TouchableOpacity>
						<TouchableOpacity style={{ height: 24, marginHorizontal: 5, width: 100, borderWidth: 1, borderColor: selectedFilter == 'ZtoA' ? 'red' : 'black', alignItems: 'center', borderRadius: 10 }} onPress={() => this.getFilterData('ZtoA', 3)} >
							<Text style={{ color: 'black', padding: 0 }}>Z to A</Text>
						</TouchableOpacity>
					
						<TouchableOpacity style={{ height: 24, marginHorizontal: 10, width: 100, borderWidth: 1, borderColor: selectedFilter == 'Discounts' ? 'red' : 'black', alignItems: 'center', borderRadius: 10 }} onPress={() => this.getFilterData('Discounts', 5)} >
							<Text style={{ color: 'black', padding: 0 }}>Discount</Text>
						</TouchableOpacity>
						

					</View>
				</ScrollView> */}
				<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Menu
          	ref={this.setMenuRef}
						button={
				<TouchableOpacity style={{backgroundColor: Theme.color_blue, paddingVertical:verticalScale(11),paddingRight:30 }} onPress={this.showMenu}>
					<View style={{flexDirection:'row'}}>
						<Text style={[global_style.TIMES_Bold14,{color:'white',paddingLeft:verticalScale(10),paddingBottom:5}]}>{'Filter'}</Text>
					<Image
					source={Images.filterIcon}
					style={{marginLeft:5,top:4,width:verticalScale(15),height:verticalScale(15),resizeMode:'contain'}}/>
					</View>				
				</TouchableOpacity>
				
			}
        >
          	<MenuItem onPress={() => this.getFilterData('HTOL', 0)}><Text style={{fontSize:moderateScale(12)}}>{'PriceHightToLow'}</Text></MenuItem>
          	<MenuItem onPress={() => this.getFilterData('LTOH', 1)}><Text style={{fontSize:moderateScale(12)}}>{'PriceLowToHigh'}</Text></MenuItem>
         	<MenuItem onPress={() => this.getFilterData('AToZ', 2)}><Text style={{fontSize:moderateScale(12)}}>A to Z</Text></MenuItem>
			<MenuItem onPress={() => this.getFilterData('ZtoA', 3)}><Text style={{fontSize:moderateScale(12)}}>Z to A</Text></MenuItem>
			<MenuItem onPress={() => this.getFilterData('Discounts', 5)}><Text style={{fontSize:moderateScale(12)}}>{'Discount'}</Text></MenuItem>
			<MenuItem onPress={() => this.doClearFilter()}><Text style={{fontSize:moderateScale(12)}}>{'Clear'}</Text></MenuItem>
        </Menu>
      </View>
				{//this.state.showArray ?
					// <ScrollView style={{ width: '100%', height: '80%' }}>

					// 	<Modal
					// 		style={{ marginTop: 50, marginBottom: 50 }}
					// 		animationInTiming={5}
					// 		animationOutTiming={5}
					// 		animationType={'fade'}
					// 		isVisible={this.state.showArray}>

					// 		<View style={[global_style.Center]}>

					// 			<View style={{ width: '100%', backgroundColor: 'white', borderRadius: 5 }}>

					// 				<View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 20, backgroundColor: Theme.color_blue }}>

					// 					<Text
					// 						style={[global_style.Roboto_Bold_18, { color: Theme.color_white }]}>
					// 						{"Select Filter"}
					// 					</Text>


					// 					<TouchableOpacity
					// 						hitSlop={{ top: 30, bottom: 30, left: 30, right: 30 }}
					// 						onPress={() => this.setState({ showArray: false })}>
					// 						<AntDesign name='close' color='#fff' size={22} borderColor='#000' />

					// 					</TouchableOpacity>

					// 				</View>

					// 				<FlatList
					// 					marginLeft={5}
					// 					data={this.state.filterList}
					// 					showsHorizontalScrollIndicator={false}
					// 					showsVerticalScrollIndicator={false}
					// 					renderItem={({ item, index }) =>

					// 						<TouchableOpacity
					// 							style={{ borderBottomWidth: 1, flexDirection: 'row', justifyContent: 'space-between', padding: 10 }}
					// 							hitSlop={{ top: 10, left: 10, right: 10 }}
					// 							onPress={() => this.getFilterData(item, index)}
					// 						>
					// 							{/* <View style={{ height: 20, width: 20, borderRadius: 10,margin:10, justifyContent: 'center', alignItems: 'center', borderWidth: 2, borderColor: 'black' }}>
					// 								{this.state.selectedFilter === item && <View style={{ height: 10, width: 10, borderRadius: 5, alignSelf: 'center', backgroundColor: 'black', borderWidth: 2, borderColor: 'black' }}>
					// 								</View>}
					// 							</View> */}
					// 							<Text
					// 								style={[global_style.Roboto_regular16, { margin: 10 }]}>
					// 								{item}
					// 							</Text>


					// 						</TouchableOpacity>
					// 					} />

					// 				<View
					// 					style={{
					// 						backgroundColor: Theme.color_blue, flexDirection: "row", justifyContent: 'space-between',
					// 						padding: 10, alignItems: 'center'
					// 					}}>
					// 					<TouchableOpacity style={{
					// 						//alignItems:'center',
					// 						backgroundColor: Theme.color_white, justifyContent: 'center', alignContent: 'center', paddingLeft: 20
					// 						, width: "47%", height: 50, borderRadius: 5,
					// 					}} onPress={() => this.doClearFilter()}>

					// 						<Text style={[global_style.TIMES_Regular18, { alignSelf: 'center' }]}>{" Clear filter"}										</Text>
					// 					</TouchableOpacity>
					// 					<TouchableOpacity style={{
					// 						//alignItems:'center',
					// 						backgroundColor: Theme.color_white, justifyContent: 'center', alignContent: 'center', paddingLeft: 20
					// 						, width: "47%", height: 50, borderRadius: 5,
					// 					}} onPress={() => this.doApplyFilter()}>

					// 						<Text style={[global_style.TIMES_Regular18,]}>{" Apply Filter"}											</Text>
					// 					</TouchableOpacity>

					// 				</View>
					// 			</View>
					// 		</View>
					// 	</Modal>

					// </ScrollView> :
				//	null
				}
			</View>

		);
	}
}
