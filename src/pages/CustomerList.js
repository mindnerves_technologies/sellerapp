import React, { Component } from "react";
import { TouchableOpacity } from "react-native";
import { FlatList } from "react-native";
import {
    View,
    ScrollView,
    ActivityIndicator,
    Text,
    Dimensions,
    Linking,
    Image,TextInput,
} from "react-native";
import Theme from "../Style/Theme";
import SearchBar from '../Style/SearchBar'
import Modal from "react-native-modal";
import { deviceHeight, deviceWidth } from "./SplashScreen";
import Api from "../Constant/Api";
import Toast from 'react-native-simple-toast';
import global_style from '../Style/GlobalStyle';
import AntDesign from 'react-native-vector-icons/dist/AntDesign';
import {verticalScale,moderateScale,scale} from 'react-native-size-matters'
import NetInfo from "@react-native-community/netinfo";
import TabFooter from '../Style/TabFooter';

export default class CustomerList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            customerList:        [
                {
                    customerName:"chinmay bhadang",
                    customerShopName:"chinu shop",
                    customerAddress:"chinu address 1",
                    customerLandmark:"chinu Landmark 1",
                    customerMobileNo:"9302666588",
                },
                {
                    customerName:"chinmay bhadang 2",
                    customerShopName:"chinu shop 2",
                    customerAddress:"chinu address 2",
                    customerLandmark:"chinu Landmark 2",
                    customerMobileNo:"9302666588",
                },
                {
                    customerName:"chinmay bhadang 3",
                    customerShopName:"chinu shop 3",
                    customerAddress:"chinu address 3",
                    customerLandmark:"chinu Landmark 31",
                    customerMobileNo:"9302666588",
                },
                {
                    customerName:"chinmay bhadang",
                    customerShopName:"chinu shop",
                    customerAddress:"chinu address 34",
                    customerLandmark:"chinu Landmark 14",
                    customerMobileNo:"9302666588",
                },
                {
                    customerName:"chinmay bhadang",
                    customerShopName:"chinu shop 55",
                    customerAddress:"chinu address 155",
                    customerLandmark:"chinu Landmark 13 ",
                    customerMobileNo:"9302666588",
                },
                {
                    customerName:"chinmay bhadang",
                    customerShopName:"chinu shop #22",
                    customerAddress:"chinu address 16",
                    customerLandmark:"chinu Landmark 16",
                    customerMobileNo:"9302666588",
                },
            ],
            modalVisiblity:false,
            modalCustomerName:'',
            modalShopName:'',
            modalAddress:'',
            modalLandmark:'',
            modalMobileNo:'',
            city_array: [],
            connection_status: false,
            showProgressBar: false,
            showCityModel:false,
            LocationId: null,
            LocationName: '',
            showPincodeModel: false,
			pincodeArray: [],
            Pincode: '',
        };
    }

    componentDidMount = () => {
        NetInfo.addEventListener(state => {
			if (state.isConnected) {
				this.setState({ connection_status: true });
				//call API

			} else {
				this.setState({ connection_status: false });
			}
		});
        setTimeout(() => {
			this.LoadLocation();
		}, 100);
    }

    LoadLocation = () => {

		this.setState({ showProgressBar: true })

		if (this.state.connection_status) {

			var url = Api.baseUrl + Api.LocationApi

			let mUserData = {

				"MasterLocation": "MasterLocation",
				"Status": "Y"
			};

			fetch(url, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(mUserData),
			})
				.then((response) => response.json())
				.then((response) => {

					if (response.length > 0) {
						this.state.city_array = [];
                        console.log('$$$$$$$$$$$',response)
						this.setState({ city_array: response })
					}
					this.setState({ showProgressBar: false })
				})
				.catch((error) => {
					this.setState({ showProgressBar: false })
					//Toast.showWithGravity("Something went wrong", Toast.SHORT, Toast.CENTER);;
				})
				.done();
		} else {
			Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
			this.setState({ showProgressBar: false })
		}
	}

    LoadPin = () => {

		this.setState({ showProgressBar: true })

		if (this.state.connection_status) {

			var url = Api.baseUrl + Api.verifyPin

			let mUserData = {

				"PinCodeList": "PinCodeList",
				"LID": this.state.LocationId !== null ? this.state.LocationId : 4
			};

			fetch(url, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(mUserData),
			})
				.then((response) => response.json())
				.then((response) => {

					this.state.pincodeArray = [];

					this.setState({ pincodeArray: response.Data })
					this.setState({ pincodeArrayAllData: response.Data })

					this.setState({ showProgressBar: false })
				})
				.catch((error) => {
					this.setState({ showProgressBar: false })
					//Toast.showWithGravity("Something went wrong", Toast.SHORT, Toast.CENTER);;
				})
				.done();
		} else {
			Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
			this.setState({ showProgressBar: false })
		}
	}

    renderItem = ({item}) => {
        return(
            <TouchableOpacity onPress={()=> {this.props.navigation.navigate('ProductList')}}>
                <View style={{borderWidth:1,borderRadius:10,paddingVertical:10,paddingHorizontal:10,borderBottomColor:Theme.color_blue,marginHorizontal:10,marginVertical:5}}>
                    <Text style={{fontSize:18,color:Theme.color_blue,marginBottom:5}}>
                        {item.customerName}
                    </Text>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{color:Theme.color_blue,fontWeight:'bold'}}>
                            {'Shop: '}
                        </Text>
                        <Text>
                            {item.customerShopName}
                        </Text>
                    </View>
                   
                    <View style={{flexDirection:'row'}}>
                        <Text style={{color:Theme.color_blue,fontWeight:'bold'}}>
                            {'Address: '}
                        </Text>
                        <Text>
                            {item.customerAddress}
                        </Text>
                    </View>

                    <View style={{flexDirection:'row'}}>
                        <Text style={{color:Theme.color_blue,fontWeight:'bold'}}>
                            {'Landmark: '}
                        </Text>
                        <Text>
                            {item.customerLandmark}
                        </Text>
                    </View>

                    <View style={{flexDirection:'row'}}>
                        <Text style={{color:Theme.color_blue,fontWeight:'bold'}}>
                            {'Mobile No: '}
                        </Text>
                        <Text>
                            {item.customerMobileNo}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    searchData = () => {

    }

    saveData = () => {
        let customerList = this.state.customerList
        let objectToAdd = {
            customerName:this.state.modalCustomerName,
            customerShopName:this.state.modalShopName,
            customerAddress:this.state.modalAddress,
            customerLandmark:this.state.modalLandmark,
            customerMobileNo:this.state.modalMobileNo,
        }
        customerList.push(objectToAdd)
        this.setState({
            customerList:customerList,
            modalVisiblity:false,
        })
    }

    cancelData = () => {
        this.setState({
            modalCustomerName:'',
            modalShopName:'',
            modalAddress:'',
            modalLandmark:'',
            modalMobileNo:'',
            modalVisiblity:false,
        });
    }

    getCity = (item) => {
		this.setState({ LocationName: item.LocationName })
		this.setState({ LocationId: item.LocationID })

		this.forceUpdate();

		this.setState({ showCityModel: false })
		setTimeout(() => {
			this.LoadPin();
		}, 100);
	}

    SearchFilterFunction(text) {
		let newData = this.state.pincodeArrayAllData.filter(function (item) {
			//..  const itemData = item.que ? item.que.toUpperCase() : ''.toUpperCase();
			//const textData = text.toUpperCase();
			return item.indexOf(text) > -1;
		});
		this.setState({
			pincodeArray: newData,
			text: text,
		});
	}

    getpincode = (item) => {
		this.setState({ Pincode: item })
		this.PinVarification(item);
		this.setState({ showPincodeModel: false })
	}

    PinVarification = (mValue) => {

		this.setState({ showProgressBar: true })

		if (this.state.connection_status) {

			var url = Api.baseUrl + Api.verifyPin

			let mUserData = {

				"ValidateLocation": "ValidateLocation",
				"LocationID": this.state.LocationId,
				"PinCode": mValue

			};

			fetch(url, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(mUserData),
			})
				.then((response) => response.json())
				.then((response) => {

					if (response.msgsuc == "bg-success") {
						//	Toast.showWithGravity('Valid Pincode', Toast.SHORT, Toast.CENTER);
					} else {
						Toast.showWithGravity('InValid Pincode', Toast.SHORT, Toast.CENTER);
					}

					if (response.length > 0) {
						this.state.city_array = [];

						this.setState({ city_array: response })

					}
					this.setState({ showProgressBar: false })
				})
				.catch((error) => {
					this.setState({ showProgressBar: false })
					//Toast.showWithGravity("Something went wrong", Toast.SHORT, Toast.CENTER);;
				})
				.done();
		} else {
			Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
			this.setState({ showProgressBar: false })
		}
	}

    render() {
        return (
            <View style={{ flex: 1 }}>
                <SearchBar searchData={this.searchData} allData={this} global_navigation={this.props.navigation}/>
                <View>
                    <TouchableOpacity onPress={()=>{ this.setState({modalVisiblity:true}) }}>
                        <View style={{marginVertical:10,marginHorizontal:20}}>
                            <Text style={{textAlign:'center',color:'white',backgroundColor:Theme.color_blue,borderRadius:50,paddingVertical:10,paddingHorizontal:20}}>
                                {'Add a customer'}
                            </Text>
                        </View>
                    </TouchableOpacity>
                    <FlatList
                        data={this.state.customerList}
                        style={{marginBottom:180}}
                        renderItem={this.renderItem.bind(this)}
                    />
                    {/* modal for location */}
					<Modal
						style={{ marginTop: 50, marginBottom: 50 }}
						animationInTiming={5}
						animationOutTiming={5}
						animationType={'fade'}
						isVisible={this.state.showCityModel}>

						<View style={[global_style.Center]}>

							<View style={{ width: '100%', backgroundColor: 'white', borderRadius: 5 }}>

								<View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 15 }}>

									<Text
										style={[global_style.Roboto_Bold_18]}>
										{"Select Location"}
									</Text>

									<TouchableOpacity
										hitSlop={{ top: 30, bottom: 30, left: 30, right: 30 }}
										onPress={() => {
                                             this.setState({ showCityModel: false })} }>

										<AntDesign name='close' color='#000' size={verticalScale(22)} borderColor='#000' />

									</TouchableOpacity>

								</View>

								<FlatList
									marginLeft={5}
									data={this.state.city_array}
									showsHorizontalScrollIndicator={false}
									showsVerticalScrollIndicator={false}
									renderItem={({ item, index }) =>

										<TouchableOpacity
											hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
											onPress={() => this.getCity(item)}
										>

											<Text
												style={[global_style.Roboto_regular16, { margin: 10 }]}>
												{item.LocationName}
											</Text>


										</TouchableOpacity>
									} />

								<View style={{ marginBottom: 20 }}></View>

							</View>
						</View>
					</Modal>

                    {/* for pincode modal */}
					<Modal
						style={{ marginTop: 50, marginBottom: 50 }}
						animationInTiming={5}
						animationOutTiming={5}
						animationType={'fade'}
						isVisible={this.state.showPincodeModel}>

						<View style={[global_style.Center]}>

							<View style={{ width: '100%', paddingHorizontal: 10, backgroundColor: 'white', borderRadius: 5 }}>

								<View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 15, margin: 15 }}>

									<Text
										style={[global_style.Roboto_Bold_18]}>
										{"SelectPincode"}
									</Text>

									<TouchableOpacity
										style={{}}
										//hitSlop={{ top: 90, bottom: 90, left: 90, right: 90 }}
										onPress={() => this.setState({ showPincodeModel: false })}>

										<AntDesign name='close' color='#000' size={verticalScale(22)} borderColor='#000' />

									</TouchableOpacity>

								</View>
								<TextInput
									style={{ paddingHorizontal: 15, marginTop: 15, borderWidth: 2 }}
									onChangeText={text => this.SearchFilterFunction(text)}
									value={this.state.text}
									keyboardType={'numeric'}
									maxLength={6}
									underlineColorAndroid="transparent"
									placeholder={"Select Pincode"}
									onFocus={() => { this.setState({ searchFocus: true }) }}
								/>

								{this.state.pincodeArray !== null && this.state.pincodeArray.length > 0 ? <FlatList
									marginLeft={5}
									data={this.state.pincodeArray}
									showsHorizontalScrollIndicator={false}
									keyExtractor={(item, index) => index.toString()}
									showsVerticalScrollIndicator={false}
									renderItem={({ item, index }) =>

										<TouchableOpacity key={index * Math.random()}
											hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
											onPress={() => this.getpincode(item)}
										>
											<Text
												style={[global_style.Roboto_regular16, { margin: 10 }]}>
												{item}
											</Text>
										</TouchableOpacity>
									} /> : <Text
										style={[global_style.Roboto_regular16, { margin: 10 }]}>
									{'Service Not Aviliable for this Pincode.'}
								</Text>}

								<View style={{ marginBottom: 20 }}></View>
							</View>
						</View>
					</Modal>

                {/* modal for add new customer */}
                    <Modal
						isVisible={this.state.modalVisiblity}>
							<View style={{height:deviceHeight/1.2,width:deviceWidth/1.1,backgroundColor:'white',borderRadius:10}}>
                                <Text style={{textAlign:'center',color:'white',backgroundColor:Theme.color_blue,fontSize:16,fontWeight:'bold',paddingVertical:10,marginBottom:20,borderTopLeftRadius:10,borderTopRightRadius:10}}>
                                    {'Add New Customer'}
                                </Text>
                                <View style={{paddingHorizontal:10}}>
                                    <View style={{marginBottom:15}}>
                                        <Text style={{marginRight:8,color:'Theme.color_blue'}}>
                                            {'Customer Name'}
                                        </Text>
                                        <TextInput
                                            style={{color:'black',borderWidth:this.state.viewMode ? 0 : 1 ,paddingVertical:4,borderRadius:3,width:320,borderColor:'Theme.color_blue'}}
                                            textAlign={'left'}
                                            autoCapitalize='words'
                                            placeholder='Enter Customer Name'
                                            keyboardType="email-address"
                                            placeholderTextColor={'#cdcdcd'}
                                            editable={!this.state.viewMode}
                                            onChangeText={ text => this.setState( { modalCustomerName:text } ) }
                                            value={this.state.modalPrescribeName}
                                            multiline={true}
                                            // onFocus={() => { this.setState({ emailFocus: true }) }}
                                            // onBlur={() => { this.setState({ emailFocus: false }); this.handleEmailInputChange(email) }}
                                            // onChange={() => { this.checkAllFieldValid(); }}
                                            // placeholder={'Enter your status'}
                                        />
                                    </View>

                                    <View style={{marginBottom:15}}>
                                        <Text style={{marginRight:8,color:'Theme.color_blue'}}>
                                            {'Shop Name'}
                                        </Text>
                                        <TextInput
                                            style={{color:'black',borderWidth:this.state.viewMode ? 0 : 1 ,paddingVertical:4,borderRadius:3,width:320,borderColor:'Theme.color_blue'}}
                                            textAlign={'left'}
                                            autoCapitalize='words'
                                            placeholder='Enter Shop Name'
                                            keyboardType="email-address"
                                            placeholderTextColor={'#cdcdcd'}
                                            editable={!this.state.viewMode}
                                            onChangeText={ text => this.setState( { modalShopName:text } ) }
                                            value={this.state.modalPrescribeName}
                                            multiline={true}
                                            // onFocus={() => { this.setState({ emailFocus: true }) }}
                                            // onBlur={() => { this.setState({ emailFocus: false }); this.handleEmailInputChange(email) }}
                                            // onChange={() => { this.checkAllFieldValid(); }}
                                            // placeholder={'Enter your status'}
                                        />
                                    </View>

                                    <View style={{marginBottom:15}}>
                                        <Text style={{marginRight:8,color:'Theme.color_blue'}}>
                                            {'Mobile No'}
                                        </Text>
                                        <TextInput
                                            style={{color:'black',borderWidth:this.state.viewMode ? 0 : 1 ,paddingVertical:4,borderRadius:3,width:320,borderColor:'Theme.color_blue'}}
                                            textAlign={'left'}
                                            autoCapitalize='words'
                                            placeholder='Enter Mobile No.'
                                            keyboardType="email-address"
                                            placeholderTextColor={'#cdcdcd'}
                                            editable={!this.state.viewMode}
                                            onChangeText={ text => this.setState( { modalMobileNo:text } ) }
                                            value={this.state.modalPrescribeName}
                                            multiline={true}
                                            // onFocus={() => { this.setState({ emailFocus: true }) }}
                                            // onBlur={() => { this.setState({ emailFocus: false }); this.handleEmailInputChange(email) }}
                                            // onChange={() => { this.checkAllFieldValid(); }}
                                            // placeholder={'Enter your status'}
                                        />
                                    </View>

                                    <View style={{marginBottom:15}}>
                                        <Text style={{marginRight:8,color:'Theme.color_blue'}}>
                                            {'Address'}
                                        </Text>
                                        <TextInput
                                            style={{color:'black',borderWidth:this.state.viewMode ? 0 : 1 ,paddingVertical:4,borderRadius:3,width:320,borderColor:'Theme.color_blue'}}
                                            textAlign={'left'}
                                            autoCapitalize='words'
                                            placeholder='Enter Address'
                                            keyboardType="email-address"
                                            placeholderTextColor={'#cdcdcd'}
                                            editable={!this.state.viewMode}
                                            onChangeText={ text => this.setState( { modalAddress:text } ) }
                                            value={this.state.modalPrescribeName}
                                            multiline={true}
                                            // onFocus={() => { this.setState({ emailFocus: true }) }}
                                            // onBlur={() => { this.setState({ emailFocus: false }); this.handleEmailInputChange(email) }}
                                            // onChange={() => { this.checkAllFieldValid(); }}
                                            // placeholder={'Enter your status'}
                                        />
                                    </View>

                                    <View style={{marginBottom:15}}>
                                        <Text style={{marginRight:8,color:'Theme.color_blue'}}>
                                            {'Landmark'}
                                        </Text>
                                        <TextInput
                                            style={{color:'black',borderWidth:this.state.viewMode ? 0 : 1 ,paddingVertical:4,borderRadius:3,width:320,borderColor:'Theme.color_blue'}}
                                            textAlign={'left'}
                                            autoCapitalize='words'
                                            placeholder='Enter Landmark'
                                            keyboardType="email-address"
                                            placeholderTextColor={'#cdcdcd'}
                                            editable={!this.state.viewMode}
                                            onChangeText={ text => this.setState( { modalLandmark:text } ) }
                                            value={this.state.modalPrescribeName}
                                            multiline={true}
                                            // onFocus={() => { this.setState({ emailFocus: true }) }}
                                            // onBlur={() => { this.setState({ emailFocus: false }); this.handleEmailInputChange(email) }}
                                            // onChange={() => { this.checkAllFieldValid(); }}
                                            // placeholder={'Enter your status'}
                                        />
                                    </View>

                                    <View style={{marginBottom:15}}>
                                        <Text style={{marginRight:8,color:'Theme.color_blue'}}>
                                            {'Location'}
                                        </Text>
                                        <TouchableOpacity onPress={() => this.setState({ showCityModel: true })}>
                                            <TextInput
                                                    style={{color:'black',borderWidth:this.state.viewMode ? 0 : 1 ,paddingVertical:4,borderRadius:3,width:320,borderColor:'Theme.color_blue'}}
                                                    placeholder={"Enter your Location"}
                                                    placeholderTextColor={'#7E7E7E'}
                                                    placeholderStyle={[global_style.TIMES_Regular16,]}
                                                    returnKeyType={"next"}
                                                    ref="LocationName"
                                                    value={this.state.LocationName}
                                                    editable={false}
                                                    allowFontScaling={false} />
                                        </TouchableOpacity>
                                    </View>

                                    <View style={{marginBottom:15}}>
                                        <Text style={{marginRight:8,color:'Theme.color_blue'}}>
                                            {'Pincode'}
                                        </Text>
                                        <TouchableOpacity
                                            onPress={() => {
                                            this.state.pincodeArray.length > 0 ? this.setState({ showPincodeModel: true }) :
											Toast.showWithGravity("Please select city first", Toast.SHORT, Toast.CENTER);
                                        }}>
                                            <TextInput
                                                    style={{color:'black',borderWidth:this.state.viewMode ? 0 : 1 ,paddingVertical:4,borderRadius:3,width:320,borderColor:'Theme.color_blue'}}
                                                    placeholder={"Enter your Pincode"}
                                                    placeholderTextColor={'#7E7E7E'}
                                                    placeholderStyle={[global_style.TIMES_Regular16,]}
                                                    returnKeyType={"next"}
                                                    ref="pincode"
                                                    value={this.state.Pincode}
                                                    editable={false}
                                                    allowFontScaling={false} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{flexDirection:'row'}}>
                                    <TouchableOpacity 
                                            onPress={()=> this.cancelData() }
                                            style={{ marginTop:10,marginLeft:40}}> 
                                            <Text style={{borderWidth:1, borderRadius:100,fontWeight:'900', fontSize:16,paddingHorizontal:20, paddingVertical:10, color:Theme.color_blue}}>
                                                {'Cancel'}
                                            </Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity 
                                            onPress={()=> this.saveData() }
                                            style={{backgroundColor: Theme.color_blue, marginTop:10, borderRadius:100,marginLeft:30}}> 
                                            <Text style={{fontWeight:'900', fontSize:16,paddingHorizontal:30, paddingVertical:10, color:'#fff'}}>
                                                {'Save'}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                
						    </View>
					</Modal>
                </View>
                <TabFooter global_navigation={this.props.navigation} activePage="CustomerList" connection_status={this.state.connection_status} />
            </View>
        );
    }
}

