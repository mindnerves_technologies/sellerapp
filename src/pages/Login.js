import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from "@react-native-community/netinfo";
import React, { Component } from 'react';
import {
  BackHandler, Dimensions, Image, Keyboard,
  Pressable, SafeAreaView, ScrollView, StyleSheet, Text,
  TextInput, TouchableOpacity, View,Linking
} from 'react-native';
import Toast from 'react-native-simple-toast';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import Api from "../Constant/Api";
import Images from "../Constant/Images";
import global_style from '../Style/GlobalStyle';
import global from '../Style/GlobalValue';
import StatusBarC from '../Style/StatusBarC';
import Theme from '../Style/Theme';
import DeviceInfo from 'react-native-device-info';
import Modal from "react-native-modal";
import CommonActivityIndicator from '../util/ActivityIndicator';
import {verticalScale,moderateScale,scale} from 'react-native-size-matters'
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
var isValidEmail = false;

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            slug:undefined,
            showDrawer: false,
            connection_status: false,
            showProgressBar: false,
            showPasswordField: true,
            showUpdate:false,
            EmailId: '',
            newPassword: '',
            showfooter: true,
            VersionName: '',
            fcm_token: '',
            activityIndicator: true,
            appversion:''
        };
    }

    componentDidMount() {

        //To get the network state once
    
        this.willBlur = this.props.navigation.addListener("willBlur", payload =>
          BackHandler.removeEventListener("hardwareBackPress", this.onBack),
        );
    
        NetInfo.addEventListener(state => {
    
          if (state.isConnected) {
            this.setState({ connection_status: true });
    
          } else {
            this.setState({ connection_status: false });
          }
    
          this.getUserData();
    
        });
        this.keyboardDidShowListener = Keyboard.addListener(
          'keyboardDidShow',
          () => this.keyboardDidShow(),
        );
        this.keyboardDidHideListener = Keyboard.addListener(
          'keyboardDidHide',
          () => this.keyboardDismiss(),
        );
    
        var VersionCode = DeviceInfo.getVersion()
        let deviceId = DeviceInfo.getDeviceId();
        setTimeout(() => {
    
          this.setState({ VersionName: VersionCode })
          this.GeneralSettingApi()
        }, 50);
    
      }

      promptFun() {
        // if (this.state.VersionName == this.state.appversion) {
        //   this.setState({ showUpdate: false })
        // } else {
        //   InAppUpdate.checkUpdate() // this is how you check for update
        //   this.setState({ showUpdate: true })
        // }
      }

      getUserData = async () => {

        //this.state.fcm_token = await firebase.messaging().getToken();
    
      }
    
      UNSAFE_componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
      }
    
      componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
      }
    
      GeneralSettingApi = () => {
    
        //this.setState({ showProgressBar: true })
    
        if (this.state.connection_status) {
          var url = Api.baseUrl + Api.verifyPin
          let mUserData = {
            "StaticPages": "StaticPages",
            "Column": "GeneralSetting"
          };
          fetch(url, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(mUserData),
          })
            .then((response) => response.json())
            .then((response) => {
              
              let str = response.GeneralSetting 
              var strIntoObj = JSON.parse(str);
              this.setState({ appversion: strIntoObj.AppVersion })
              
              setTimeout(() => {
                this.promptFun();
              }, 200);
    
            })
            .catch((error) => {
              
            })
            .done();
        } else {
          Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
          this.setState({ showProgressBar: false })
        }
      }

    LoginApi = () => {
        if (this.state.EmailId == 0) {
          Toast.showWithGravity("Please Enter Email Or Mobile No", Toast.SHORT, Toast.CENTER);
        } else if (this.state.newPassword == 0) {
          Toast.showWithGravity("Please enter Password", Toast.SHORT, Toast.CENTER);
        } else if (this.state.newPassword.length < 6) {
          Toast.showWithGravity("Password Size Must Be 6 Digit", Toast.SHORT, Toast.CENTER);
        } else {
    
          this.setState({ activityIndicator: false })
          // return;
          if (this.state.connection_status) {
    
            var url = Api.baseUrl + Api.Login
    
            let mUserData = {
    
              "LoginC": this.state.EmailId,
              "LoginP": this.state.newPassword,
              "DeviceID": this.state.fcm_token
            };
    
            fetch(url, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(mUserData),
            })
              .then((response) => response.json())
              .then( async (response) => {
    
                if (response.msgsuc == "bg-success") {
    
                  if (response.Data !== null) {
    
    
                    let user_data = {
                      userId: response.Data.UID,
                      userFName: response.Data.FName,
                      userLName: response.Data.LName,
                      userAddress: response.Data.Address,
                      userContact: response.Data.Contact,
                      userEmail: response.Data.Email,
                      userShopName: response.Data.ShopName,
                      userGstno: response.Data.Gstno,
                      userPanno: response.Data.Panno,
                      userPincode: response.Data.Pincode,
                      userLID: response.Data.LID,
                      userStatus: response.Data.Status
                    }
                    //await firebase.analytics().setUserId(response.Data.UID.toString())
    
                   //this.LoadViewProfileAPI(response.Data.UID)
    
                    this.setState({ activityIndicator: true })
                    if (user_data.userStatus == 'Y') {
                      AsyncStorage.setItem('userData', JSON.stringify(user_data))
                      Toast.showWithGravity("Welcome To Goodluck Wholesale \n\t\t\t\t\t\t\t\t\t\t\t\t\t🙏🙏", Toast.LONG, Toast.CENTER)
                      
                      this.props.navigation.replace('CustomerList')  
                       
                    }
    
                    //this.props.navigation.goBack(null);
    
                  } else {
                    Toast.showWithGravity("data is empty", Toast.SHORT, Toast.CENTER);
                    this.setState({ activityIndicator: true })
                  }
    
    
                } else {
                  this.setState({ activityIndicator: true })
                  Toast.showWithGravity(response.msg, Toast.SHORT, Toast.CENTER);
                }
    
    
    
              })
              .catch((error) => {
                this.setState({ activityIndicator: true })
                Toast.showWithGravity("Something went wrong", Toast.SHORT, Toast.CENTER);;
              })
              .done();
    
          } else {
            Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
            this.setState({ activityIndicator: true })
          }
       }
    }

    keyboardDismiss = () => {
        this.setState({ showfooter: true })
      }
    
      keyboardDidShow = () => {
        this.setState({ showfooter: false })
      }

    render() {
        return (
    
          <SafeAreaView style={[global_style.Container]}>
            <View style={[global_style.Container,{width:deviceWidth}]}>
              <StatusBarC global_navigation={this.props.navigation} />
              <Modal
                isVisible={this.state.showProgressBar}>
                <View style={[global_style.Center]}>
                  <Image
                    source={Images.newLoader}
                    style={{
                      flex: 1,
                      width: verticalScale(140), height: verticalScale(35), resizeMode: 'center', marginRight: 30
                    }} />
                </View>
              </Modal>
              <ScrollView style={{ width: '100%', }}>
                <View
                  style={{ width: '100%', backgroundColor: Theme.color_blue, }}>
                  <Text style={[global_style.TIMES_Bold18, { textAlign: 'center', color: Theme.color_white, padding: 10, alignSelf: 'center', }]}>
                    {"Login"}
                  </Text>
                </View>
    
                <View style={{ width: '100%', backgroundColor: Theme.color_white, padding: 10, position: 'relative' }} >
                  <Image
                    style={{ width: verticalScale(260), height: verticalScale(125), alignSelf: 'center', marginTop:30,marginBottom:20 }}
                    source={Images.DashboardLogo} />
    
                  <Text style={[global_style.TIMES_Bold18,{textAlign:'center',marginBottom:2}]}>
                  {"Login"}
                  </Text>
    
                  <Text style={[global_style.TIMES_Regular14, { marginTop: 5,textAlign:'center' }]}>
                  {"Provide your employee code"}
                  </Text>
    
                  <View style={[styles.outerBox]}>
    
                    <View style={[global_style.dropdownStyle, { marginTop: 20 }]}>
    
                      <TextInput
                        style={[global_style.textInputSignin,]}
                        placeholder={"Enter your Employee code"}
                        placeholderTextColor={'#7E7E7E'}
                        placeholderStyle={[global_style.TIMES_Regular16,]}
                        returnKeyType={"next"}
                        ref="EmailId"
                        value={this.state.EmailId}
                        onChangeText={(EmailId) => this.setState({ EmailId: EmailId })}
                        allowFontScaling={false} />
    
                    </View>
    
                    {/* <View style={[global_style.dropdownStyle, { flexDirection: "row", marginTop: 10 }]}>
    
                      <TextInput
                        style={[global_style.textInputSignin, { width: "90%" }]}
                        placeholder={"Enter your New Password"}
                        placeholderTextColor={'#7E7E7E'}
                        placeholderStyle={[global_style.textInputSignin,]}
                        ref="newPassword"
                        value={this.state.newPassword}
                        onChangeText={newPassword => this.setState({ newPassword: newPassword })}
                        secureTextEntry={this.state.showPasswordField}
                        allowFontScaling={false} />
    
                      <TouchableOpacity
                        activeOpacity={global.activeOpacity}
                        hitSlop={{ top: 30, bottom: 30, left: 50, right: 50 }}
                        style={{ alignSelf: 'center', marginLeft: -15 }}
                        onPress={() => this.showPassword()}>
    
                        <FontAwesome
                          name={this.state.showPasswordField ? 'eye-slash' : 'eye'}
                          color={Theme.color_blue}
                          style={{ alignSelf: 'center' }}
                          size={verticalScale(22)} borderColor='#000' />
    
                      </TouchableOpacity>
    
                    </View> */}
    
                    {/* <TouchableOpacity
                      activeOpacity={global.activeOpacity}
                      hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                    //   onPress={() => this.JumpToForget()}
                    >
                      <Text style={[global_style.TIMES_Bold14,
                      { color: Theme.color_blue, textAlign: "right", }]}>
                        {"Forget Password"}
                      </Text>
                    </TouchableOpacity> */}
    
                  </View>
    
    
                </View>
    
                <View style={{ alignSelf: 'center', marginTop: -35, }}>
    
                  <TouchableOpacity
                    activeOpacity={global.activeOpacity}
                    hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
                    // onPress={() => this.state.activityIndicator && this.LoginApi()}
                    onPress={() => this.props.navigation.navigate('OTP')}
                    style={{ width: verticalScale(140), alignSelf: 'center' }}
                  >
                      <Text style={{
                        fontSize:moderateScale(12),
                      backgroundColor: Theme.secondary, padding: 10,
                      borderRadius: 5, alignSelf: 'center', paddingHorizontal: 50
                    }}>
                        {'Submit'}
                      {/* {this.state.activityIndicator ? "Login" : <CommonActivityIndicator></CommonActivityIndicator>} */}
                    </Text>
    
                  </TouchableOpacity>
                  {/* <Text style={[{ textAlign: 'center', paddingVertical: moderateScale(15) },global_style.TIMES_Regular12]}>
                    ----------- OR -----------
                    </Text>
                  <TouchableOpacity
                    activeOpacity={global.activeOpacity}
                    hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
                    onPress={() => this.JumpToSignup()}
                    style={{ width: verticalScale(200), alignSelf: 'center' }}
                  >
                    <Text style={[{
                      backgroundColor: Theme.secondary, padding: 10,
                      borderRadius: 5, alignSelf: 'center', paddingHorizontal: 50
                    },global_style.TIMES_Regular12]}>
                      {"Create Account"}
                    </Text>
                  </TouchableOpacity> */}
                </View>
              </ScrollView>
            </View>
    
          </SafeAreaView>
        );
      }
}

const styles = StyleSheet.create({

    outerBox: {
  
      width: '100%',
      alignSelf: 'center',
      marginTop: 4,
      marginBottom: 40
  
    }
  
  });