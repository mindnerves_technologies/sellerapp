import React, { Component } from 'react';
import {
  View, StyleSheet, Image, Animated, Easing, Dimensions,
  SafeAreaView, ImageBackground, StatusBar, Text,
} from 'react-native';
// import global_style from '../Style/GlobalStyle';
export const deviceHeight = Dimensions.get('window').height;
export const deviceWidth = Dimensions.get('window').width;
import AsyncStorage from '@react-native-community/async-storage';
import DeviceInfo from "react-native-device-info";
import Images from "../Constant/Images";
import global_style from '../Style/GlobalStyle';
import Api from "../Constant/Api";

class SplashScreen extends Component {

  constructor(props) {
    super(props)
    this.animatedWidth = new Animated.Value(0)
    this.state = {
      status: false
    }

  }

  componentDidMount() {

    //stringsoflanguages.setLanguage('ar');
    //get device unique id
    this.setState({ deviceId: DeviceInfo.getUniqueId() });
    //Save to asynch storage
    AsyncStorage.setItem('deviceId', DeviceInfo.getUniqueId());

    setTimeout(() => {
      // this section call after 3 second
      this.getUserData();
      //this.props.navigation.replace('dashboard')
    }, 500)

  }

  //check weather user is active or not
  checkActiveOrNot = (userId) => {
    var url = Api.baseUrl + "InsertAll"
    let mUserData = {
      "CheckSt": "CheckSt",
      "UID": userId
    }
    var status = null
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(mUserData),
    })
      .then((response) => response.json())
      .then((response) => {
        if (response.msg === "Not Active Now") {
          this.props.navigation.replace('Login')
        }
        if (response.msg === "Active Now") {
          this.props.navigation.replace('dashboard')
        }
      })
      .catch((error) => {
      })
    return status
  }

  //Get user Info
  getUserData = async () => {

    try {
      let user_token = await AsyncStorage.getItem('userData');
      //Parse user data
      let userData = JSON.parse(user_token);

      if (userData == null) {
        this.props.navigation.replace('Login')
      } else {
        this.props.navigation.replace('Login')
      }
    } catch (e) {
      //getting error
      this.props.navigation.replace('Login')
    }
  }

  render() {

    return (

      <SafeAreaView style={{ flex: 1 }}>

        {/* for status bar color */}
        <StatusBar
          barStyle="dark-content"
          hidden={true}
          backgroundColor={global.statusBarColor}
          translucent={true} />

        <ImageBackground
          source={Images.splash_logo}
          style={[global_style.imageBackgroundStyle, { flex: 1, }]} >

          {/* <View style={[global_style.Center]}>
              <Image 
              source={Images.app_logo_icon} />
          </View> */}

        </ImageBackground>

      </SafeAreaView>

    );
  }
}


export default SplashScreen;