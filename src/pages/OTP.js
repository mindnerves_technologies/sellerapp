import React, { Component } from "react";
import {
    View,
    ScrollView,
    ActivityIndicator,
    Text,
    Dimensions,
    Linking, Image,TouchableOpacity
} from "react-native";
import OTPInputView from '@twotalltotems/react-native-otp-input';
import Images from "../Constant/Images";
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
import {scale, verticalScale, moderateScale} from 'react-native-size-matters'
import Theme from "../Style/Theme";

export default class OTP extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View>
                <View style={{marginHorizontal:20}}>
                    <View style={{marginVertical:42,width:deviceWidth, height:deviceHeight/4.5,alignItems:'center',marginLeft:-20}}>
                    <Image
                    style={{ width: verticalScale(260), height: verticalScale(125), alignSelf: 'center', marginTop:30,marginBottom:20 }}
                    source={Images.DashboardLogo} />
                    </View>
                    <Text style={{fontSize:18, fontWeight:'bold',marginBottom:10}}>{'Enter the 6 digit OTP send to 99988XXXXX'}</Text>
                    <OTPInputView
                        style={{ width: '100%', height: 60, alignSelf: 'center', marginTop: 5 }}
                        pinCount={6}
                        autoFocusOnLoad
                        onCodeFilled = {(code => {
                            console.log("Code is, you are good to go!")
                        })}
                        codeInputFieldStyle={{ fontSize: 15, color: '#000', }}
                        codeInputHighlightStyle={{ fontSize: 15, color: '#000' }}
                        code={"115544"}
                        offTintColor={'#BFBFBF'}
                        tintColor={'#000000'}
                        textInputStyle={{ fontSize: 15, color: '#000' }}
                    />
                    <Text style={{alignSelf:'flex-end',color:'#001794',textDecorationLine:'underline',fontSize:16,fontWeight:'bold',marginTop:20}}>{"Didn't receieve otp ?"}</Text>
                </View>
                <View style={{top:deviceWidth/1.32,justifyContent:'center'}}>
                    <TouchableOpacity
                    onPress={()=>{
                        
                        this.props.navigation.navigate('CustomerList')
                    }}
                    style={{backgroundColor:Theme.color_blue}}>
                        <View>
                            <Text style={{paddingVertical:18,textAlign:'center',color:'white',fontWeight:'bold'}}>
                                {'VERIFY'}
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

