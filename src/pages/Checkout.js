import React, { Component } from 'react';
import {
    View, Text, StyleSheet, Image, TouchableOpacity, Dimensions, YellowBox, Alert,
    FlatList, TextInput, BackHandler, ActivityIndicator, ImageBackground, SafeAreaView,
    PixelRatio, Share, KeyboardAvoidingView, Platform, StatusBar, ScrollView, Button
    , PermissionsAndroid, NativeEventEmitter
} from 'react-native'
import global from '../Style/GlobalValue';
import global_style from '../Style/GlobalStyle';
import Modal from "react-native-modal";
import Images from "../Constant/Images";
import Theme from '../Style/Theme';
import StatusBarC from '../Style/StatusBarC';
// import { CachedImage } from "react-native-img-cache";
import AsyncStorage from '@react-native-community/async-storage';
import Fontisto from 'react-native-vector-icons/dist/Fontisto';
import {verticalScale,moderateScale,scale} from 'react-native-size-matters'
import Toast from 'react-native-simple-toast';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default class Checkout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            deviceHeight:deviceHeight,
            deviceWidth:deviceWidth,
            connection_status: false,
            showDrawer: false,
            userLID: "",
            paymentArray: [
                { methodName: 'Cash On Delivery' },
                // { methodName: 'PayU Biz' },
                // {methodName:'HDFC'} 
            ],
            selectRadio: -1,
            param_price: '',
            userUID: '',
            cartDataArray: [],
            param_BillingUdid: '',
            param_couponName: '',
            param_VersionName: '',
            param_cartArray: [],
            user_data: null,
            OID: '',
            TxnID: '',
            hidePlaceOrderBtn: false
        };

        AsyncStorage.setItem('DataCartobj', JSON.stringify(this.state.param_cartArray));



    }

    changeRadioBtn = (item, index) => {
        this.setState({ selectRadio: index });
        //this.props.navigation.navigate('Payment_Success')

        //this.placeOrder();
    }

    placeOrder = async () => {
        Toast.showWithGravity("Transaction Successful...!!!", Toast.SHORT, Toast.CENTER);
        setTimeout(()=>{
            this.props.navigation.navigate('CustomerList')
        },1000)
        
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Theme.color_bgColor }}>
                <View style={{ flex: 1, backgroundColor: Theme.color_bgColor }} onLayout={(event) =>{ this.setState({deviceWidth:event.nativeEvent.layout.width, deviceHeight:event.nativeEvent.layout.height}) }} >

                    {/* for status bar color */}

                    <StatusBarC global_navigation={this.props.navigation} />

                    {/* For loading spinner */}

                    <Modal
                        isVisible={this.state.showProgressBar}>
                        <View style={[global_style.Center]}>
                            {/* <Spinner color={Theme.color_orange} style={{ height: 50 }} /> */}
                            <Image
                                source={Images.newLoader}
                                style={{
                                    flex: 1,
                                    width: verticalScale(140), height: verticalScale(35), resizeMode: 'center', marginRight: 30
                                }} />
                        </View>
                    </Modal>

                    <View style={[global_style.outerHeader, { backgroundColor: Theme.color_blue, height: verticalScale(50), }]}>

                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity
                                activeOpacity={global.activeOpacity}
                                onPress={() => this.props.navigation.goBack(null)}
                                hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
                                style={[global_style.OuterImgBox]}>

                                <Image
                                    source={Images.BackArrow}
                                    style={[global_style.ImgBox, { marginHorizontal: 15, tintColor: Theme.color_white }]} />

                            </TouchableOpacity>

                            <TouchableOpacity
                                activeOpacity={global.activeOpacity}
                                onPress={() => this.props.navigation.goBack(null)}
                                hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
                                style={[global_style.OuterImgBox]}>

                                <Text style={[global_style.TIMES_Regular16,
                                { color: Theme.color_white, alignSelf: 'center', alignItems: 'center', }]}>
                                    {'Payment Method'}
                                </Text>
                            </TouchableOpacity>

                        </View>



                    </View>
                    <ScrollView style={{ width: "100%", }}>
                        <Image
                            source={Images.Payment}
                            style={[{ height: verticalScale(300), width: "100%", resizeMode: 'stretch' }]} />

                        <Text style={[global_style.TIMES_Bold18, { textAlign: 'center' }]}>{'Choose Payment Mode'}</Text>
                        <View style={{
                            marginHorizontal: 10,
                            elevation: 4, backgroundColor: 'white',
                            padding: 10, borderTopRadius: 5, borderWidth: 1,
                            borderColor: Theme.secondary, borderRadius: 5, marginTop: 10
                        }}>

                            <FlatList
                                data={this.state.paymentArray}
                                keyExtractor={(item, index) => index.toString()}
                                showsHorizontalScrollIndicator={false}
                                renderItem={({ item, index }) =>
                                    <View style={{ marginVertical: 10 }}>
                                        <View style={{ flexDirection: 'row' }}>

                                            <Fontisto
                                                name={this.state.selectRadio == index ? 'radio-btn-active' : 'radio-btn-passive'}
                                                color={this.state.selectRadio == index ? Theme.color_blue : Theme.color_blue}
                                                hitSlop={{ top: 30, bottom: 10, left: 30, right: 10 }}
                                                size={verticalScale(20)}
                                                onPress={() => this.changeRadioBtn(item, index)}
                                                style={{ alignSelf: 'center', }} />
                                            <Text style={[global_style.TIMES_Regular16, { marginLeft: 10 }]}>{item.methodName}</Text>

                                        </View>
                                        <Text style={[global_style.TIMES_Regular16, { marginLeft: 30, marginTop: 10 }]}>
                                            {'4000/-'}</Text>

                                    </View>

                                }
                            />

                        </View>
                    </ScrollView>

                    {!this.state.hidePlaceOrderBtn && <TouchableOpacity
                        activeOpacity={global.activeOpacity}
                        hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                        onPress={() => this.placeOrder()}
                        style={{ padding: 10, backgroundColor: Theme.color_blue }}>

                        <Text style={[global_style.TIMES_Bold18,
                        { color: Theme.color_white, textAlign: "center", }]}>
                            {"Proceed"}
                        </Text>
                    </TouchableOpacity>}

                </View>
            </SafeAreaView>
        );
    }
}
