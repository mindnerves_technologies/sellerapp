/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
 import React, { Component } from 'react';
 import {
     View, Text, StyleSheet, Image, Pressable, Dimensions, YellowBox, Alert,
     FlatList, TextInput, BackHandler, ActivityIndicator, ImageBackground, SafeAreaView,
     PixelRatio, Share, KeyboardAvoidingView, Platform, StatusBar, ScrollView, Button
     , PermissionsAndroid, RefreshControl, TouchableOpacity, Vibration
 } from 'react-native'
 //import { Spinner, Body } from 'native-base';
 import global from '../Style/GlobalValue';
 import global_style from '../Style/GlobalStyle';
 import Modal from "react-native-modal";
 import Images from "../Constant/Images";
 import Theme from '../Style/Theme'
 //import Drawer from '../Style/Drawer';
 import Api from "../Constant/Api";
 import NetInfo from "@react-native-community/netinfo";
 import SearchBar from '../Style/SearchBar'
 import Toast from 'react-native-simple-toast';
 import AntDesign from 'react-native-vector-icons/dist/AntDesign';
 //import SearchBar from '../Style/SearchBar';
 import StatusBarC from '../Style/StatusBarC';
 import AsyncStorage from '@react-native-community/async-storage';
 import { NavigationEvents } from 'react-navigation';
 import ToolbarFooter from '../Style/ToolbarFooter';
 //import TabFooter from '../Style/TabFooter';
 import DeviceInfo from 'react-native-device-info';
 import Util from '../util/Util';
 import CommonActivityIndicator from '../util/ActivityIndicator';
 //import Filter from '../Style/Filter'
 import {verticalScale,moderateScale,scale} from 'react-native-size-matters'
import TabFooter from '../Style/TabFooter';
 
 const deviceHeight = Dimensions.get('window').height;
 const deviceWidth = Dimensions.get('window').width;
 var searchRef = null
 
 export default class ProductList extends Component {
     constructor(props) {
         super(props);
         this.onEndReachedCalledDuringMomentum = true;
         this.state = {
             deviceWidth:deviceWidth,
             deviceHeight:deviceHeight,
             connection_status: false,
             showDrawer: false,
             showExitAppAlert: false,
             showPincodeModel: false,
             SubCategory: [],
             count: 0,
             userLID: "",
             userUID: '',
             showArray: true,
             AllProducts: [],
             refreshing: false,
             OnBottomReach: false,
             CartCount: '',
             CartItems: [],
             filterPID: '',
             WishlistArrayPush: [],
             stockArray: [],
             FinalTotal: 0.0,
             ProdPID: '',
             showArray1: false,
             totalValue2: '',
             AllProductsList: [],
             zeroQty: true,
             oneIncrement: 1,
             Product_PriceQty: 2,
             MultiQty: '',
             clickCount: 0,
             userGetData: null,
             VersionName: '',
             currentCartVisible: [],
             pageCount: 0,
             endResult: false,
             filterType:'',
             pagesCount: 0,
             priceFilterOn: '',
             sortingOrder:'',
             // getLocationId:"",
             searchBarRef:{}
         };
 
 
         this.didFocus = props.navigation.addListener("didFocus", payload =>
             BackHandler.addEventListener("hardwareBackPress", this.onBack),
         );
 
     }
 
     flatListRef = null
 
     componentDidMount = () => {
         this.willBlur = this.props.navigation.addListener("willBlur", payload =>
             BackHandler.removeEventListener("hardwareBackPress", this.onBack),
         );
 
         //To get the network state once
         NetInfo.addEventListener(state => {
             if (state.isConnected) {
                 this.setState({ connection_status: true });
             } else {
                 this.setState({ connection_status: false });
             }
         });
 
         var VersionCode = DeviceInfo.getVersion()
         setTimeout(() => {
 
             //this.getUserData();
             this.setState({ VersionName: VersionCode })
             //this.LoadSubCategoryAPI();
             // this.LoadStockAPI();
         }, 100);
 
         Util.getAsyncStorage('ORDER_MIN_VALUE').then((data) => {
             if (data !== null) {
                 this.setState({ limit: data })
             }
         })
         this.callLocalcart()
 
     }
     callLocalcart() {
 
         Util.getAsyncStorage('MY_LOCAL_CART').then((data) => {
             if (data !== null) {
                 this.setState({ currentCartVisible: data })
                 setTimeout(() => {
                     this.caluculate()
                 }, 100);
             }
         })
     }
 
     componentWillUnmount() {
         this.didFocus.remove();
         this.willBlur.remove();
         BackHandler.removeEventListener("hardwareBackPress", this.onBack);
     }
 
     // BackBtn
     onBack = () => {
         this.props.navigation.goBack(null)
         return true
     };
 
     //Get user Info
     getUserData = async () => {
 
         try {
             this.setState({ showProgressBar: true })
             // for getting user data
             let user_data = await AsyncStorage.getItem('userData');
 
             //Parse user data
             let get_data = JSON.parse(user_data);
             this.setState({ userLID: get_data.userLID })
             this.setState({ userUID: get_data.userId, userGetData: get_data })
             this.LoadSubCategoryAPI(false, this.state.pagesCount);
             //		this.LoadCartCountAPI();
         } catch (e) {
             let user_loc = await AsyncStorage.getItem('user_location');
             let get_token = JSON.parse(user_loc);
             //this.state.userLID = get_token.LocationId;
             this.state.userLID = 11;
             this.LoadSubCategoryAPI(false, this.state.pagesCount);
         }
     }
 
     applyFilterAPI(show,count) {
 
         if (this.state.refreshing) {
             count = 0
             this.setState({ AllProducts: [], pagesCount: 0 })
         }
 
         if (!show)
             this.setState({ showProgressBar: true })
         
         
         if (this.state.connection_status) {
             let mUserData = {
                 "AllProducts1":"AllProducts1",
                 "count": this.state.pagesCount,
                 "FilterData":this.state.priceFilterOn,
                 "HTOL": this.state.sortingOrder,
                 "LID": this.state.userLID
             }
     
             const url = Api.baseUrl + Api.verifyPin
             fetch(url, {
                 method: 'POST',
                 headers: {
                     'Content-Type': 'application/json',
                 },
                 body: JSON.stringify(mUserData),
             })
                 .then((response) => response.json())
                 .then((response) => {
                     this.setState({ refreshing: false, bottomLoader: false })
                     if (response.length > 0) {
                         let data = []
                         
                         data = this.state.AllProducts.length > 0 ? this.state.AllProducts : []
                         let bigCities = response;
                     
                         data = data.length > 1 ? data.concat(bigCities) : bigCities
                         this.setState({ showArray: true })
                         if (response.length > 2) {
                             this.setState({ AllProducts: bigCities })
                             for (let i = 0; i <= 10; i++) {
                                 this.setState({ AllProducts: data })
                             }
                             this.setState({ showProgressBar: false })
                         } else {
                             this.setState({ AllProducts: data })
                             this.setState({ showProgressBar: false })
                         }
                     } else {
                         this.setState({ showArray: false })
                     }
                 })
                 .catch((error) => {
                     this.setState({ showProgressBar: false })
                 })
                 .done();
         } else {
             Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
             this.setState({ showProgressBar: false })
         }
 
         
     }
     // location code end
     LoadSubCategoryAPI = (show, count) => {
         searchRef!=null && searchRef.searchProductsAPI(this.state.filterType,this.state.priceFilterOn,this.state.sortingOrder)
         if (this.state.refreshing) {
             count = 0
             this.setState({ AllProducts: [], pagesCount: 0 })
         }
 
         if (!show)
             this.setState({ showProgressBar: true })
         if (this.state.connection_status) {
             var url = Api.baseUrl + Api.verifyPin
             let mUserData = {
                 "AllProducts1_V1": "AllProducts1_V1",
                 "LID": this.state.userLID,
                 "count": count
             };
 
             if (this.state.priceFilterOn != '') {
                 mUserData={
                     "AllProducts2":"AllProducts2",
                     "pageno":`${parseInt(count/20)+1}`,
                     "FilterData":this.state.priceFilterOn,
                     "HTOL": this.state.sortingOrder,
                     "LID":this.state.userLID
                 }
                 if (this.state.priceFilterOn == 'AlphabetFilter' && this.state.filterType == "Alphabetic") {
                     mUserData = {
                         "AllProducts2":"AllProducts2",
                         "pageno":`${parseInt(count/20)+1}`,
                         "FilterData": this.state.priceFilterOn,
                         "AToZ": this.state.sortingOrder,
                         "LID": this.state.userLID
                     }	
                 }
                 // if (this.state.filterType=='Brands') {
                     
                 // }
                 if (this.state.filterType=='Discounts') {
                     mUserData={
                         "AllProducts2":"AllProducts2",
                         "pageno":`${parseInt(count/20)+1}`,
                         "FilterData": "DiscountFilter",
                         "Discount":"Discount",
                         "LID":this.state.userLID
                     }
                 }
                 
             }
             
             fetch(url, {
                 method: 'POST',
                 headers: {
                     'Content-Type': 'application/json',
                 },
                 body: JSON.stringify(mUserData),
             })
                 .then((response) => response.json())
                 .then((response) => {
                     this.setState({ refreshing: false, bottomLoader: false })
                     if (response.length > 0) {
                         //this.setState({ AllProducts1: response })
                         let data = []
                         //let jsondata = []
                         data = this.state.AllProducts.length > 0 ? this.state.AllProducts : []
                         let bigCities = response;
                         //	for (let i = 0; i < response.length; i++) {
                         //		if (response[i].pstatus === 'Y') {
                         // var obj = JSON.parse(response[i].pro_price_data);
 
                         // if(i===4){
 
                         // }
                         //	response[i].priceData = obj[0];
 
                         // jsondata.push(obj)
                         //			bigCities.push(response[i]);
                         //		}
                         //	}
                         data = data.length > 1 ? data.concat(bigCities) : bigCities
                         
                         // bigCities.length!=0 && bigCities.map(()=>{
                         // 	bigCities.length!=0 && bigCities.map((ele,index)=>{
                         // 		if(ele.Qty_avail==0){
                         // 			temp = bigCities.splice(index,1)
                         // 			bigCities.push(temp[0])
                         // 		}
                         // 	})
                         // })
 
                         // data.length!=0 && data.map(()=>{
                         // 	data.length!=0 && data.map((ele,index)=>{
                         // 		if(ele.Qty_avail==0){
                         // 			temp = data.splice(index,1)
                         // 			data.push(temp[0])
                         // 		}
                         // 	})
                         // })
 
                         this.setState({ showArray: true })
                         if (response.length > 2) {
                             this.setState({ AllProducts: bigCities })
                             for (let i = 0; i <= 10; i++) {
                                 this.setState({ AllProducts: data })
                             }
                             this.setState({ showProgressBar: false })
                         } else {
                             this.setState({ AllProducts: data })
                             this.setState({ showProgressBar: false })
                         }
                     } else {
                         this.setState({ showArray: false })
                     }
                 })
                 .catch((error) => {
                     this.setState({ showProgressBar: false })
                 })
                 .done();
         } else {
             // Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
             this.setState({ showProgressBar: false })
         }
     }
 
     LoadStockAPI = (getPID, getUnit, getIndex) => {
 
         if (this.state.connection_status) {
             var url = Api.baseUrl + Api.verifyPin
             let mData = {
                 "check_stock_qty": "check_stock_qty",
                 "PID": getPID,
                 "Unit": getUnit
             };
             fetch(url, {
                 method: 'POST',
                 headers: {
                     'Content-Type': 'application/json',
                 },
                 body: JSON.stringify(mData),
             })
                 .then((response) => response.json())
                 .then((response) => {
                     this.state.AllProducts[getIndex].available = response.Data;
                 })
                 .catch((error) => {
                     this.setState({ showProgressBar: false })
                 })
                 .done();
         } else {
             Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
             this.setState({ showProgressBar: false })
         }
     }
 
     doInfinte = () => {
         let pageCount = this.state.pagesCount + 20
         this.setState({ pagesCount: pageCount, bottomLoader: true, isScroll: true })
         this.setState({ isScroll: true })
         this.LoadSubCategoryAPI(true, pageCount);
     }
     scrollProd = () => {
     }
     LoadCartCountAPI = () => {
         if (this.state.connection_status) {
             var url = Api.baseUrl + Api.verifyPin
             let mUserData = {
                 "CartDataByUser": "CartDataByUser",
                 "uid": this.state.userUID
             };
 
             fetch(url, {
                 method: 'POST',
                 headers: {
                     'Content-Type': 'application/json',
                 },
                 body: JSON.stringify(mUserData),
             })
                 .then((response) => response.json())
                 .then((response) => {
                     if (response == 'blank') {
 
                         this.setState({ CartCount: 0 })
                     }
 
                     if (response.ProductsDetail != undefined) {
                         this.state.CartItems = [];
                         this.setState({ CartItems: response.ProductsDetail })
                         //	this.setState({ currentCartVisible: response.ProductsDetail })
                         //	this.setState({ CartCount: this.state.CartItems.length })
 
                         this.setState({ showArray1: true })
                         var totalValue = 0.0
 
 
                         for (let i = 0; i < this.state.CartItems.length; i++) {
                             //totalValue = totalValue + Number.parseFloat(this.state.CartItems[i].Total_val_enc)
 
                             //   let totalValue1 = totalValue
 
                             //   this.setState({ totalValue2: totalValue1 });
                             // }
                             // this.setState({ FinalTotal: this.state.totalValue2 });
                             var stringToConvert = this.state.CartItems[i].Total_val_enc
                             var numberValue = JSON.parse(stringToConvert);
                             totalValue = totalValue + numberValue
                         }
 
                         //		this.setState({ FinalTotal: totalValue.toFixed(2) });
                         this.setState({ AddTocartLoading: false })
 
                         // this.setState({ showProgressBar: false })
                         //this.setState({ showProgressBar: false })
 
                     } else {
                         this.setState({ AddTocartLoading: false })
                         //this.setState({ showProgressBar: false })
                     }
 
                 })
                 .catch((error) => {
                     this.setState({ showProgressBar: false })
                     this.setState({ AddTocartLoading: false })
                     //Toast.showWithGravity("Something went wrong", Toast.SHORT, Toast.CENTER);;
                 })
                 .done();
         } else {
             Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
             this.setState({ showProgressBar: false })
         }
     }
 
     //jump to drawer
     clickHamburgerMenu = () => {
         this.setState({ showDrawer: !this.state.showDrawer })
     }
 
     JumpToProductDetais = (item) => {
 
         this.props.navigation.navigate('Checkout')
 
     }
 
     JumpToSearch = () => {
         this.props.navigation.navigate('Search')
     }
 
     gotoCartPage = () => {
         this.props.navigation.navigate('MyCart')
     }
 
     onRefresh = () => {
         this.setState({ refreshing: true });
         this.getUserData();
     }
 
     _backRefresh = () => {
         //this.setState({ currentCartVisible: [], AllProducts: [], })
         //this.setState({AllProducts:[]})
         //this.LoadSubCategoryAPI(false, 0);
         this.getUserData();
         this.callLocalcart()
 
         if (this.state.showArray1 == true) {
             this.componentDidMount()
             //.LoadCartCountAPI();
         }
     }
 
     removeFromCart(itemId) {
         var newData = this.state.currentCartVisible;
         var updatedData = [];
         newData.map((item) => {
             if (item.PID !== itemId) {
                 updatedData.push(item);
             }
         });
         this.setState({ currentCartVisible: updatedData })
         return updatedData;
     }
 
     // RemoveApi
     RemoveApi = (item, id) => {
 
 
         Alert.alert(
             "Delete",
             'AreYouSureAboutDelete',
             [
                 {
                     text: "No",
                     style: "cancel"
                 },
                 {
                     text: "Yes", onPress: () => {
 
 
                         {
 
                             let newLst = this.state.currentCartVisible.filter(arr => arr.PID !== item.PID)
 
                             this.setState({ currentCartVisible: newLst })
                             this.caluculate()
                             
                         }
 
                     }
                 }
             ],
             { cancelable: false }
         );
     }
 
 
 
 
     toAddcart = (items,) => {
         let item = items
         this.setState({ AddTocartLoading: true })
 
 
         var price = item.pro_price_data
         item.Product_Unit = price[0].productprice_qty
         item.qty = parseInt(price[0].productprice_qty)
         this.AddToCartApi(item)
     }
     updatmData = (id, items) => {
         var currentCartVisible = this.state.currentCartVisible
         currentCartVisible.map((item) => {
             if (item.PID === id) {
                 item.Total_val_enc = items.Total_val_enc
                 item.qty = items.qty
                 item.qty = items.qty
                 item.single_price = items.single_price
                 item.unit = items.unit
                 item.priceData = items.priceData
             }
         });
         this.forceUpdate();
         this.setState({ currentCartVisible: currentCartVisible })
         setTimeout(() => {
             this.caluculate()
         }, 100);
 
     }
 
 
     Increment = (items, id) => {
         let item = items
         let decidePriceArr = []
         var pricepro = item.priceData
         var upQty = 0
         let price = item.pro_price_data
 
         if (price.length > 1) {
             pricepro = ''
 
             decidePriceArr = price
             decidePriceArr = decidePriceArr.sort((firstProduct, secondProduct) => {
                 return (parseInt(firstProduct.productprice_qty) - parseInt(secondProduct.productprice_qty))
             })
 
             let count = 0
             upQty = parseInt(item.qty) + parseInt(decidePriceArr[0].productprice_qty)
 
             for (let product of decidePriceArr) {
 
                 if (parseInt(upQty) === parseInt(product.productprice_qty)) {
                     pricepro = product
                     // upQty = parseInt(item.qty) + parseInt(pricepro.productprice_qty)
                 }
                 else if (parseInt(upQty) < parseInt(product.productprice_qty) && count !== 0) {
                     pricepro = decidePriceArr[count - 1]
                     break;
                 } else if (decidePriceArr.length === count + 1) {
                     pricepro = product
                 }
 
                 count += 1
 
             }
 
         } else {
             upQty = parseInt(item.qty) + parseInt(pricepro.productprice_qty)
         }
 
 
         var Total_val_enc = upQty * pricepro.productprice_ppc
 
         if (parseInt(item.Qty_avail) >= upQty) {
             item.qty = upQty
             item.Total_val_enc = upQty * pricepro.productprice_ppc
             item.single_price = pricepro.productprice_ppc
             item.unit = pricepro.productprice_unit
             item.priceData = pricepro
             this.updatmData(item.PID, item);
         }
 
     }
     Decrement = (item, id) => {
         var pricepro = item.priceData
         var price = item.pro_price_data
         let decidePriceArr = []
         var upQty = 0
 
         if (price.length > 1) {
             pricepro = ''
 
             decidePriceArr = price
             decidePriceArr = decidePriceArr.sort((firstProduct, secondProduct) => {
                 return (parseInt(firstProduct.productprice_qty) - parseInt(secondProduct.productprice_qty))
             })
 
             let count = 0
             upQty = parseInt(item.qty) - parseInt(decidePriceArr[0].productprice_qty)
 
             for (let product of decidePriceArr) {
 
                 if (decidePriceArr.length === 1) {
                     pricepro = product
                 } else if (parseInt(upQty) === parseInt(product.productprice_qty)) {
                     pricepro = product
                 }
                 else if (parseInt(upQty) < parseInt(product.productprice_qty) && count !== 0) {
                     pricepro = decidePriceArr[count - 1]
                     break;
                 } else if (decidePriceArr.length === count + 1) {
                     pricepro = product
                 } else if (upQty === 0) {
                     //upQty = parseInt(item.qty) - parseInt(decidePriceArr[count].productprice_qty)
                     pricepro = decidePriceArr[count - 1]
                 }
 
                 count += 1
 
             }
 
         } else {
             upQty = parseInt(item.qty) - parseInt(pricepro.productprice_qty)
         }
 
         var Total_val_enc = upQty * pricepro.productprice_ppc
         if (upQty > 0) {
 
             item.qty = upQty
             item.Total_val_enc = upQty * pricepro.productprice_ppc
             item.single_price = pricepro.productprice_ppc
             item.unit = pricepro.productprice_unit
             item.priceData = pricepro
             this.updatmData(item.PID, item);
         } else {
             this.RemoveApi(item)
             // let newLst = this.state.currentCartVisible.filter(arr => arr.PID !== item.PID)
             // this.setState({ currentCartVisible: newLst })
             // this.caluculate()
         }
     }
 
     changeQuantityDirect = (item, id,value) => {
         var pricepro = item.priceData
         var price = item.pro_price_data
         let decidePriceArr = []
         var upQty = 0
         if (price.length > 1) {
             pricepro = ''
 
             decidePriceArr = price
             decidePriceArr = decidePriceArr.sort((firstProduct, secondProduct) => {
                 return (parseInt(firstProduct.productprice_qty) - parseInt(secondProduct.productprice_qty))
             })
 
             let count = 0
         if(parseInt(value) <= parseInt(item && item.Qty_avail) && parseInt(value) % parseInt(item && item.pro_price_data[0] && item.pro_price_data[0].productprice_qty) == 0){
             upQty = parseInt(value) 
         }
         else{
             Toast.showWithGravity('EnterValidData'+parseInt(item && item.pro_price_data[0] && item.pro_price_data[0].productprice_qty)+'AndUnderValidData', Toast.SHORT, Toast.CENTER)
             upQty = parseInt(item && item.pro_price_data[0] && item.pro_price_data[0].productprice_qty)
         }
 
             for (let product of decidePriceArr) {
 
                 if (decidePriceArr.length === 1) {
                     pricepro = product
                 } else if (parseInt(upQty) === parseInt(product.productprice_qty)) {
                     pricepro = product
                 }
                 else if (parseInt(upQty) < parseInt(product.productprice_qty) && count !== 0) {
                     pricepro = decidePriceArr[count - 1]
                     break;
                 } else if (decidePriceArr.length === count + 1) {
                     pricepro = product
                 } else if (upQty === 0) {
                     //upQty = parseInt(item.qty) - parseInt(decidePriceArr[count].productprice_qty)
                     pricepro = decidePriceArr[count - 1]
                 }
 
                 count += 1
 
             }
 
         } else {
             if(parseInt(value) <= parseInt(item && item.Qty_avail) && parseInt(value) % parseInt(item && item.pro_price_data[0] && item.pro_price_data[0].productprice_qty) == 0){
                 upQty = parseInt(value) 
             }
             else{
                 Toast.showWithGravity('EnterValidData'+parseInt(item && item.pro_price_data[0] && item.pro_price_data[0].productprice_qty)+'AndUnderValid', Toast.SHORT, Toast.CENTER)
                 upQty = parseInt(item && item.pro_price_data[0] && item.pro_price_data[0].productprice_qty)
             }
         }
 
     
         var Total_val_enc = upQty * pricepro.productprice_ppc
         if (upQty > 0) {
             item.qty = upQty
             item.Total_val_enc = upQty * pricepro.productprice_ppc
             item.single_price = pricepro.productprice_ppc
             item.unit = pricepro.productprice_unit
             item.priceData = pricepro
             this.updatmData(item.PID, item);
         } else {
             let newLst = this.state.currentCartVisible.filter(arr => arr.PID !== item.PID)
             this.setState({ currentCartVisible: newLst })
             this.caluculate()
         }
         this.onRefresh()
     }
 
     isAlreadyopen(itemId) {
         var found = false;
         this.state.currentCartVisible.map((item) => {
             if (item !== null && item.PID === itemId) {
                 found = true;
             }
         });
         return found;
     }
 
     isAlreadyInLiveCart(itemId) {
         var found = false;
         this.state.CartItems.map((item) => {
             if (item !== null && item.PID === itemId) {
                 found = true;
             }
         });
         return found;
     }
     isAlreadyInLiveCartRowid(itemId) {
         var found = false;
         this.state.CartItems.map((item) => {
             if (item !== null && item.PID === itemId) {
                 found = item;
             }
         });
         return found;
     }
     isAlreadyInLiveCartCount(itemId) {
         var found = false;
         this.state.currentCartVisible.map((item) => {
             if (item !== null && item.PID === itemId) {
                 found = item;
             }
         });
         return found;
     }
 
 
     getItemLiveCart(itemId) {
         var found = false;
         this.state.CartItems.map((item) => {
             if (item.PID === itemId) {
                 found = item;
             }
         });
         return found;
     }
 
     updatePriceData(itemId, data) {
         var found = false;
         var newData = this.state.currentCartVisible;
         var updatedItem = '';
         newData.map((item) => {
             if (item.PID === itemId) {
                 item.priceData = data
                 updatedItem = item;
             }
         });
         this.setState({ currentCartVisible: newData })
         return updatedItem;
     }
 
     updateItemData(itemId, item_) {
         var found = false;
         var newData = this.state.currentCartVisible;
         this.state.currentCartVisible = [];
         var updatedItem = '';
         newData.map((item) => {
             if (item.PID === itemId) {
                 item = item_
                 updatedItem = item;
             }
         });
         this.setState({ currentCartVisible: newData })
         setTimeout(() => {
             this.caluculate()
         }, 100);
         //	return updatedItem;
     }
 
     getItem(itemId) {
         var found = "";
         this.state.currentCartVisible.map((item) => {
             if (item.PID === itemId) {
                 found = item;
             }
         });
         return found;
     }
     LoadProductPriceAPI = (item) => {
         if (parseFloat(item.priceData.productprice_qty) <= parseFloat(item.Qty_avail)) {
             var newItem = false;
             var currentCartVisible = this.state.currentCartVisible;
             var data = {};
             if (!this.isAlreadyopen(item.PID)) {
                 var price = item.pro_price_data
                 let pricepro = price[0]
 
                 item.Total_val_enc = pricepro.productprice_ppc * pricepro.productprice_qty;
                 item.qty = pricepro.productprice_qty;
                 item.single_price = pricepro.productprice_ppc
                 item.unit = item.priceData.productprice_unit
                 item.priceData = pricepro
 
             }
             currentCartVisible.push(item)
 
             this.setState({ currentCartVisible: currentCartVisible })
             setTimeout(() => {
                 this.caluculate(currentCartVisible)
 
             }, 100);
         } else {
             Toast.showWithGravity("Product is out of stock", Toast.SHORT, Toast.CENTER)
         }
 
     }
 
     gettotalvalinc = (item) => {
 
         var price = JSON.parse(item.pro_price_data)
         let proPriceData = price[0]
         var cal = proPriceData.productprice_ppc * proPriceData.productprice_qty;
 
         return proPriceData
 
     }
     caluculate = () => {
         this.setState({ CartCount: this.state.currentCartVisible.length })
         var totalValue = 0.0
         for (let i = 0; i < this.state.currentCartVisible.length; i++) {
             var stringToConvert = this.state.currentCartVisible[i].Total_val_enc
             var numberValue = JSON.parse(stringToConvert);
             totalValue = totalValue + numberValue
         }
         this.setState({ FinalTotal: totalValue.toFixed(2) });
         Util.setAsyncStorage('MY_LOCAL_CART', this.state.currentCartVisible)
         this.forceUpdate();
     }
 
     onEndReached = ({ distanceFromEnd }) => {
         if (!this.state.bottomLoader) {
             this.doInfinte();
         }
     }
 
     gotoTop = () => {
         this.flatListRef.scrollToOffset({animated: true, offset:0})
     }
 
     getValue(maxQuanti, item , liverowid,enterData, minimumData){
         var reminder = enterData % minimumData
         var data = reminder!= 0 ? enterData - reminder : maxQuanti + minimumData
         Toast.showWithGravity('EnterValidData'+minimumData+'AndUnderValid', Toast.SHORT, Toast.CENTER)
         this.changeQuantityDirect(item, liverowid, data)
         this.forceUpdate()
     }
 
     renderItem(item_) {
         var item = item_.item;
         const zeroQty = this.isAlreadyopen(item.PID)
         const liveItem = this.isAlreadyInLiveCartCount(item.PID)
         const incartQunat = liveItem.qty
         const liverowid = liveItem.rowid
         if (zeroQty == true) {
             item.qty = liveItem.qty
         }
         let AviliableQuantity = item.Qty_avail
         if (zeroQty) {
             AviliableQuantity = item.Qty_avail - item.qty
         }
 
         if (item != null || item != undefined) {
             return (
                 <View>
                     <View style={{ flexDirection: 'row', margin: 5, }}>
                         <View style={{ flex: 1, alignItems: 'center', alignSelf: 'center' }}>
                             <TouchableOpacity
                                 activeOpacity={global.activeOpacity}
                                 hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
                                 onPress={() => this.JumpToProductDetais(item)}
                                 style={{ height: verticalScale(100), width: "95%", padding: 5 }}
                             >
                                 {item.image_mid == null ?
                                     <Image
                                         source={Images.goodLuckimg}
                                         style={[global_style.thmbnail_box, { width: verticalScale(80), height: verticalScale(90), }]} />
                                     :
                                     <Image
                                         source={{ uri: Api.imageUrl + item.image_mid }}
                                         resizeMode={'contain'} resizeMethod="resize"
                                         style={[{ flex: 1, width: null, height: null }]} />}
                                 {item.Qty_avail == 0 ?
                                     <Image
                                         source={Images.stockimg}
                                         style={[global_style.thmbnail_box, { width: "95%", height: verticalScale(100), marginTop: -100 }]} />
                                     : null}
                             </TouchableOpacity>
                         </View>
                         <View style={{ flex: 2, }}>
                             <TouchableOpacity
                                 activeOpacity={global.activeOpacity}
                                 hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
                                 onPress={() => this.JumpToProductDetais(item)}
                             >
                                 <Text style={[global_style.TIMES_Regular14,]}>
                                     {item.ProductName}
                                 </Text>
                                 <View style={{ flex: 1, flexDirection: 'row' }}>
                                     <View style={{ marginTop: 10, marginRight: 10 }}>
                                         <Text style={[global_style.TIMES_Bold12, { color: Theme.color_mrp_gray, textDecorationLine: 'line-through' }]}>
                                             {'MRP ₹'} {item.MrpPrice}
                                         </Text>
                                         <Text style={[global_style.TIMES_Bold14, { color: Theme.color_blue, }]}>
                                             {'Price ₹'} {item.Selling_Price}
                                         </Text>
                                         <Text style={[global_style.TIMES_Bold12, { color: Theme.secondary, }]}>
                                             {'You Save ₹'}{Number.parseFloat(item.MrpPrice - item.Selling_Price).toFixed(2)}
                                             {item.discount != "" &&
                                                 <Text style={[global_style.TIMES_Bold12, { color: Theme.color_red }]}>
                                                     {"\n"}&nbsp; ({Number.parseFloat(item.discount).toFixed(2) + '% Off'})
                                          </Text>}
                                         </Text>
                                     </View>
                                     <View style={{ flex: 1, alignItems: 'flex-end', marginTop: 10, marginRight: 10 }}>
                                         <Text style={[global_style.TIMES_Bold14, { color: Theme.color_blue, }]}>
                                             {'Avail Qty'} {AviliableQuantity}
                                         </Text>
                                         {item.priceData != null && item.priceData != undefined && <Text style={[global_style.TIMES_Bold14, { color: Theme.color_blue, }]}>
                                         {'Minimum Qty'} {item.priceData.productprice_qty}
                                         </Text>}
 
                                         {item.Qty_avail === "0" 
                                         ?
                                             zeroQty == false &&
                                             <TouchableOpacity
                                                 //	disabled={this.state.hideIncrement ? true : false}
                                                 activeOpacity={global.activeOpacity}
                                                 hitSlop={{ top: 10, bottom: 10, left: 10, right: 8 }}
                                                 style={{ width: DeviceInfo.isTablet() ? "55%" : "80%", marginTop: 30 }}
                                                 onPress={() => { item.Notify !== "Notified" ? this.handleNotifyClick(item.PID) : Toast.showWithGravity('NotificationAlreadySet', Toast.SHORT, Toast.CENTER)} }
                                             >
                                                 <View style={{
                                                     backgroundColor: item.Notify === "Notified" ? 'green' : Theme.color_red, borderRadius: 5,
                                                     paddingHorizontal: 15, paddingVertical: 5
                                                 }}>
                                                     <Text style={{
                                                         color: Theme.color_white, fontSize: moderateScale(15),
                                                         alignSelf: "center"
                                                     }}>
                                                         {'Notify Me'}
                                                     </Text>
                                                 </View>
                                             </TouchableOpacity>
                                         :
                                         zeroQty == false && 
                                             <TouchableOpacity
                                                 //	disabled={this.state.hideIncrement ? true : false}
                                                 activeOpacity={global.activeOpacity}
                                                 hitSlop={{ top: 10, bottom: 10, left: 10, right: 8 }}
                                                 style={{ width: DeviceInfo.isTablet() ? "55%" : "70%", marginTop: 30 }}
                                                 onPress={() => [item.Qty_avail === "0" ? Toast.showWithGravity("Product is out of stock", Toast.SHORT, Toast.CENTER) : this.LoadProductPriceAPI(item)]}
                                             >
                                                 <View style={{
                                                     backgroundColor: Theme.color_blue, borderRadius: 5,
                                                     paddingHorizontal: 15, paddingVertical: 5
                                                 }}>
                                                     <Text style={{
                                                         color: Theme.color_white, fontSize: moderateScale(14),
                                                         alignSelf: "center"
                                                     }}>
                                                         {'ADD'}
                                                     </Text>
                                                 </View>
                                             </TouchableOpacity>
                                         }
                                         
                                     </View>
                                 </View>
                             </TouchableOpacity>
                         </View>
                     </View>
                     <View style={{ flex: 1 }}>
                         {(zeroQty == true && item.priceData != null && item.priceData != undefined) &&
                             <View style={{ alignSelf: 'flex-end', alignItems: 'flex-end', flexDirection: "row", marginTop: 0, marginEnd: 7 }}>
                                 <TouchableOpacity
                                     style={{marginRight:4}}
                                     activeOpacity={global.activeOpacity}
                                     hitSlop={{ top: 7, bottom: 10, left: 10, right: 8 }}
                                     //onPress={() => this.Decrement(item)}
                                     onPress={() => this.Decrement(item, liverowid)}
                                 >
                                     <View style={{
                                         backgroundColor: Theme.color_blue, borderRadius: 50,
                                         height: verticalScale(26), width: verticalScale(26), alignSelf: "center", flexDirection: 'column',
                                         justifyContent: 'center', alignItems: 'center'
                                     }}>
                                         <AntDesign name='minus' color={Theme.color_white} size={verticalScale(16)} />
                                     </View>
                                 </TouchableOpacity>
                                 <TextInput style={{
                                     color: Theme.color_black, fontSize: moderateScale(14),padding:0,
                                     alignSelf: 'center', width: 'auto',paddingHorizontal:5, marginHorizontal:5 ,height: verticalScale(32),paddingHorizontal:5,
                                     textAlign: "center", marginHorizontal:5,alignSelf: 'center',top:2 , borderWidth:0.5,borderColor:'#c9c9c9',
                                 }}
                                 onEndEditing={(e)=>{ [ AviliableQuantity >= parseInt(e.nativeEvent.text) && parseInt(e.nativeEvent.text)%parseInt(item && item.pro_price_data[0] && item.pro_price_data[0].productprice_qty)==0 ? this.changeQuantityDirect(item, liverowid, parseInt(e.nativeEvent.text == "0" ? item.priceData.productprice_qty : e.nativeEvent.text)) : this.getValue(AviliableQuantity, item,liverowid, parseInt(e.nativeEvent.text == "0" ? item.priceData.productprice_qty : e.nativeEvent.text),parseInt(item && item.pro_price_data[0] && item.pro_price_data[0].productprice_qty)) ] }}
                                 >
                                     {item.qty}
                                 </TextInput>
                                 <TouchableOpacity style={global_style.plusStyle}
                                     activeOpacity={global.activeOpacity}
                                     hitSlop={{ top: 7, bottom: 10, left: 10, right: 8 }}
                                     onPress={() => [AviliableQuantity < parseInt(item.pro_price_data[0].productprice_qty) ?
                                         Toast.showWithGravity('ReachAvailQty', Toast.SHORT, Toast.CENTER) : this.Increment(item, liverowid)]}>
                                     <View style={{
                                         backgroundColor: Theme.color_blue, borderRadius: 50,
                                         height: verticalScale(26), width: verticalScale(26), alignSelf: "center", flexDirection: 'column',
                                         justifyContent: 'center', alignItems: 'center'
                                     }}>
                                         <AntDesign name='plus' color={Theme.color_white} size={verticalScale(16)} />
                                     </View>
                                 </TouchableOpacity>
                                 {liveItem &&
                                     <TouchableOpacity
                                         disabled={this.state.hideIncrement ? true : false}
                                         activeOpacity={global.activeOpacity}
                                         hitSlop={{ top: 10, bottom: 10, left: 10, right: 8 }}
                                         style={{ width: "10%", marginTop: 0 }}
                                         onPress={() => this.RemoveApi(item, liverowid)}>
                                         <View style={{
                                             // backgroundColor: Theme.color_blue, borderRadius: 5,
                                             paddingHorizontal: 15, paddingVertical: 3
                                         }}>
                                             <Image style={{
                                                 tintColor: Theme.color_blue,
                                                 alignSelf: "center", width: verticalScale(20), height: verticalScale(20)
                                             }} source={Images.trash}>
                                             </Image>
                                         </View>
                                     </TouchableOpacity>}
                             </View>
                         }
                     </View>
                     <View
                         style={{
                             borderBottomColor: Theme.color_gray, alignSelf: 'center',
                             borderBottomWidth: 0.6, width: '100%', marginVertical: 5
                         }} />
                 </View>
             )
         }
     }
 
     handleNotifyClick = (PID) => {
         var url = Api.baseUrl + "InsertAll"
         let mUserData = {
               "InsertNotifyPro":"InsertNotifyPro",
               "PID":PID,
               "UID":this.state.userUID,
               "LID":this.state.userLID
           }      
     
         fetch(url, {
           method: 'POST',
           headers: {
             'Content-Type': 'application/json',
           },
           body: JSON.stringify(mUserData),
         })
           .then((response) => response.json())
           .then((response) => {
             Toast.showWithGravity(response.msg === "Added Successfully" ? 'WillNotifyYou' : null, Toast.SHORT, Toast.CENTER);
             this.setState({AllProducts:[],pagesCount:0})
             this.LoadSubCategoryAPI(true,1);
         })
           .catch((error) => {
           })
           
       }
 
       searchData = () => {

    }
 
     render() {
         return (
             <SafeAreaView style={{ flex: 1, backgroundColor: Theme.color_bgColor }}>
                 <View style={{ flex: 1, backgroundColor: Theme.color_bgColor }} onLayout={(event) =>{ this.setState({deviceWidth:event.nativeEvent.layout.width, deviceHeight:event.nativeEvent.layout.height}) }}>
                     {/* for status bar color */}
                     <StatusBarC global_navigation={this.props.navigation} />
                     {/* For loading spinner */}
                     <NavigationEvents onDidFocus={() => this._backRefresh()} />
                     <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', backgroundColor: Theme.color_blue, padding: 5, height: verticalScale(50), }}>
                         <View style={{ flexDirection: "row" }}>     
                            <Text style={[global_style.TIMES_Regular16,
                            { color: Theme.color_white, alignSelf: 'center', alignItems: 'center', marginLeft: 10 }]}>
                                {'Product list'}
                            </Text>
                           
                         </View>
                         <View style={{ flexDirection: 'row', marginRight: 10 }}>
                            
                             <TouchableOpacity
                                 activeOpacity={global.activeOpacity}
                                 hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
                                 onPress={() => this.gotoCartPage()}
                                 style={{ alignSelf: 'center' }}>
                                 <Image
                                     style={[styles.backArrow, { width: verticalScale(25), height: verticalScale(25), resizeMode: 'contain', tintColor: Theme.secondary, marginRight: 10 }]}
                                     source={Images.cart} />
                                 {this.state.CartCount != 0 ?
                                     <Text style={[global_style.TIMES_Regular12, { color: '#fff' }]}>{'₹ '}{this.state.FinalTotal}</Text>
                                     : null}
                             </TouchableOpacity>
                             {this.state.CartCount != 0 ?
                                 <View
                                     style={[global_style.cartCircle, { marginLeft: 5, marginTop: -3, right: 0 }]}>
                                     <Text
                                         style={{
                                             fontSize: moderateScale(12), color: Theme.color_white, textAlign: 'center'
                                         }}>
                                         {this.state.CartCount}
                                     </Text>
                                 </View>
                                 : null}
                         </View>
                     </View>
                     <SearchBar searchData={this.searchData} allData={this} global_navigation={this.props.navigation}/>
                     {/* <View style={{flexDirection:'row',backgroundColor: Theme.color_white}}>
                     <View style={{flex:1}}>
                         <SearchBar searchData={this.searchData} allData={this} global_navigation={this.props.navigation} />
                     </View>
                     
 
                     <View style={{backgroundColor: Theme.color_blue,position:'absolute',top:10, flexDirection:'row',marginLeft:'82%'}}>
                         <Filter from={'productlist'} filterClassobjt={this} />
                     </View>
                     </View> */}
                     {/* product List  */}
                     <View>
                         {this.state.showArray ?
                             <FlatList
                                 ref={(ref)=> this.flatListRef = ref}
                                 style={{ marginBottom: 100 }}
                                 data={this.state.AllProducts}
                                 //.slice(0, this.state.pagesCount)}
                                 //data={this.state.AllProducts}
                                 // marginBottom={100}
                                 marginTop={20}
                                 keyExtractor={(item, index) => index.toString()}
                                 showsHorizontalScrollIndicator={false}
                                 showsVerticalScrollIndicator={false}
                                 //   onEndReachedThreshold={0.5}
                                 // onEndReached={this.doInfinte}
                                 //   onScrollBeginDrag={() => this.doInfinte()}
                                 // refreshing={this.state.refreshing}
                                 onEndReachedThreshold={0.5}
                                 onEndReached={this.onEndReached.bind(this)}
                                 onMomentumScrollBegin={() => { [this.onEndReachedCalledDuringMomentum = false] }}
                                 refreshing={this.state.refreshing}
                                 renderItem={this.renderItem.bind(this)}
                                 refreshControl={
                                     <RefreshControl
                                         refreshing={this.state.refreshing}
                                         onRefresh={this.onRefresh.bind(this)}
                                     />}
                             />
                             :
                             <View style={{
                                 width: this.state.deviceWidth, flexDirection: 'column',
                                 justifyContent: 'center', height: this.state.deviceHeight / 1.2,
                                 alignItems: 'center',
                             }}>
                                 <Image
                                     source={Images.comingsoon}
                                     style={{ height: "70%", width: "70%", alignSelf: 'center', }} />
                             </View>}
                     </View>
 
                     {this.state.OnBottomReach ?
                         <Modal
                             isVisible={this.state.OnBottomReach}>
                             <View style={{ alignSelf: 'center' }}>
                                 <CommonActivityIndicator></CommonActivityIndicator>
                             </View>
                         </Modal>
                         : null}
 
                     {/* </ScrollView> */}
                     {this.state.showProgressBar &&
                         <View style={[global_style.Center, {
                             position: 'absolute', left: 0, right: 0, top: 0,
                             bottom: 0, alignItems: 'center', justifyContent: 'center'
                         }]}>
                             <Image source={Images.newLoader}
                                 style={{ flex: 1, width: verticalScale(140), height: verticalScale(35), resizeMode: 'center', marginRight: 30 }} />
                         </View>}
                     {this.state.AddTocartLoading &&
                         <View style={[global_style.Center, {
                             position: 'absolute', left: 0, right: 0, top: 0,
                             bottom: 0, alignItems: 'center', justifyContent: 'center'
                         }]}>
                             <Image source={Images.newLoader}
                                 style={{ flex: 1, width: verticalScale(140), height: verticalScale(35), resizeMode: 'center', marginRight: 30 }} />
                         </View>}
 
                     <TabFooter global_navigation={this.props.navigation} activePage="ProductList" connection_status={this.state.connection_status} />
                     {/* {this.state.showDrawer ?
                         <Drawer global_navigation={this.props.navigation} data={this}/>
                         : null
                     } */}
                     {/* For connection checking */}
                     {!this.state.connection_status ?
                         <ToolbarFooter global_navigation={this.props.navigation} param_title={'You are Offline'} />
                         :
                         null}
                 </View>
                 { this.state.bottomLoader && <View style={{ top: -70 }}>
                     <CommonActivityIndicator />
                 </View>}
                 <TouchableOpacity style={[global_style.gotoTopBtn,{backgroundColor:'red'}]} onPress={this.gotoTop}>
                     <Image source={Images.goToTop} style={ global_style.gotoTopImg}/>
                 </TouchableOpacity>
             </SafeAreaView>
         );
     }
 }
 const styles = StyleSheet.create({
 });