import React, { Component } from 'react';
import {
  View, Text, StyleSheet, Image, TouchableOpacity, Dimensions, YellowBox, Alert,
  FlatList, TextInput, BackHandler, ActivityIndicator, ImageBackground, SafeAreaView,
  PixelRatio, Share, KeyboardAvoidingView, Platform, StatusBar, ScrollView, Button
  , PermissionsAndroid
} from 'react-native'
import global from '../Style/GlobalValue';
import global_style from '../Style/GlobalStyle';
import Modal from "react-native-modal";
import Images from "../Constant/Images";
import Theme from '../Style/Theme'
import Api from "../Constant/Api";
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast'
import StatusBarC from '../Style/StatusBarC';
import AsyncStorage from '@react-native-community/async-storage';
import Util from '../util/Util';
import * as STRINGS from '../util/Strings';
import DeviceInfo from 'react-native-device-info';
import { NavigationEvents } from 'react-navigation';
import {verticalScale,moderateScale,scale} from 'react-native-size-matters'
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
var VersionCode = DeviceInfo.getVersion()

export default class MyCart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceHeight:deviceHeight,
      deviceWidth:deviceWidth,
      connection_status: false,
      showDrawer: false,
      userLID: "",
      userUID: '',
      CartArray: [],
      CartArray1: [{
        Qty: '1', Product_Name: 'Product Name', price: '200'
      }, {
        Qty: '1', Product_Name: 'Product Name', price: '200'
      }, {
        Qty: '1', Product_Name: 'Product Name', price: '200'
      }, {
        Qty: '1', Product_Name: 'Product Name', price: '200'
      }],
      ShowEmptyArray: false,
      userGetData: '',
      TFinalTotal: null,
      FinalTotal: 0.0,
      TaxRatePer: null,
      TotalTaxValue: null,
      EmptyCartData: true,
      limit: '',
      showProgressBar: false,
      itemsOutOfStock: [],
      itemsWithQtyChange: [],
      navigateOnPopup: false

    };
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }


  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);

  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }


  handleBackButtonClick() {
    this.props.navigation.goBack(null);
    //  this.props.navigation.goBack(null);
    //this.props.navigation.goBack(null);
    return true;
  }

  componentDidMount = () => {

    this.willBlur = this.props.navigation.addListener("willBlur", payload =>
      BackHandler.removeEventListener("hardwareBackPress", this.onBack),
    );
    //To get the network state once
    NetInfo.addEventListener(state => {
      if (state.isConnected) {
        this.setState({ connection_status: true });
        //call API
      } else {
        this.setState({ connection_status: false });
      }
    });

    setTimeout(() => {
      this.getUserData();

    }, 50);

    Util.getAsyncStorage('ORDER_MIN_VALUE').then((data) => {
      if (data !== null) {
        this.setState({ limit: data })
      }
    })
    Util.getAsyncStorage('MY_LOCAL_CART').then((data) => {
      if (data !== null) {
        this.setState({ CartArray: data })
        setTimeout(() => {
          //  this.LoadCartProdAPI()
        }, 100);
      }
    })
    // this.showItemsOutOfStock()
  }

  //Get user Info
  getUserData = async () => {

    try {
      // for getting user data
      let user_data = await AsyncStorage.getItem('userData');
      this.setState({ userGetData: get_data })
      //Parse user data
      let get_data = JSON.parse(user_data);
      this.setState({ userLID: get_data.userLID })
      this.setState({ userUID: get_data.userId })
      // setTimeout(() => {
      if (get_data.userStatus != 'Y') {
        this.props.navigation.navigate("Login")
        return
      }
      this.loadLivecart();

      // }, 100);

    } catch (e) {
      // Saving error
      // this.props.navigation.navigate('Login')

      let user_loc = await AsyncStorage.getItem('user_location');
      let get_token = JSON.parse(user_loc);
      //this.state.userLID = get_token.LocationId;

      //this.LoadCartProdAPI();
    }

  }


  loadLivecart = async () => {
    let data = await Util.getAsyncStorage('MY_LOCAL_CART')
    if (data !== null) {
      this.setState({ CartArray: data })
    } else {
      this.setState({ CartArray: [] })
    }
    this.LoadCartProdAPI()

    //     var url = Api.baseUrl + Api.verifyPin


    //     let mUserData = {

    //       "CartDataByUser": "CartDataByUser",
    //       "uid": this.state.userUID
    //     };


    //     fetch(url, {
    //       method: 'POST',
    //       headers: {
    //         'Content-Type': 'application/json',
    //       },
    //       body: JSON.stringify(mUserData),
    //     })
    //       .then((response) => response.json())
    //       .then((response) => {
    // if(response!== 'blank'){

    //         this.setState({ CartLiveArray: response.ProductsDetail })

    //         setTimeout(() => {
    //           this.LoadCartProdAPI();

    //         }, 30);
    //         this.setState({ showProgressBar: false })

    //       }else{
    //         this.setState({ CartLiveArray: [] })

    //         this.LoadCartProdAPI();
    //         this.setState({ showProgressBar: false })
    //       }

    //   })
  }

  LoadCartProdAPI = () => {



    var totalValue = 0.0

    for (let i = 0; i < this.state.CartArray.length; i++) {
      var stringToConvert = this.state.CartArray[i].Total_val_enc
      var numberValue = JSON.parse(stringToConvert);
      totalValue = totalValue + numberValue

    }
    this.setState({ FinalTotal: totalValue.toFixed(2) });

  }


  updateCart = (item, Rowid) => {
    var d = new Date();
    var num = d.getTime();
    var n = num.toString();
    let mUserData = {
      "UpdateCart": "UpdateCart",
      "Unit": item.unit, // per peice 
      "Qty": item.qty,
      "PID": item.PID,
      "color": "",
      "Total_val_enc": item.Total_val_enc,
      "uid": this.state.userUID,
      "AppVersion": VersionCode,
      "rowid": Rowid,
      "CartData": [{
        "rowid": Rowid,
        "id": item.PID,
        "qty": item.qty,
        "price": item.priceData.productprice_ppc,
        "Total_val_enc": item.Total_val_enc,
        "name": "blank",
        "coupon": "XMAS-50OFF",
        "unit": item.unit,
        "single_price": item.single_price,
        "color": "",
        "QtyCount": item.priceData.productprice_qty,
        "Qty_avail": item.Qty_avail,
        "pro_price_data": item.pro_price_data,
        "priceData": item.priceData
      }]
    };

    var url = Api.baseUrl + Api.UpdateApi
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(mUserData),
    })
      .then((response) => response.json())
      .then((response) => {
        if (response.msgsuc == 'bg-success') {
          this.loadLivecart()
        }
      })

  }

  prepareItemForRemoteCart(item) {
    var d = new Date();
    var num = d.getTime();
    var n = num.toString();
    let mUserData = {
      "rowid": n,
      "id": item.PID,
      "qty": item.qty,
      "price": item.priceData.productprice_ppc,
      "Total_val_enc": item.Total_val_enc,
      "name": "blank",
      "coupon": "XMAS-50OFF",
      "unit": item.unit,
      "single_price": item.single_price,
      "color": "",
      "QtyCount": item.priceData.productprice_qty,
      "Qty_avail": item.Qty_avail,
      "pro_price_data": item.pro_price_data,
      "priceData": item.priceData
    }
    return mUserData
  }

  addItemsToRemoteCart = async () => {
    let localCartItems = this.state.CartArray
    let remoteCartItems = []

    for (const item of localCartItems) {
      const remoteItem = this.prepareItemForRemoteCart(item)
      remoteCartItems.push(remoteItem)
    }

    let mUserData = {
      "Updat_Temp_Cart": "Updat_Temp_Cart",
      "uid": this.state.userUID,
      "cart_data": remoteCartItems
    }

    const url = Api.baseUrl + 'InsertAll'

    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(mUserData),
    })
      .then((response) => response.json())
      .catch(error => {
        return ('Somthing went wrong!. Try again later')
      })
    return (response)
  }

  getUpdatedQty = async () => {

    let localCartItems = this.state.CartArray
    let productIdArr = []

    for (let item of localCartItems) {
      productIdArr.push(item.PID)
    }

    const url = Api.baseUrl + 'GetDataAllTest'
    const mUserData = {
      "CartUpdate_V2": "CartUpdate_V2",
      "pids": productIdArr
    }

    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(mUserData),
    })
      .then((response) => response.json())
      .catch(error => {
        return (error.message)
      })
    return response
  }

  getUpdatedQuantity = async () => {
    const url = Api.baseUrl + 'InsertAll'

    const mUserData = {
      "CartUpdate_V1": "CartUpdate_V1",
      "uid": this.state.userUID
    }

    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(mUserData),
    })
      .then((response) => response.json())
      .catch(error => {
        return (error.message)
      })

    return JSON.parse(response.Data.cart_data)
  }

  addtocart = (item) => {
    var d = new Date();
    var num = d.getTime();
    var n = num.toString();
    let mUserData = {
      "AddToCart": "AddToCart",
      "Unit": item.unit, // per peice 
      "Qty": item.qty,
      "PID": item.PID,
      "color": "",
      "Total_val_enc": item.Total_val_enc,
      "uid": this.state.userUID,
      "AppVersion": VersionCode,
      "CartData": [{
        "rowid": n,
        "id": item.PID,
        "qty": item.qty,
        "price": item.priceData.productprice_ppc,
        "Total_val_enc": item.Total_val_enc,
        "name": "blank",
        "coupon": "XMAS-50OFF",
        "unit": item.unit,
        "single_price": item.single_price,
        "color": "",
        "QtyCount": item.priceData.productprice_qty,
        "Qty_avail": item.Qty_avail,
        "pro_price_data": item.pro_price_data,
        "priceData": item.priceData
      }]
    };

    var url = Api.baseUrl + Api.UpdateApi
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(mUserData),
    })
      .then((response) => response.json())
      .then((response) => {
        if (response.msgsuc == 'bg-success') {
          this.loadLivecart()
        }
      })

  }

  // sub-subcategory
  LoadCartProdAPIOld = () => {

    this.setState({ showProgressBar: true })

    if (this.state.connection_status) {

      var url = Api.baseUrl + Api.verifyPin


      let mUserData = {

        "CartDataByUser": "CartDataByUser",
        "uid": this.state.userUID
      };


      fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(mUserData),
      })
        .then((response) => response.json())
        .then((response) => {

          this.setState({ CartArray: response.ProductsDetail })
          this.setState({ showProgressBar: false })

          if (this.state.CartArray != undefined) {


            this.setState({ EmptyCartData: true })



            var totalValue = 0.0

            for (let i = 0; i < this.state.CartArray.length; i++) {
              // totalValue = totalValue + this.state.CartArray[i].Total_val_enc
              //totalValue = totalValue + this.state.CartArray[i].Total_val_enc

              var stringToConvert = this.state.CartArray[i].Total_val_enc
              var numberValue = JSON.parse(stringToConvert);
              totalValue = totalValue + numberValue

              // this.FinalTotal = totalValue.toFixed(2);
            }
            this.setState({ FinalTotal: totalValue.toFixed(2) });


            this.state.CartArray.forEach((item, index) => {

              this.setState({ TFinalTotal: item.Total_val_enc })

              // this.setState({ FinalTotal: (this.state.FinalTotal + item.Total_val_enc) });

              this.setState({ TaxRatePer: parseInt(item.TaxRate) })

              var aValue = this.state.FinalTotal * this.state.TaxRatePer

              var bValue = 100 + this.state.TaxRatePer

              this.setState({ TotalTaxValue: (aValue / bValue).toFixed(2) })



            })
          } else {
            this.setState({ showProgressBar: false })
            this.setState({ EmptyCartData: false })
          }



        })
        .catch((error) => {
          this.setState({ showProgressBar: false })
          //Toast.showWithGravity("Something went wrong", Toast.SHORT, Toast.CENTER);;
        })
        .done();
    } else {
      Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
      this.setState({ showProgressBar: false })
    }
  }

  updateLocalQuantity = async (products) => {
    let itemsOutOfStock = []
    let itemsWithQtyChange = []
    let cartArray = this.state.CartArray

    for (let product of products) {
      let localCartProductIndex = -1 //cartArray.findIndex((item) => { item.PID === product.id})

      for (let index in cartArray) {
        if (cartArray[index].PID == product.id) {
          localCartProductIndex = index
          break
        }
      }

      if (parseInt(cartArray[localCartProductIndex].qty) > parseInt(product.Avail_qty)) {
        cartArray[localCartProductIndex].qty = product.Avail_qty
        let productToCheck = cartArray[localCartProductIndex]
        let price = productToCheck.pro_price_data
        let pricepro = productToCheck.priceData

        let decidePriceArr = price
        decidePriceArr = decidePriceArr.sort((firstProduct, secondProduct) => {
          return parseInt(secondProduct.productprice_qty) - parseInt(firstProduct.productprice_qty)
        })

        for (let priceApplied of decidePriceArr) {

          if (parseInt(product.Avail_qty) === 0) {
            itemsOutOfStock.push(cartArray[localCartProductIndex].ProductName)
          }
          else if (productToCheck.qty >= priceApplied.productprice_qty) {
            productToCheck.priceData = priceApplied
            pricepro = priceApplied
            break
          }
          // else if (priceApplied.productprice_qty == decidePriceArr[0].productprice_qty) {
          //   productToCheck.priceData = priceApplied
          //   pricepro = priceApplied
          // }
        }

        productToCheck.Total_val_enc = productToCheck.qty * pricepro.productprice_ppc
        productToCheck.single_price = pricepro.productprice_ppc
        productToCheck.unit = pricepro.productprice_unit

        cartArray[localCartProductIndex] = productToCheck
        itemsWithQtyChange.push(cartArray[localCartProductIndex].ProductName)
      }

    }

    this.setState({
      itemsWithQtyChange: itemsWithQtyChange,
      itemsOutOfStock: itemsOutOfStock,
      CartArray: cartArray
    })


    if (itemsWithQtyChange.length > 0 || itemsOutOfStock.length > 0) {
      let response = await this.addItemsToRemoteCart()

      if (response.msgsuc === 'bg-success') {
        this.caluculate()
        this.showItemsOutOfStock()
      } else {
        Toast.showWithGravity('Somthing went wrong!. Try again later', Toast.SHORT, Toast.CENTER);
      }
    } else {

      if (this.state.FinalTotal >= this.state.limit) {
        this.props.navigation.navigate('CheckOut', {
          Param_AddAddress: 'fromMyCart'
        })
      } else {
        Toast.showWithGravity(`Minimum order value should be \n ₹ ${this.state.limit}`, Toast.SHORT, Toast.CENTER);
      }


    }
  }

  updateLocalQty = async (products) => {
    let itemsOutOfStock = []
    let itemsWithQtyChange = []
    let cartArray = this.state.CartArray
    
    for (let product of products) {
      let localCartProductIndex = -1 //cartArray.findIndex((item) => { item.PID === product.id})

      for (let index in cartArray) {
        if (cartArray[index].PID == product.pid) {
          localCartProductIndex = index
          break
        }
      }
      
      if (parseInt(cartArray[localCartProductIndex].qty) > parseInt(product.Avail_qty)) {
        cartArray[localCartProductIndex].qty = product.Avail_qty
        let productToCheck = cartArray[localCartProductIndex]
        let price = productToCheck.pro_price_data
        let pricepro = productToCheck.priceData

        let decidePriceArr = price
        decidePriceArr = decidePriceArr.sort((firstProduct, secondProduct) => {
          return parseInt(secondProduct.productprice_qty) - parseInt(firstProduct.productprice_qty)
        })

        if (parseInt(product.Avail_qty) == 0 || parseInt(decidePriceArr[0].productprice_qty)>parseInt(product.Avail_qty)) {
          itemsOutOfStock.push(cartArray[localCartProductIndex].ProductName)
        } else {
          for (let priceApplied of decidePriceArr) {

            if (parseInt(productToCheck.qty) >= parseInt(priceApplied.productprice_qty)) {
              productToCheck.priceData = priceApplied
              pricepro = priceApplied
              break
            }
          }
        }



        productToCheck.Total_val_enc = productToCheck.qty * pricepro.productprice_ppc
        productToCheck.single_price = pricepro.productprice_ppc
        productToCheck.unit = pricepro.productprice_unit

        cartArray[localCartProductIndex] = productToCheck

        if (parseInt(product.Avail_qty) !== 0 && parseInt(product.Avail_qty)>parseInt(productToCheck.priceData.productprice_qty)) {
          itemsWithQtyChange.push(cartArray[localCartProductIndex].ProductName)
        }
      }
    }

    var temp = []
    for (let product of products) {
      for (let index in cartArray) {
        if (cartArray[index].PID == product.pid) {
          temp.push(cartArray[index])
          break
        }
      }
    }
    cartArray = temp
    this.setState({
      itemsWithQtyChange: itemsWithQtyChange,
      itemsOutOfStock: itemsOutOfStock,
      CartArray: cartArray
    })
    this.caluculate()

    console.log("++++@@@@++++",itemsWithQtyChange)
    if (itemsWithQtyChange.length > 0 || itemsOutOfStock.length > 0) {
      //let response = await this.addItemsToRemoteCart()

      cartArray = cartArray.filter((el) => {
        return !(this.state.itemsOutOfStock.includes(el.ProductName))
      })
     
      this.setState({ CartArray: cartArray })
      this.caluculate()
      this.showItemsOutOfStock()


      // if (response.msgsuc === 'bg-success') {
      //   this.caluculate()
      //   this.showItemsOutOfStock() 
      // } else {
      //   Toast.showWithGravity('Somthing went wrong!. Try again later', Toast.SHORT, Toast.CENTER);
      // }
    } else {

      if (this.state.FinalTotal >= this.state.limit) {
        this.props.navigation.navigate('CheckOut', {
          Param_AddAddress: 'fromMyCart'
        })
      } else {
        Toast.showWithGravity(`Minimum order value should be \n ₹ ${this.state.limit}`, Toast.SHORT, Toast.CENTER);
      }
    }
  }

  showItemsOutOfStock() {

    let outOfStockItems = ``
    let inadequateItems = ``
    let message = ``
    //this.state.itemsOutOfStock = ['Pears Soap pack of 3', 'Amla Hair oil Pack of 3 with discount', 'Moti Soap pack of 24']
    //this.state.itemsWithQtyChange=['Pears Soap pack of 7', 'Amla Hair oil Pack of 8 with discount','Moti Soap pack of 56']

    for (let item of this.state.itemsOutOfStock) {
      outOfStockItems += `${'\n'}${item}`
    }

    for (let item of this.state.itemsWithQtyChange) {
      inadequateItems += `${'\n'}${item}`
    }

    if (inadequateItems != `` && outOfStockItems != ``) {
      message = `Items out of Stock ${'\n'}${outOfStockItems}${'\n\n'} Items with Inadequate quantity ${'\n'}${inadequateItems}${'\n'}  
     `
    }
    else if (outOfStockItems != ``) {
      message = `Items out of Stock ${'\n'}${outOfStockItems}${'\n'}`
    }
    else if (inadequateItems != ``) {
      message = ` Items with Inadequate quantity ${'\n'}${inadequateItems}${'\n'}`
    }

    Alert.alert(
      "Following Products may be removed from Cart",
      message,
      [
        {
          text: "Okay", onPress: () => {
            if (this.state.FinalTotal >= this.state.limit) {
              this.props.navigation.navigate('CheckOut', {
                Param_AddAddress: 'fromMyCart'
              })
            }
            else {
              Toast.showWithGravity(`Minimum order value should be \n ₹ ${this.state.limit}`, Toast.SHORT, Toast.CENTER);
            }
          }
        }
      ],
      { cancelable: false }
    )

  }

  verifyUserActivation = () => {
      this.props.navigation.navigate('Checkout')
    // var url = Api.baseUrl + "InsertAll"
	// 	let mUserData = {
    //       "CheckSt":"CheckSt",
    //       "UID":this.state.userUID  
    //   }

	// 	fetch(url, {
	// 	  method: 'POST',
	// 	  headers: {
	// 		'Content-Type': 'application/json',
	// 	  },
	// 	  body: JSON.stringify(mUserData),
	// 	})
	// 	  .then((response) => response.json())
	// 	  .then((response) => {
    //   if(response.msg === "Not Active Now"){
    //     Toast.showWithGravity("Your Account Is Inactive! Please Contact Administrator", Toast.SHORT, Toast.CENTER);
    //     this.props.navigation.replace('Login')
    //   }
    //   if(response.msg === "Active Now"){
    //     this.gotoCheckOut();
    //   }
	// 	})
	// 	  .catch((error) => {
	// 	  })
  }


  gotoCheckOut = async () => {


    let updateQuantityResponse = await this.getUpdatedQty()
    if (this.state.CartArray.length == 0) {
      Toast.showWithGravity(`Minimum order value should be \n ₹ ${this.state.limit}. `, Toast.SHORT, Toast.CENTER);
      return
    }
    if (!(Array.isArray(updateQuantityResponse))) {
      Toast.showWithGravity(`Somthing went wrong!. ${updateQuantityResponse}`, Toast.SHORT, Toast.CENTER);
    } else {
      this.updateLocalQty(updateQuantityResponse)
    }


  }

  // RemoveApi
  RemoveApiNew = (item) => {
    let liveItem = this.state.CartLiveArray.find(e => e.PID === item.PID)

    let mUserData = {

      "RemoveProductFromCart": "RemoveProductFromCart",
      "rowid": liveItem.rowid,
      "UID": this.state.userUID
    };
    var url = Api.baseUrl + Api.verifyPin
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(mUserData),
    })
      .then((response) => response.json())
      .then((response) => {
        if (response.msgsuc == 'bg-success') {
          this.loadLivecart()
        }

      })

  }

  _backRefresh = () => {
    this.getUserData();

  }

  RemoveAllApi = () => {

    Alert.alert(
      'Delete',
      'Are You Sure About Delete',
      [
        {
          text: 'No',
          style: "cancel"
        },
        {
          text: 'Yes', onPress: () => {
            {
              this.setState({ CartArray: [] })
              this.caluculate()
            }
            if (this.state.connection_status) {

              //               var url = Api.baseUrl + Api.verifyPin



              //               let mUserData = {

              //                 "RemoveProductFromCart": "RemoveProductFromCart",
              //                 "rowid": liveItem.rowid,
              //                 "UID": this.state.userUID
              //               };


              //               fetch(url, {
              //                 method: 'POST',
              //                 headers: {
              //                   'Content-Type': 'application/json',
              //                 },
              //                 body: JSON.stringify(mUserData),
              //               })
              //                 .then((response) => response.json())
              //                 .then((response) => {
              //                   if (response.msgsuc == 'bg-success') {
              //                     Toast.showWithGravity(response.msg, Toast.SHORT, Toast.CENTER);
              //                     this.setState({ showProgressBar: false })

              //                     let newLst = this.state.CartArray.filter(arr => arr.PID !== item.PID)
              //                     this.state.CartArray = []
              //                     setTimeout(() => {
              //                       this.setState({ CartArray: newLst })

              //                     }, 40);
              //                     setTimeout(() => {
              //                       this.caluculate()

              //                     }, 20);

              //                     setTimeout(() => {
              //                       this.loadLivecart();

              //                     }, 200);
              //                   } else {
              //                     Toast.showWithGravity(response.msg, Toast.SHORT, Toast.CENTER);
              //                     this.setState({ showProgressBar: false })
              //                   }


              //                 })
              //                 .catch((error) => {
              //                   this.setState({ showProgressBar: false })
              //                   //Toast.showWithGravity("Something went wrong", Toast.SHORT, Toast.CENTER);;
              //                 })
              //                 .done();
            } else {
              Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
              this.setState({ showProgressBar: false })
            }
          }
        }
      ],
      { cancelable: false }
    );
  }

  RemoveApi = (item) => {

    Alert.alert(
      'Delete',
      'AreYouSureAboutDelete',
      [
        {
          text: 'No',
          style: "cancel"
        },
        {
          text: 'Yes', onPress: () => {
            // this.setState({ showProgressBar: true })


            {

              let newLst = this.state.CartArray.filter(arr => arr.PID !== item.PID)

              this.setState({ CartArray: newLst })
              this.caluculate()
              //this.RemoveApiNew(item)

            }
            if (this.state.connection_status) {

              //               var url = Api.baseUrl + Api.verifyPin



              //               let mUserData = {

              //                 "RemoveProductFromCart": "RemoveProductFromCart",
              //                 "rowid": liveItem.rowid,
              //                 "UID": this.state.userUID
              //               };


              //               fetch(url, {
              //                 method: 'POST',
              //                 headers: {
              //                   'Content-Type': 'application/json',
              //                 },
              //                 body: JSON.stringify(mUserData),
              //               })
              //                 .then((response) => response.json())
              //                 .then((response) => {
              //                   if (response.msgsuc == 'bg-success') {
              //                     Toast.showWithGravity(response.msg, Toast.SHORT, Toast.CENTER);
              //                     this.setState({ showProgressBar: false })

              //                     let newLst = this.state.CartArray.filter(arr => arr.PID !== item.PID)
              //                     this.state.CartArray = []
              //                     setTimeout(() => {
              //                       this.setState({ CartArray: newLst })

              //                     }, 40);
              //                     setTimeout(() => {
              //                       this.caluculate()

              //                     }, 20);

              //                     setTimeout(() => {
              //                       this.loadLivecart();

              //                     }, 200);
              //                   } else {
              //                     Toast.showWithGravity(response.msg, Toast.SHORT, Toast.CENTER);
              //                     this.setState({ showProgressBar: false })
              //                   }


              //                 })
              //                 .catch((error) => {
              //                   this.setState({ showProgressBar: false })
              //                   //Toast.showWithGravity("Something went wrong", Toast.SHORT, Toast.CENTER);;
              //                 })
              //                 .done();
            } else {
              Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
              this.setState({ showProgressBar: false })
            }
          }
        }
      ],
      { cancelable: false }
    );
  }
  caluculate = () => {
    this.setState({ CartCount: this.state.CartArray.length })
    var totalValue = 0.0
    for (let i = 0; i < this.state.CartArray.length; i++) {
      var stringToConvert = this.state.CartArray[i].Total_val_enc
      var numberValue = JSON.parse(stringToConvert);
      totalValue = totalValue + numberValue
    }
    this.setState({ FinalTotal: totalValue.toFixed(2) });
    Util.setAsyncStorage('MY_LOCAL_CART', this.state.CartArray)
    this.loadLivecart();

  }

  EditApi = (item) => {
    // let liveItem = this.state.CartArray.find(e => e.PID === item.PID)
    this.props.navigation.navigate('Edit_product', {
      param_productDetails: item

    })


  }

  deleteAll = () => {
    //  this.setState({CartArray : []})
    //  this.setState({ EmptyCartData: true })

  }
  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: Theme.color_bgColor }}>
        <View style={{ flex: 1, backgroundColor: Theme.color_bgColor }} onLayout={(event) =>{ this.setState({deviceWidth:event.nativeEvent.layout.width, deviceHeight:event.nativeEvent.layout.height}) }}>

          {/* for status bar color */}

          <StatusBarC global_navigation={this.props.navigation} />
          <NavigationEvents onDidFocus={() => this._backRefresh()} />
          {/* For loading spinner */}

          <Modal
            isVisible={this.state.showProgressBar}>
            <View style={[global_style.Center]}>
              {/* <Spinner color={Theme.color_orange} style={{ height: 50 }} /> */}
              <Image
                source={Images.newLoader}
                style={{
                  flex: 1,
                  width: verticalScale(140), height: verticalScale(35), resizeMode: 'center', marginRight: 30
                }} />
            </View>
          </Modal>

          <View style={[global_style.outerHeader, { backgroundColor: Theme.color_blue, height: verticalScale(50), }]}>

            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity
                activeOpacity={global.activeOpacity}
                hitSlop={{ top: 30, bottom: 30, left: 30, right: 30 }}
                onPress={() => this.props.navigation.goBack(null)}
                //onPress={() => this.props.navigation.goBack(dashboard)}
                style={[global_style.OuterImgBox]}>

                <Image
                  source={Images.BackArrow}
                  style={[global_style.ImgBox, { marginHorizontal: 15, tintColor: Theme.color_white }]} />

              </TouchableOpacity>

              <TouchableOpacity
                activeOpacity={global.activeOpacity}
                onPress={() => this.props.navigation.goBack(null)}
                hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
                style={[global_style.OuterImgBox]}>

                <Text style={[global_style.TIMES_Regular16,
                { color: Theme.color_white, alignSelf: 'center', alignItems: 'center', }]}>
                  {'GoBack'}
                </Text>
              </TouchableOpacity>

            </View>

            {/* <View style={{ flexDirection: 'row' }}>
                <TouchableOpacity
                  activeOpacity={global.activeOpacity}
                  // onPress={() => this.JumpToSearch()}
                  hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
                  style={[global_style.OuterImgBox, { right: 10 }]}>

                    <Text style={{ color: Theme.color_white }}>
                      {STRINGS.MIN_ORDER_AMOUNT}{this.state.limit} |
                    </Text>
                </TouchableOpacity>
            </View> */}

          </View>

          <ScrollView style={{ flex: 1, width: '100%', height: "100%", }}>

            {this.state.CartArray.length != 0 ?

              <View>
                <View style={{
                  backgroundColor: Theme.color_gray,
                  flexDirection: 'row',
                  padding: 10,
                  justifyContent: 'space-between',
                  flex: 1
                }}>

                  <View style={{ alignSelf: 'center' }}>
                    <Text style={[global_style.TIMES_Bold16, { color: Theme.color_blue, }]}>{'Total Price'}{' 10'}</Text>
                  </View>

                  <Text style={{ color: Theme.color_red, fontSize:moderateScale(14) }}>
                    {STRINGS.MIN_ORDER_AMOUNT} ₹ {this.state.limit}
                  </Text>
                </View>


                <View style={{flexDirection:'row'}}>
                    <View style={{ padding: 10,paddingTop:15,width:'70%' }}>
                      <Text style={[global_style.TIMES_Bold16]}>{'MyCartItem'}</Text>
                    </View>
                    <TouchableOpacity
                     style={{ padding: 10,width:'30%'}}
                    onPress={() => this.RemoveAllApi()}>
                    <View>
                      <Text style={[global_style.TIMES_Bold16,{borderRadius:5,backgroundColor:'red',paddingVertical:6,textAlign:'center',color:'white'}]}>{'ClearCart'}</Text>
                    </View>
                    </TouchableOpacity>
                </View>
                <View
                  style={{
                    borderBottomColor: Theme.color_gray, alignSelf: 'center',
                    borderBottomWidth: 0.6, width: '100%', marginTop: 5, marginBottom: 5
                  }} />
                <View>
                  <FlatList
                    data={this.state.CartArray}
                    marginBottom={10}
                    keyExtractor={(item, index) => index.toString()}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item, index }) =>


                      <View style={{ width: '100%', alignSelf: 'center', marginTop: 5 }}>

                        <View style={{ flexDirection: "row", padding: 10 }}>

                          <View style={{ flex: 1.3, justifyContent: "flex-start" }}>

                            <TouchableOpacity
                              activeOpacity={global.activeOpacity}
                            //onPress={() => this.JumpToProductDetais(item, index)}
                            >
                              {item.image_mid == null ?

                                <Image
                                  source={Images.goodLuckimg}
                                  style={[global_style.thmbnail_box, { width: verticalScale(80), height: verticalScale(90), }]} />

                                :

                                <Image
                                  source={{ uri: Api.imageUrl + item.image_mid }}
                                  style={[global_style.thmbnail_box, { width: verticalScale(100), height: verticalScale(130), }]} />
                              }

                            </TouchableOpacity>

                          </View>

                          <View style={{ flex: 2 }}>
                            <TouchableOpacity
                              activeOpacity={global.activeOpacity}
                              hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
                            // onPress={() => this.JumpToProductDetais(item, index)}
                            >

                              <Text style={[global_style.TIMES_Regular14,]}>
                                {item.ProductName}
                              </Text>

                              <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', marginTop: 10, marginRight: 10 }}>

                                <Text style={[global_style.TIMES_Bold12, { color: Theme.color_blue, }]}>
                                  {'Price'+" :"} {item.single_price}
                                </Text>

                                <Text style={[global_style.TIMES_Bold12, { color: Theme.color_blue, }]}>
                                {'Qty'} {item.qty}
                                  {/* {item.Unit.replace(/[\"[\]']+/g, '')} */}
                                </Text>
                                <Text style={[global_style.TIMES_Bold12, { color: Theme.color_blue, }]}>
                                {'TotalPrice'} {parseFloat(item.Total_val_enc).toFixed(2)}
                                </Text>

                              </View>

                            </TouchableOpacity>

                          </View>


                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 30 }}>

                          <TouchableOpacity
                            activeOpacity={global.activeOpacity}
                            hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
                            onPress={() => this.RemoveApi(item)}
                          >

                            <Text style={[global_style.TIMES_Bold14, { color: Theme.color_red }]}>{'Remove'}</Text>

                          </TouchableOpacity>

                          <TouchableOpacity
                            activeOpacity={global.activeOpacity}
                            hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
                            onPress={() => this.EditApi(item)}
                          >

                            <Text style={[global_style.TIMES_Bold14, { color: Theme.color_green }]}>{'Edit'}</Text>

                          </TouchableOpacity>

                        </View>

                        <View
                          style={{
                            borderBottomColor: Theme.color_gray, alignSelf: 'center',
                            borderBottomWidth: 0.6, width: '100%', marginTop: 5, marginBottom: 5
                          }} />

                      </View>

                    } />

                  <View style={{
                    margin: 10, backgroundColor: 'white', borderWidth: 1,
                    padding: 10, borderRadius: 5, elevation: 4, borderColor: Theme.secondary
                  }}>

                    <Text style={[global_style.TIMES_Bold16, { marginBottom: 10 }]}>
                    {'Price Details'}
                    </Text>

                    <View style={{
                      flexDirection: 'row', justifyContent: 'space-between', borderBottomColor: Theme.secondary,
                      borderBottomWidth: 1,
                    }}>

                      <Text style={[global_style.TIMES_Bold16, { marginBottom: 10 }]}>
                      {'Price Items'}
                      </Text>

                      <Text style={[global_style.TIMES_Regular14, { marginTop: 5 }]}>
                        {this.state.FinalTotal}
                      </Text>

                    </View>

                    <View style={{
                      flexDirection: 'row', justifyContent: 'space-between', borderBottomColor: Theme.secondary,
                      borderBottomWidth: 1,
                    }}>

                      <Text style={[global_style.TIMES_Bold16, { marginBottom: 10 }]}>
                      {'Delivery Charges'}
                      </Text>

                      <Text style={[global_style.TIMES_Regular14, { marginTop: 5 }]}>
                      {'Free'}
                      </Text>

                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>

                      <Text style={[global_style.TIMES_Bold16, { marginBottom: 10 }]}>
                      {'AmountPayble'}
                      </Text>

                      <Text style={[global_style.TIMES_Regular14, { marginTop: 5 }]}>
                        {this.state.FinalTotal}
                      </Text>

                    </View>


                  </View>

                </View>
              </View>

              :

              <View style={{
                //width: this.state.deviceWidth,
                justifyContent: 'center', height: this.state.deviceHeight / 1.4,
                alignItems: 'center',
              }}>

                <Image
                  source={Images.CartEmpty}
                  style={{ width: "70%", height: "70%", resizeMode: 'contain', }} />
                   <TouchableOpacity
                      activeOpacity={global.activeOpacity}
                      hitSlop={{ top: 10, bottom: 10, left: 10, right: 8 }}
                      style={{ alignSelf: "center",backgroundColor: Theme.color_blue, padding: 10, flexDirection: 'row',borderRadius:30, top:15 }}
                      onPress={() => this.props.navigation.navigate('ProductList')}
                    >

                  <View style={{
                     flexDirection: 'row', justifyContent: 'center',
                    alignItems: 'center', alignSelf: 'center', padding: 5
                  }}>

                    <Text style={[global_style.TIMES_Regular16, {
                      color: Theme.color_white, marginHorizontal: 20}]}>
                      {'Continue Shopping'}
                    </Text>

                  </View>

          </TouchableOpacity>

              </View>
              
            }


          </ScrollView>

          {this.state.CartArray.length!=0 ?
            <TouchableOpacity
              activeOpacity={global.activeOpacity}
              hitSlop={{ top: 10, bottom: 10, left: 10, right: 8 }}
              style={{ alignSelf: "center", width: "100%", backgroundColor: Theme.color_blue, padding: 10, flexDirection: 'row' }}
              onPress={() => this.verifyUserActivation()}
            >

              <View style={{
                width: "100%", flexDirection: 'row', justifyContent: 'center',
                alignItems: 'center', alignSelf: 'center', padding: 5
              }}>

                {/* <Image
									style={[{
										width: 20, height: 20,
										resizeMode: 'contain', tintColor: "#fff",
									}]}
									source={Images.cart} /> */}

                <Text style={[global_style.TIMES_Regular16, {
                  color: Theme.color_white, marginLeft: 10

                }]}>
                  {'Proceed To Checkout'}
                </Text>

              </View>

            </TouchableOpacity>
            : 
           null
          }
        </View>
      </SafeAreaView>

    );
  }
}

